package com.indexify.indexify;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indexify.indexify.api.ForgotPasswordApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.ForgotPasswordPojo;
import com.indexify.indexify.pojo.ResponsePojo;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgotPasswordActivity extends AppCompatActivity {
    EditText txtForgotPasswordUsername;
    Button btnForgotPasswordSubmit;
    Button btnCancel;
    TextView tvForgotPasswordTitle;
    TextView tvForgotPasswordSubTitle;
    TextInputLayout textInputLayoutForgot;


    Retrofit retrofit;
    Typeface playBold;
    Typeface playRegular;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_forgot_password);
        getControls();


    }

    private void getControls() {
        //for font
        playBold = Typeface.createFromAsset(ForgotPasswordActivity.this.getAssets(),
                "fonts/play_bold.ttf");

        playRegular = Typeface.createFromAsset(ForgotPasswordActivity.this.getAssets(),
                "fonts/play_regular.ttf");



        txtForgotPasswordUsername = (EditText) findViewById(R.id.txtForgotPasswordUsername);
        btnForgotPasswordSubmit = (Button) findViewById(R.id.btnForgotPasswordSubmit);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        tvForgotPasswordTitle = (TextView) findViewById(R.id.tvForgotPasswordTitle);
        tvForgotPasswordSubTitle = (TextView) findViewById(R.id.tvForgotPasswordSubTitle);
        textInputLayoutForgot = (TextInputLayout) findViewById(R.id.textInputLayoutForgot);

        //set font

        tvForgotPasswordTitle.setTypeface(playBold);
        tvForgotPasswordSubTitle.setTypeface(playRegular);
        textInputLayoutForgot.setTypeface(playRegular);
        btnForgotPasswordSubmit.setTypeface(playBold);
        btnCancel.setTypeface(playRegular);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Constants.BASE_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        retrofit = builder.build();
        getData();
    }

    private void getData() {


        btnForgotPasswordSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = txtForgotPasswordUsername.getText().toString().trim();


                if ((username != null) && (username.length() > 0)) {
                    boolean emailIdValid = false;

                    emailIdValid = org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(username);
                    if (emailIdValid) {
                        if (ConstantsMethods.isConnected(ForgotPasswordActivity.this)) {
                            getForgotPassword(username);
                        }
                    } else {
                        showError("Email Not Valid!", "Please Enter Valid Email Id");
                    }
                } else {

                    showError("Empty", "Email is Empty");
                }


            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }


    private void getForgotPassword(String userName) {


        ConstantsMethods.showProgessDialog(ForgotPasswordActivity.this, "Please Wait");
        try {


            ForgotPasswordApi forgotPasswordApi = retrofit.create(ForgotPasswordApi.class);
            Single<ForgotPasswordPojo> responseSingle = forgotPasswordApi.getNewPassword(userName);
            responseSingle.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<ForgotPasswordPojo>() {
                @Override
                public void onSuccess(@NonNull ForgotPasswordPojo forgotPasswordPojo) {
                    ConstantsMethods.cancleProgessDialog();
                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ForgotPasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                    sweetAlertDialog.setTitleText("Congrats Password Changed");
                    sweetAlertDialog.setContentText("Your New Password Has Been Sent to Your Registered Email");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.setConfirmText("    OK    ");
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    });
                    sweetAlertDialog.show();
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    try {
                        HttpException ex = (HttpException) e;
                        String er = ex.response().errorBody().string();
                        ResponsePojo erroPojo = new Gson().fromJson(er, ResponsePojo.class);

                        if (erroPojo != null) {
                            if (erroPojo.getStatus() == 500) {
                                showError("Intermal server error", "Something went wrong please try later.");
                            } else if (erroPojo.getResponseCode() == ConstantsErrorCodes.EMAIL_NOT_VERIFIED) {
                                showError("Email", "Email not Verified or Email Does not Exists!");
                            }
                        }
                    } catch (Exception e1) {
                        showError("Error", "Something went wrong please try later.");
                    } finally {
                        ConstantsMethods.cancleProgessDialog();
                    }
                }
            });


        } catch (Exception error) {
            ConstantsMethods.cancleProgessDialog();
            showError("Unknown Error!", "Unknown Error");
        }


    }

    private void showError(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ForgotPasswordActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });
    }
}


