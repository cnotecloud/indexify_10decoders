package com.indexify.indexify;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.indexify.indexify.adapter.FavouriteOrganizationProductAdapter;
import com.indexify.indexify.api.FavouriteApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.pojo.FavouriteProductPojo;
import com.indexify.indexify.pojo.FavouriteProductsGetPojo;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class FavouriteProductListActivity extends AppCompatActivity {

    RecyclerView rvFavouriteProducts;
    LinearLayoutManager mLayoutManager;
    ProgressBar progressbar;
    List<FavouriteProductPojo> alFavoriteproducts = new ArrayList<>();
    FavouriteOrganizationProductAdapter adapter = null;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 5;
    int page = 0;
    Long organizationId;
    String organizationName = "";
    boolean isEmpty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_product_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getControls();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getControls() {
        resetValues();
        organizationId = getIntent().getLongExtra("orgId", 0);
        organizationName = getIntent().getStringExtra("orgName");
        getSupportActionBar().setTitle(organizationName);
        rvFavouriteProducts = (RecyclerView) findViewById(R.id.rvFavouriteProducts);
        mLayoutManager = new LinearLayoutManager(FavouriteProductListActivity.this, LinearLayoutManager.VERTICAL, false);
        rvFavouriteProducts.setHasFixedSize(true);
        rvFavouriteProducts.setLayoutManager(mLayoutManager);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.INVISIBLE);

        rvFavouriteProducts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = rvFavouriteProducts.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (ConstantsMethods.isConnected(FavouriteProductListActivity.this)) {
                                loadMoreProducts();
                            }
                        }
                    });
                    // Do something
                    loading = true;
                }
            }
        });

        if (ConstantsMethods.isConnected(FavouriteProductListActivity.this)) {
            getData();
        }
    }

    private void loadMoreProducts() {
        if (ConstantsMethods.isConnectedWithoutMessage(FavouriteProductListActivity.this)) {
            progressbar.setVisibility(View.VISIBLE);
            try {
                FavouriteApi favouriteApi = retrofit.create(FavouriteApi.class);
                Observable<FavouriteProductsGetPojo> observable = favouriteApi.getFavouriteProductsByOrganizationId(organizationId, ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_USER_SCAN_LOADING_SIZE, ++page);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<FavouriteProductsGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(FavouriteProductsGetPojo alProducts) {
                                progressbar.setVisibility(View.INVISIBLE);
                                if (alProducts != null && alProducts.getEmbedded().getFavouriteProducts().size() > 0) {
                                    alFavoriteproducts.addAll(alProducts.getEmbedded().getFavouriteProducts());
                                    adapter.notifyDataSetChanged();
                                    getSupportActionBar().setTitle("(" + alFavoriteproducts.size() + ") " + organizationName);
                                    //  getSupportActionBar().setTitle("(" + alOrganizations.size() + ")  Favourites ");
                                } else if (alFavoriteproducts != null && alFavoriteproducts.size() == 0) {
                                    // show not found message
                                    showError("Not found", "Favourite organization data not found", 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                progressbar.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {
                progressbar.setVisibility(View.INVISIBLE);
            } finally {

            }

        }
    }

    public void getData() {
        //get my scan from server
        if (ConstantsMethods.isConnected(FavouriteProductListActivity.this)) {
            try {
                ConstantsMethods.showProgessDialogWithCancelable(FavouriteProductListActivity.this, "Loading favourite proudcts...");
                FavouriteApi favouriteApi = retrofit.create(FavouriteApi.class);
                Observable<FavouriteProductsGetPojo> observable = favouriteApi.getFavouriteProductsByOrganizationId(organizationId, ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_USER_SCAN_LOADING_SIZE, page);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<FavouriteProductsGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(FavouriteProductsGetPojo alProducts) {
                                ConstantsMethods.cancleProgessDialog();
                                if (alProducts != null && alProducts.getEmbedded().getFavouriteProducts().size() > 0) {
                                    alFavoriteproducts = new ArrayList<FavouriteProductPojo>();
                                    alFavoriteproducts.addAll(alProducts.getEmbedded().getFavouriteProducts());
                                    adapter = new FavouriteOrganizationProductAdapter(alFavoriteproducts, FavouriteProductListActivity.this);
                                    rvFavouriteProducts.setAdapter(adapter);
                                    getSupportActionBar().setTitle("(" + alFavoriteproducts.size() + ") " + organizationName);
                                } else if (alFavoriteproducts != null && alFavoriteproducts.size() == 0) {
                                    // show not found message
                                    showError("Not found", "Favourite products data not found", 1);
                                }
                            }


                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                                try {
                                    HttpException error = (HttpException) e;

                                    if (error != null) {
                                        showError("Not found", "Favourite products data not found", 1);
                                    }

                                } catch (Exception e1) {
                                    showError("Not found", "Favourite products data not found", 1);
                                }

                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {

            } finally {

            }
        }
    }

    public void updateToolbar(int size) {
        getSupportActionBar().setTitle("(" + size + ") " + organizationName);
        if (size == 0){
            Intent intent = new Intent();
            setResult(900,intent);
            finish();
        }
    }

    private void showError(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(FavouriteProductListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        });

    }

    private void showErrorWithoutFinish(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(FavouriteProductListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                //    finish();
            }
        });

    }


    public void resetValues() {
        alFavoriteproducts.clear();
        page = 0;
        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        loading = true;
        previousTotal = 0;
        visibleThreshold = 5;
    }

}
