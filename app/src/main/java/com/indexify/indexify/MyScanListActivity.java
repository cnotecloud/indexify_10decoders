package com.indexify.indexify;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.indexify.indexify.adapter.UserScanAdapter;
import com.indexify.indexify.api.UserScanApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.interfaces.DeleteOrganizationScanById;
import com.indexify.indexify.interfaces.FinishOrgDataAndUserScanActivity;
import com.indexify.indexify.pojo.UserScanGetPojo;
import com.indexify.indexify.pojo.UserScanPojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class MyScanListActivity extends AppCompatActivity implements DeleteOrganizationScanById, FinishOrgDataAndUserScanActivity {

    RecyclerView rvMyScanList;
    LinearLayoutManager mLayoutManager;
    ProgressBar progressbar;
    SearchView search;
    List<UserScanPojo> alUserScans = new ArrayList<>();
    List<UserScanPojo> alRefreshUserScans = new ArrayList<>();
    UserScanAdapter adapter = null;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 5;
    int page = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_scan_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getControls();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getControls() {
        resetValues();
        rvMyScanList = (RecyclerView) findViewById(R.id.rvMyScanList);
        mLayoutManager = new LinearLayoutManager(MyScanListActivity.this, LinearLayoutManager.VERTICAL, false);
        rvMyScanList.setHasFixedSize(true);
        rvMyScanList.setLayoutManager(mLayoutManager);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.INVISIBLE);

        rvMyScanList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = rvMyScanList.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (ConstantsMethods.isConnected(MyScanListActivity.this)) {
                                loadMoreUserScans();
                            }
                        }
                    });
                    // Do something
                    loading = true;
                }
            }
        });

        if (ConstantsMethods.isConnected(MyScanListActivity.this)) {
            getData();
        }
    }

    private void loadMoreUserScans() {
        if (ConstantsMethods.isConnectedWithoutMessage(MyScanListActivity.this)) {
            progressbar.setVisibility(View.VISIBLE);
            try {
                UserScanApi userScanApi = retrofit.create(UserScanApi.class);
                Observable<UserScanGetPojo> observable = userScanApi.getUserScanList(ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_USER_SCAN_LOADING_SIZE, ++page);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserScanGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(UserScanGetPojo userScanGetPojo) {
                                progressbar.setVisibility(View.INVISIBLE);
                                if (userScanGetPojo != null && userScanGetPojo.getEmbedded().getUserScans().size() > 0) {
                                    alUserScans.addAll(userScanGetPojo.getEmbedded().getUserScans());
                                    adapter.notifyDataSetChanged();
                                    getSupportActionBar().setTitle("(" + alUserScans.size() + ")  Scans");
                                } else if (userScanGetPojo != null && userScanGetPojo.getEmbedded().getUserScans().size() == 0) {
                                    // show not found message
                                    //showError("Not found", "User scan data not found", 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                progressbar.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {
                progressbar.setVisibility(View.INVISIBLE);
            } finally {

            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_scan_activity, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search = (SearchView) menu.findItem(R.id.action_search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() < 3) {
                    Toast.makeText(MyScanListActivity.this, "Enter minimum 3 chars", Toast.LENGTH_SHORT).show();
                } else {
                    // check internet connectin
                    if (ConstantsMethods.isConnected(MyScanListActivity.this)) {

                        searchUserScanContaining(query);
                    }
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                /*if (newText.length() == 0) {
                    rvUserList.setAdapter(adapter);
                }*/
                return true;
            }
        });
        return true;
    }

    private void searchUserScanContaining(String query) {
        if (ConstantsMethods.isConnected(MyScanListActivity.this)) {
            try {
                ConstantsMethods.showProgessDialogWithCancelable(MyScanListActivity.this, "Searching...");

                UserScanApi userScanApi = retrofit.create(UserScanApi.class);
                Observable<UserScanGetPojo> observable = userScanApi.searchUserScanContaining(query.toLowerCase(), ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_USER_SCAN_LOADING_SIZE, 0);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserScanGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(UserScanGetPojo userScanGetPojo) {
                                ConstantsMethods.cancleProgessDialog();

                                if (userScanGetPojo != null && userScanGetPojo.getEmbedded().getUserScans().size() > 0) {
                                    Intent intent = new Intent(MyScanListActivity.this, MyScanSearchListActivity.class);
                                    intent.putExtra("searchText", query);
//                                    intent.putExtra("context", String.valueOf(getApplicationContext()));
                                    intent.putExtra("userScans", (Serializable) userScanGetPojo.getEmbedded().getUserScans());
                                    startActivityForResult(intent, 200);
                                } else if (userScanGetPojo != null && userScanGetPojo.getEmbedded().getUserScans().size() == 0) {
                                    // show not found message
                                    showErrorWithoutFinish("Not found", "User scan data not found", 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                                showErrorWithoutFinish("Not found", "User scan data not found", 1);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {

            } finally {

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 200) {
            search.setIconified(true);
            search.onActionViewCollapsed();
            getControls();

//            String deletedId = EasySP.init(MyScanListActivity.this).getString("deletedId", "");
//            if (deletedId != null) {
//                for (int i = 0; i < alUserScans.size(); i++) {
//                    if (alUserScans.get(i).getId().equalsIgnoreCase(deletedId)) {
//                        alUserScans.remove(i);
//                        adapter = new UserScanAdapter(alUserScans, MyScanListActivity.this,false);
//                        getSupportActionBar().setTitle("(" + alUserScans.size() + ")  Scans");
//                        rvMyScanList.setAdapter(adapter);
//                    }
//                }
//            }


        } else if (requestCode == 505 && resultCode == 506) {
            finish();
        }
    }

    public void getData() {
        //get my scan from server
        if (ConstantsMethods.isConnected(MyScanListActivity.this)) {
            try {
                ConstantsMethods.showProgessDialogWithCancelable(MyScanListActivity.this, "Loading Scan List...");

                UserScanApi userScanApi = retrofit.create(UserScanApi.class);
                Observable<UserScanGetPojo> observable = userScanApi.getUserScanList(ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_USER_SCAN_LOADING_SIZE, page);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserScanGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(UserScanGetPojo userScanGetPojo) {
                                ConstantsMethods.cancleProgessDialog();
                                if (userScanGetPojo != null && userScanGetPojo.getEmbedded().getUserScans().size() > 0) {
                                    alUserScans = new ArrayList<UserScanPojo>();
                                    alUserScans.addAll(userScanGetPojo.getEmbedded().getUserScans());
                                    adapter = new UserScanAdapter(alUserScans, MyScanListActivity.this);
                                    rvMyScanList.setAdapter(adapter);
                                    getSupportActionBar().setTitle("(" + alUserScans.size() + ")  Scans");
                                } else if (userScanGetPojo != null && userScanGetPojo.getEmbedded().getUserScans().size() == 0) {
                                    // show not found message
                                    showError("Not found", "User scan data not found", 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {

            } finally {

            }
        }
    }

    private void showError(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MyScanListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        });

    }

    private void showErrorWithoutFinish(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MyScanListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                //    finish();
            }
        });

    }

    public void resetValues() {
        alUserScans.clear();
        page = 0;
        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        loading = true;
        previousTotal = 0;
        visibleThreshold = 5;
    }

    public void refreshList(List alRefreshedUserScans) {
        this.alRefreshUserScans = alRefreshedUserScans;
        adapter = new UserScanAdapter(alRefreshedUserScans, MyScanListActivity.this);
        adapter.notifyDataSetChanged();
        getSupportActionBar().setTitle("(" + alRefreshedUserScans.size() + ")  Scans");
        rvMyScanList.setAdapter(adapter);
    }


    @Override
    public void deleteOrganizationScanById(String orgScanId) {
        getSupportActionBar().setTitle("(" + alUserScans.size() + ")  Scans");
    }

    @Override
    public void finishOrgDataAndUserScanActivity() {
        finish();
    }

    public void startForActivityResult(Intent intent) {
        startActivityForResult(intent, 505);
    }


}
