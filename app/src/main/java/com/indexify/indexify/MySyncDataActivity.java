package com.indexify.indexify;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.indexify.indexify.api.NodeApi;
import com.indexify.indexify.api.ProductApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.constants.ConstantsTables;
import com.indexify.indexify.database.DatabaseAccess;
import com.indexify.indexify.pojo.NodePojo;
import com.indexify.indexify.pojo.NodesGetPojo;
import com.indexify.indexify.pojo.ProductPojo;
import com.indexify.indexify.pojo.ProductsGetPojo;
import com.indexify.indexify.pojo.ResponsePojo;
import com.white.easysp.EasySP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.HttpException;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class MySyncDataActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnSync;
    Button btnCancel;
    TextView tvCount;
    TextView tvName;
    TextView tvLoading;
    TextView tvTotal;
    ImageView imgLoading;
    TextView tvCountProducts;
    TextView tvTotalProducts;
    long orgId;
    int displayCount = 1;
    int totalSize = 50;

    List<String> alNewProductsImageList;
    List<String> alOldProductsImageList;
    List<String> alOldProductsLocalImageList;
    List<ProductPojo> alOldProducts;
    List<ProductPojo> alNewProducts;
    ProductsGetPojo productsGetPojo;
    boolean isError = false;
    boolean isFinish = false;
    boolean isImageDownloadError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_sync_data);
        orgId = getIntent().getLongExtra("orgId", 0);
        getControls();


    }

    private void getControls() {
        imgLoading = (ImageView) findViewById(R.id.imgLoading);
        tvCount = (TextView) findViewById(R.id.tvCount);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvLoading = (TextView) findViewById(R.id.tvLoading);
        tvName = (TextView) findViewById(R.id.tvName);
        tvCountProducts = (TextView) findViewById(R.id.tvCountProducts);
        tvTotalProducts = (TextView) findViewById(R.id.tvTotalProducts);

        btnSync = (Button) findViewById(R.id.btnSync);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSync.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        getData();
    }

    private void getData() {
        //put this code after dataDownload complete


        alOldProductsImageList = new ArrayList<>();
        alNewProductsImageList = new ArrayList<>();
        alOldProductsLocalImageList = new ArrayList<>();
        alNewProducts = new ArrayList<>();
        alOldProducts = new ArrayList<>();

        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(MySyncDataActivity.this);
        databaseAccess.open();
        alOldProducts = databaseAccess.getProductsForImageDownload();
        databaseAccess.close();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSync) {
            if (ConstantsMethods.isConnected(MySyncDataActivity.this)) {
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("Sync All Data !");
                sweetAlertDialog.setContentText("Are you sure ?\nIt will take some time.");
                sweetAlertDialog.setCancelText("Cancel");
                sweetAlertDialog.setConfirmText("  Sync  ");
                sweetAlertDialog.showCancelButton(true);
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                    }
                });
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        btnSync.setEnabled(false);
                        sweetAlertDialog.cancel();

                        if (ConstantsMethods.isConnectedWithoutMessage(MySyncDataActivity.this)) {
                            syncData();
                        } else {
                            SweetAlertDialog sweetAlertDialog1 = new SweetAlertDialog(MySyncDataActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog1.setTitleText("No Internet");
                            sweetAlertDialog1.setContentText("Please check Intenet connection.");
                            sweetAlertDialog1.setCancelable(false);
                            sweetAlertDialog1.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                }
                            });
                            sweetAlertDialog1.show();
                        }
                    }
                });
                sweetAlertDialog.show();
            }
        } else if (view.getId() == R.id.btnCancel) {
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText("Cancel !");
            sweetAlertDialog.setContentText("Are you sure ?");
            sweetAlertDialog.setCancelText("No");
            sweetAlertDialog.setConfirmText("Yes");
            sweetAlertDialog.showCancelButton(true);
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.cancel();
                    // finish();
                }
            });
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    isFinish = true;
                    sweetAlertDialog.cancel();
                    finish();
                }
            });
            sweetAlertDialog.show();
        }
    }

    private void syncData() {


        try {

            Glide.with(MySyncDataActivity.this).load(R.drawable.loading)
                    .priority(Priority.IMMEDIATE)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(imgLoading);

            tvName.setText("Structure");

            //get refresh token

//                    String refreshToken = EasySP.init(MySyncDataActivity.this).getString(ConstantsEasySP.SP_REFRESH_TOKEN);
//                    Retrofit.Builder builderToken = new Retrofit.Builder()
//                            .baseUrl(Constants.BASE_ENDPOINT)                         // set Base url (Endpoint)                                                         // adding the okhttp clint in retrofit object
//                            .addConverterFactory(GsonConverterFactory.create())
//                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
//
//                    Retrofit retrofitToken = builderToken.build();
//                    final LoginApi loginApi = retrofitToken.create(LoginApi.class);
//                    Call<TokenPojo> newTokenFromRefreshToken = loginApi.getNewTokenFromRefreshToken(refreshToken);
//                    TokenPojo tokenPojo = null;
//                    try {
//                        tokenPojo = newTokenFromRefreshToken.execute().body();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    if (tokenPojo != null) {
//                        EasySP.init(MySyncDataActivity.this).putString(ConstantsEasySP.SP_TOKEN, "Bearer " + tokenPojo.getToken());
//                    }

            //get My Nodes
            final NodeApi nodeApi = retrofit.create(NodeApi.class);
            Observable<NodesGetPojo> getMyNodes = nodeApi.getMyNodesByOrganizationId(orgId, ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_MY_PRODUCT_GET_SIZE, Constants.DEFAULT_PAGE);
            getMyNodes.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<NodesGetPojo>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(NodesGetPojo nodesGetPojo) {
                    if (nodesGetPojo != null) {
                        insertMyNodes(nodesGetPojo);

                        //get products and save to database

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tvName.setText("Products");
                                    }
                                });
                                int page = 0;
                                int size = 50;

                                while (true) {
                                    if (isFinish == false) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (displayCount == 1) {
                                                    tvTotal.setText(displayCount + " - " + totalSize + " products )");
                                                } else {
                                                    displayCount = displayCount + 49;
                                                    totalSize = totalSize + 50;
                                                    tvTotal.setText(displayCount + " - " + totalSize + " products )");
                                                }
                                            }
                                        });

                                        ProductApi productApi = retrofit.create(ProductApi.class);
                                        Call<ProductsGetPojo> getPojoCall = productApi.getMyProducts(ConstantsProjection.PROJECTION_DETAIL, size, page);
                                        try {

                                            productsGetPojo = getPojoCall.execute().body();

                                            if (productsGetPojo != null) {
                                                if (productsGetPojo.getEmbedded().getProducts() != null) {
                                                    if (productsGetPojo.getEmbedded().getProducts().size() > 1) {
                                                        insertProduct(productsGetPojo);
                                                        for (int f = 0; f < productsGetPojo.getEmbedded().getProducts().size(); f++) {
                                                            ProductPojo productPojo = productsGetPojo.getEmbedded().getProducts().get(f);
                                                            alNewProducts.add(productPojo);
                                                        }
                                                        page++;
                                                    } else if (productsGetPojo.getEmbedded().getProducts().size() == 0) {
                                                        break;
                                                    }
                                                } else {
                                                    break;
                                                }

                                            } else {
                                                break;
                                            }


                                        } catch (Exception e) {
                                            isError = true;
                                            break;

                                        }

                                        displayCount++;
                                    } else {
                                        break;

                                    }
                                }

                                if (isFinish == true) {
                                    finish();
                                } else {
                                    //get Old ImageUrls

                                    if (isError == true) {
                                        showErrorWithFinish("Error", "Something Went wrong try after some time");
                                    } else {
                                        dataDownloadComplete();
                                        /*Retrofit retrofit;
                                        OkHttpClient okhttpBulider = new OkHttpClient.Builder()
                                                .connectTimeout(600, TimeUnit.SECONDS)
                                                .readTimeout(600, TimeUnit.SECONDS)
                                                .build();
                                        String oldImageUrlString ="";
                                        String newImageUrlString ="";
                                        boolean isFound = false;
                                       exitMainLoop: for (int a = 0;a<alNewProducts.size();a++){
                                           if ((alNewProducts.get(a).getImgUrls() != null) && (alNewProducts.get(a).getImgUrls().size() > 0)) {
                                               newImageUrlString = alNewProducts.get(a).getImgUrls().toString();

                                           }
                                            exitLoop :for (int b = 0;b<alOldProducts.size();b++){
                                                if (alOldProducts.get(b).getName().equalsIgnoreCase(alNewProducts.get(a).getName())){
                                                    if ((alOldProducts.get(b).getImgUrls() != null) && (alOldProducts.get(b).getImgUrls().size() > 0)) {
                                                        oldImageUrlString = alOldProducts.get(b).getImgUrls().toString();
                                                        break exitLoop;
                                                    }

                                                }
                                            }
                                            if ((newImageUrlString.length() > 0) && (oldImageUrlString.length() > 0)) {
                                                if (!newImageUrlString.equalsIgnoreCase(oldImageUrlString)) {
                                                    isFound = true;

                                                }
                                            }
                                            if (isFound == true){

                                                ProductPojo productPojo = alNewProducts.get(a);
                                                List<String> alNewLocalImagePaths = new ArrayList<>();
                                                //check img urls are not null
                                                if ((productPojo.getImgUrls() != null) && (productPojo.getImgUrls().size() > 0)) {
                                                    //get images from list using for loop
                                                    for (int k = 0; k < productPojo.getImgUrls().size(); k++) {
                                                        try {

                                                            if ((productPojo.getImgUrls().get(k) != null) && (productPojo.getImgUrls().get(k).length() > 0)) {
                                                                String fileName = getFileName(productPojo.getImgUrls().get(k)).trim();
                                                                String endPoint = productPojo.getImgUrls().get(k).substring(0, productPojo.getImgUrls().get(k).length() - fileName.length());
                                                                Retrofit.Builder builder = new Retrofit.Builder()
                                                                        .baseUrl(endPoint)                         // set Base url (Endpoint)
                                                                        .client(okhttpBulider)                                   // adding the okhttp clint in retrofit object
                                                                        .addConverterFactory(GsonConverterFactory.create())
                                                                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

                                                                retrofit = builder.build();


                                                                ImageApi imageApi = retrofit.create(ImageApi.class);

                                                                Call<ResponseBody> call = imageApi.res(fileName);
                                                                ResponseBody body = call.execute().body();


                                                                if (body != null) {
                                                                    File file = saveImage(fileName, body);
                                                                    Uri uri = Uri.fromFile(file);
                                                                    alNewLocalImagePaths.add(uri.toString());
                                                                    Log.i("uri", uri.getPath());
                                                                }
                                                            }
                                                        } catch (Exception error) {
                                                            isImageDownloadError = true;
                                                            break exitMainLoop;
                                                        }
                                                    }

                                                }
                                                //update to localimage in database

                                                if (alNewLocalImagePaths.size() > 0) {
                                                    DatabaseAccess databaseAccess2 = DatabaseAccess.getInstance(MySyncDataActivity.this);
                                                    databaseAccess2.open();
                                                    int x = databaseAccess2.updateLocalImageUrlOfProdcut(productPojo.getId(), alNewLocalImagePaths);
                                                    databaseAccess2.close();
                                                }
                                            }else{
                                                isFound = false;
                                            }

                                        }
*/


                                    }
                                }


                            }
                        }).start();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    try {
                        HttpException ex = (HttpException) e;
                        String er = ex.response().errorBody().string();
                        ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                        if (responsePojo != null) {
                            if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.INVALID_USERNAME_PASSWORD) {
                                showError("Unauthorized", "Please check email and password.");
                            } else if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.EMAIL_NOT_VERIFIED) {
                                showError("User not verified", "Please verify through email and try again.");
                            } else if (responsePojo.getStatus() == 403) {
                                showError("Not access", "Please try again or contact admin.");
                            } else if (responsePojo.getStatus() == 500) {
                                showError("Internal server error", "Something went wrong please try later.");
                            }
                        }
                    } catch (Exception e1) {
                        showErrorWithFinish("Error", "Something went wrong please try later.");
                    } finally {

                    }

                }

                @Override
                public void onComplete() {

                }
            });

        } catch (
                Exception error)

        {

        }

    }

    //insert MyNodes to database and check whether node exists or not
    private void insertMyNodes(NodesGetPojo nodesGetPojo) {
        try {
            for (int i = 0; i < nodesGetPojo.getEmbedded().getNodes().size(); i++) {
                DatabaseAccess databaseAccess = DatabaseAccess.getInstance(MySyncDataActivity.this);
                databaseAccess.open();
                if ((nodesGetPojo.getEmbedded().getNodes() != null) && (nodesGetPojo.getEmbedded().getNodes().size() > 0)) {
                    NodePojo nodePojo = nodesGetPojo.getEmbedded().getNodes().get(i);

                    ContentValues values = new ContentValues();
                    values.put(ConstantsTables.T_MY_NODES_ORG_ID, orgId);
                    values.put(ConstantsTables.T_MY_NODES_SERVER_ID, nodePojo.getId());
                    values.put(ConstantsTables.T_MY_NODES_DISPLAY_NAME, nodePojo.getDisplayName());
                    values.put(ConstantsTables.T_MY_NODES_PARENT_SERVER_ID, nodePojo.getParentId());

                    if (nodePojo.isLeaf()) {
                        values.put(ConstantsTables.T_MY_NODES_IS_LEAF, 1);
                    } else {
                        values.put(ConstantsTables.T_MY_NODES_IS_LEAF, 0);
                    }

                    values.put(ConstantsTables.T_MY_NODES_CREATED, nodePojo.getCreated());
                    values.put(ConstantsTables.T_MY_NODES_UPDATED, nodePojo.getUpdated());

                    String x = databaseAccess.checkNodeExistOrNot(values);
                    Log.i("myNode insert", x);
                    databaseAccess.close();
                }

            }
        } catch (Exception e) {
            Log.e("node error : ", e.toString());
        }

    }

    //insert product to database and check products exists or not
    private void insertProduct(ProductsGetPojo productGetPojo) {
        Gson gson = new Gson();
        try {
            for (int i = 0; i < productGetPojo.getEmbedded().getProducts().size(); i++) {
                DatabaseAccess databaseAccess = DatabaseAccess.getInstance(MySyncDataActivity.this);
                databaseAccess.open();
                ProductPojo product = productGetPojo.getEmbedded().getProducts().get(i);
                ContentValues values = new ContentValues();

                values.put(ConstantsTables.T_MY_PRODUCTS_SERVER_ID, product.getId());
                values.put(ConstantsTables.T_MY_PRODUCTS_NAME, product.getName());
                values.put(ConstantsTables.T_MY_PRODUCTS_LEAF_NODE_SERVER_ID, product.getLeafId());

                String urlJsonStirng = new Gson().toJson(product.getImgUrls());
                values.put(ConstantsTables.T_MY_PRODUCTS_IMG_URLS, urlJsonStirng);


                if (product.getControls() != null) {
                    if (product.getControls().size() > 0) {
                        String controls = gson.toJson(product.getControls());
                        values.put(ConstantsTables.T_MY_PRODUCTS_CONTROLS, controls);
                    } else if (product.getControls().size() == 0) {
                        values.put(ConstantsTables.T_MY_PRODUCTS_CONTROLS, "");
                    }
                } else {
                    values.put(ConstantsTables.T_MY_PRODUCTS_CONTROLS, "");
                }
                String x = databaseAccess.checkProductExistOrNot(values);
                databaseAccess.close();
                Log.i("product ", String.valueOf(x));
            }

        } catch (Exception e) {
            Log.e("insert product error:", " " + productGetPojo.getEmbedded().getProducts().size());
        }
    }

    private File saveImage(String fileName, ResponseBody response) {
        // File imageDir = new File(Environment.getExternalStorageDirectory() + File.separator + "imagesx");
        //File imageDir = new File(this.getFilesDir() + File.separator + "imagesx");
        File imageDir = new File(this.getFilesDir() + File.separator + "imagesx");
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
        Log.d("sdf", "saveImage: " + imageDir + File.separator + fileName);
        File file = new File(imageDir + File.separator + fileName);

        FileOutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            inputStream = response.byteStream();
            outputStream = new FileOutputStream(file);
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    String getFileName(String url) {
        return url.substring(url.lastIndexOf(File.separatorChar) + 1, url.length());
    }

    private void dataDownloadComplete() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                ConstantsMethods.cancleProgessDialog();
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MySyncDataActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                sweetAlertDialog.setTitleText("Done");
                sweetAlertDialog.setContentText("Data Sync completed.");
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a");
                        String lastSyncedDate = df.format(calendar.getTime());
                        sweetAlertDialog.cancel();
                        Intent intent = new Intent();
                        intent.putExtra("lastSynced", lastSyncedDate);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                });
                sweetAlertDialog.show();
            }
        });

    }

    private void showError(String title, String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MySyncDataActivity.this, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText(title);
                sweetAlertDialog.setContentText(message);
                sweetAlertDialog.show();
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                });
            }
        });


    }

    private void showErrorWithFinish(String title, String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MySyncDataActivity.this, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText(title);
                sweetAlertDialog.setContentText(message);
                sweetAlertDialog.show();
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        finish();
                    }
                });
            }
        });


    }

    private void showDataDownloadError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ConstantsMethods.cancleProgessDialog();
                EasySP.init(MySyncDataActivity.this).putBoolean(ConstantsEasySP.SP_IS_DATA_DOWNLOAD, false);
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MySyncDataActivity.this, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText("Data download fail");
                sweetAlertDialog.setContentText("Try again or contact admin");
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        finish();
                    }
                });
                sweetAlertDialog.show();
            }
        });
    }


}
