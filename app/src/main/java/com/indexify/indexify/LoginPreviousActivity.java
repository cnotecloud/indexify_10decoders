package com.indexify.indexify;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.indexify.indexify.api.LoginApi;
import com.indexify.indexify.api.UserApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.ResponsePojo;
import com.indexify.indexify.pojo.TokenPojo;
import com.indexify.indexify.pojo.UserInfoPojo;
import com.white.easysp.EasySP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class LoginPreviousActivity extends AppCompatActivity implements View.OnClickListener {
    EditText txtUserName;
    CheckBox chkLoginWithOtp;
    Button btnNext;
    TextView tvSignUp;
    TextView textView;
    TextView tvDontHaveAccount;
    TextInputLayout textInputLayout;
    //for font
    boolean clicked = false;
    Typeface playBold;
    Typeface playRegular;

    boolean isLoginWithOtp = false;
    boolean isDigitOnly = false;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    boolean isFromInterceptor = false;
    TextView tvSessionExpiredmsg;
    LoginButton loginButton;
    LinearLayout facebookLogin;
    CallbackManager callbackManager;
    LinearLayout txtSignInWithEmail;
    LinearLayout llSignInWithEmail;
    TextView tvFacebookText;
    TextView tvGoogleText;
    TextView tvSignIn;
    LinearLayout googleLogin;
    SignInButton btnSignInWithGoogle;
    GoogleSignInClient mGoogleSignInClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookSDKInitialize();
        setContentView(R.layout.activity_login_previous);
        isFromInterceptor = getIntent().getBooleanExtra("fromInterceptor", false);
        getControls();


    }

    private void getControls() {
        checkAndRequestPermissions();
        //get font
        playBold = Typeface.createFromAsset(LoginPreviousActivity.this.getAssets(),
                "fonts/play_bold.ttf");

        playRegular = Typeface.createFromAsset(LoginPreviousActivity.this.getAssets(),
                "fonts/play_regular.ttf");
        txtUserName = (EditText) findViewById(R.id.txtUserName);
        chkLoginWithOtp = (CheckBox) findViewById(R.id.chkLoginWithOtp);
        btnNext = (Button) findViewById(R.id.btnNext);
        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
       // textView = (TextView) findViewById(R.id.textView);
        tvSessionExpiredmsg = (TextView) findViewById(R.id.tvSessionExpiredmsg);
        tvDontHaveAccount = (TextView) findViewById(R.id.tvDontHaveAccount);
        loginButton = (LoginButton) findViewById(R.id.loginButton);
        btnSignInWithGoogle = (SignInButton) findViewById(R.id.btnSignInWithGoogle);
        facebookLogin = (LinearLayout) findViewById(R.id.facebookLogin);
        facebookLogin.setOnClickListener(LoginPreviousActivity.this);
        //facebookLogin.setVisibility(View.VISIBLE);
        googleLogin =(LinearLayout) findViewById(R.id.googleLogin);
        googleLogin.setOnClickListener(LoginPreviousActivity.this);

        llSignInWithEmail = (LinearLayout)findViewById(R.id.llSignInWithEmail);
        txtSignInWithEmail = (LinearLayout)findViewById(R.id.txtSignInWithEmail);
        txtSignInWithEmail.setOnClickListener(LoginPreviousActivity.this);

        chkLoginWithOtp.setTypeface(playRegular);
//        textView.setTypeface(playBold);
        tvDontHaveAccount.setTypeface(playRegular);
        tvSignUp.setTypeface(playRegular);
        btnNext.setTypeface(playBold);

        textInputLayout = (TextInputLayout) findViewById(R.id.textInputLayout);
        textInputLayout.setTypeface(playRegular);

        tvFacebookText = (TextView) findViewById(R.id.tvFacebookText);
        tvFacebookText.setTypeface(playRegular);
        tvGoogleText = (TextView)findViewById(R.id.tvGoogleText);
        tvSignIn = (TextView)findViewById(R.id.tvSignIn);
        tvSignIn.setTypeface(playRegular);
        tvGoogleText.setTypeface(playRegular);

        //initialization of Click listners

        btnNext.setOnClickListener(LoginPreviousActivity.this);
        tvSignUp.setOnClickListener(LoginPreviousActivity.this);
        loginButton.setOnClickListener(LoginPreviousActivity.this);
        btnSignInWithGoogle.setOnClickListener(LoginPreviousActivity.this);

        if (isFromInterceptor == true) {
            tvSessionExpiredmsg.setVisibility(View.VISIBLE);
        } else {
            tvSessionExpiredmsg.setVisibility(View.GONE);
        }
        getData();
    }

    private void getData() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .requestProfile()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.tvSignUp) {
            if (ConstantsMethods.isConnected(LoginPreviousActivity.this)) {
                Intent intent = new Intent(LoginPreviousActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        } else if (view.getId() == R.id.btnNext) {
            if (ConstantsMethods.isConnected(LoginPreviousActivity.this)) {
                String username = "";
                username = txtUserName.getText().toString().trim();

                if (username.length() == 0) {
                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
                    sweetAlertDialog.setTitleText("Empty Data");
                    sweetAlertDialog.setContentText("Please Enter Mobile or Email");
                    sweetAlertDialog.show();
                } else {
                    if (chkLoginWithOtp.isChecked()) {
                        isLoginWithOtp = true;
                    } else {
                        isLoginWithOtp = false;
                    }

                    if (Patterns.PHONE.matcher(username).matches()) {
                        isDigitOnly = true;
//                        if (ConstantsMethods.checkPhoneLib(username)) {
                        //means user entered Mobile number
                        sendDataToServer(username);

//                        } else {
//                            //show invalid mobile number message
//                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
//                            sweetAlertDialog.setTitleText("Invalid !");
//                            sweetAlertDialog.setContentText("Please Enter Valid Mobile Number");
//                            sweetAlertDialog.show();
//                        }


                    } else {
                        isDigitOnly = false;

                        if (org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(username)) {
                            //means user entered email id
                            sendDataToServer(username);

                        } else {
                            //means user entered username
                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
                            sweetAlertDialog.setTitleText("Invalid !");
                            sweetAlertDialog.setContentText("Please Enter Valid Email Id or Phone Number");
                            sweetAlertDialog.show();
                        }
                    }


                }

            }
        } else if (view.getId() == R.id.facebookLogin) {

            if (ConstantsMethods.isConnected(LoginPreviousActivity.this)) {
                loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
                loginButton.performClick();
                getLoginDetails(loginButton);
            }
        }else if (view.getId() == R.id.txtSignInWithEmail) {

           // llSignInWithEmail.setVisibility(View.VISIBLE);
        }else if (view.getId() == R.id.googleLogin) {
            if (ConstantsMethods.isConnected(LoginPreviousActivity.this)){
                btnSignInWithGoogle.performClick();
                clicked = true;
                googleSignIn();
            }
        }


    }
    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent,100);
    }

    private void sendDataToServer(String userName) {
        try {
            ConstantsMethods.showProgessDialog(LoginPreviousActivity.this, "Authenticating...");
            JsonObject jsonUserName = new JsonObject();
            jsonUserName.addProperty("username", userName);
            Retrofit retrofit;
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

            retrofit = builder.build();


            LoginApi loginApiStep1 = retrofit.create(LoginApi.class);
            Single<TokenPojo> responseStep1;
            if (isLoginWithOtp == true) {
                responseStep1 = loginApiStep1.getStep1TokenWithTagOtp(jsonUserName);
            } else {
                responseStep1 = loginApiStep1.getStep1Token(jsonUserName);
            }

            responseStep1.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<TokenPojo>() {
                @Override
                public void onSuccess(TokenPojo tokenPojo) {
                    if (tokenPojo != null) {
                        if (tokenPojo.getAccessToken() != null) {
                            ConstantsMethods.cancleProgessDialog();
                            Intent intent = new Intent(LoginPreviousActivity.this, LoginActivity.class);
                            intent.putExtra("accessToken", tokenPojo.getAccessToken());
                            intent.putExtra("isLoginWithOtp", isLoginWithOtp);
                            intent.putExtra("isDigitOnly", isDigitOnly);
                            intent.putExtra("userName", userName);
                            startActivity(intent);


                        }

                    }

                }

                @Override
                public void onError(Throwable e) {
                    try {
                        HttpException ex = (HttpException) e;
                        String er = ex.response().errorBody().string();
                        ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                        if (responsePojo != null) {
                            if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.INVALID_USERNAME_PASSWORD) {
                                showError("Unauthorized", "Please check email and password.");
                            } else if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.EMAIL_NOT_VERIFIED) {
                                showError("User not verified", "Please verify through email and try again.");
                            } else if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.AUTHENTICATION_FAILED) {
                                showError("Authentication Failed", "Please check your email/phone number and try again.");
                            } else if (responsePojo.getStatus() == 403) {
                                showError("Not access", "Please try again or contact admin.");
                            } else if (responsePojo.getStatus() == 500) {
                                showError("Internal server error", "Something went wrong please try later.");
                            }
                        }
                    } catch (Exception e1) {
                        showError("Error", "Something went wrong please try later.");
                    } finally {
                        ConstantsMethods.cancleProgessDialog();
                    }
                }
            });

        } catch (Exception error) {
            showError("Error", "Something Went Wrong Please try again");
        }


    }

    private void showError(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginPreviousActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });

    }

    private boolean checkAndRequestPermissions() {
        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    private void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(LoginPreviousActivity.this);
        callbackManager = CallbackManager.Factory.create();
    }

    private void getLoginDetails(LoginButton loginButton) {
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                if (ConstantsMethods.isConnected(LoginPreviousActivity.this)) {
                    //facebookLogin.setVisibility(View.INVISIBLE);
                    ConstantsMethods.showProgessDialog(LoginPreviousActivity.this, "Please Wait...");
                    if ((loginResult.getAccessToken().getToken() != null) && (loginResult.getAccessToken().getToken().length() > 0)) {
                        getTokenFromServer(loginResult.getAccessToken().getToken());
                    }
                }


            }

            @Override
            public void onCancel() {
//                Toast.makeText(LoginPreviousActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                showError("Error", "Something Went Wrong");
            }

            @Override
            public void onError(FacebookException error) {
//                Toast.makeText(LoginPreviousActivity.this, "Unable to Login \n Please Try After Sometime", Toast.LENGTH_LONG).show();
                showError("Unable To Login", "Please Try After Sometime");
            }
        });

    }

    private void getTokenFromServer(String token) {

        if ((token!= null) && (token.length() > 0)) {
            JsonObject jsonObject = new JsonObject();
            if (clicked){
                jsonObject.addProperty("provider", "google");
            }else{
                jsonObject.addProperty("provider", "facebook");
            }

            jsonObject.addProperty("accessToken", token);
            Retrofit retrofit;
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

            retrofit = builder.build();
            LoginApi loginApi = retrofit.create(LoginApi.class);
            Single<TokenPojo> socialLogin = loginApi.getLoginWithFacebookOrGoogle(jsonObject);
            socialLogin.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<TokenPojo>() {
                @Override
                public void onSuccess(TokenPojo tokenPojo) {
                    EasySP.init(LoginPreviousActivity.this).putString(ConstantsEasySP.SP_TOKEN, "Bearer " + tokenPojo.getToken());
                    EasySP.init(LoginPreviousActivity.this).putString(ConstantsEasySP.SP_REFRESH_TOKEN, "Bearer " + tokenPojo.getRefreshToken());
                    //EasySP.init(LoginActivity.this).putBoolean("isLogin", true);

                    if (ConstantsMethods.isConnectedWithoutMessage(LoginPreviousActivity.this)) {
                        getUserInformation();
                    } else {
                        EasySP.init(LoginPreviousActivity.this).putBoolean(ConstantsEasySP.SP_IS_LOGIN, false);
                        EasySP.init(LoginPreviousActivity.this).remove(ConstantsEasySP.SP_TOKEN);
                        EasySP.init(LoginPreviousActivity.this).remove(ConstantsEasySP.SP_REFRESH_TOKEN);

                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginPreviousActivity.this, SweetAlertDialog.ERROR_TYPE);
                        sweetAlertDialog.setTitleText("No Internet");
                        sweetAlertDialog.setContentText("Please check internet connection.");
                        sweetAlertDialog.show();
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                restartApp();
                            }
                        });
                    }
                }

                @Override
                public void onError(Throwable e) {
                    try {
                        HttpException ex = (HttpException) e;
                        String er = ex.response().errorBody().string();
                        ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                        if (responsePojo != null) {
                            if (responsePojo.getStatus() == 401) {
                                showError("Something Went Wrong","Please Try Again");
//                                GraphRequest request = GraphRequest.newMeRequest(
//                                        loginResult.getAccessToken(),
//                                        new GraphRequest.GraphJSONObjectCallback() {
//                                            @Override
//                                            public void onCompleted(JSONObject object, GraphResponse response) {
//                                                Log.v("LoginActivity", response.toString());
//
//                                                // Application code
//                                                try {
//                                                    String email = object.getString("email");
//                                                    //String birthday = object.getString("birthday");
//                                                    Intent intent = new Intent(LoginPreviousActivity.this,SignUpActivity.class);
//                                                    intent.putExtra("email",email);
////                                                    intent.putExtra("AccessToken",loginResult.getAccessToken().getToken());
//                                                    intent.putExtra("first_name",object.getString("first_name"));
//                                                    intent.putExtra("last_name",object.getString("last_name"));
//                                                    // intent.putExtra("mobile_number",object.getString("mobile_number"));
//                                                    startActivity(intent);
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                                // 01/31/1980 format
//                                            }
//                                        });
//
//                                Bundle parameters = new Bundle();
//                                parameters.putString("fields", "id,first_name,last_name,link,email");
//                                // parameters.putString("public_profile", "id,name,link,email");
//                                request.setParameters(parameters);
//                                request.executeAsync();
////                                if (responsePojo.getResponseCode() == ConstantsErrorCodes.INVALID_USERNAME_PASSWORD) {
////                                    showError("Unauthorized", "Please check email and password.");
////                                } else if (responsePojo.getResponseCode() == ConstantsErrorCodes.EMAIL_NOT_VERIFIED) {
////                                    showError("User not verified", "Please verify through email and try again.");
////                                } else if (responsePojo.getResponseCode() == ConstantsErrorCodes.TOKEN_EXPIRED) {
////                                    showErrorwithFinish("Session Expired!", "Please try again");
////                                } else {
////                                    showError("Unauthorized", "Please check email and password.");
////                                }
                            } else if (responsePojo.getStatus() == 403) {
                                showError("Not access", "Please try again or contact admin.");
                            }else if (responsePojo.getStatus() == 404) {
                                showError("Something Went Wrong", "Please try again.");
                            } else if (responsePojo.getStatus() == 500) {
                                showError("Internal server error", "Something went wrong please try later.");
                            }
                        }
                    } catch (Exception e1) {
                        showError("Error", "Something went wrong please try later.");
                    } finally {
                        EasySP.init(LoginPreviousActivity.this).putBoolean("isLogin", false);
                        EasySP.init(LoginPreviousActivity.this).remove(ConstantsEasySP.SP_TOKEN);
                        EasySP.init(LoginPreviousActivity.this).remove(ConstantsEasySP.SP_REFRESH_TOKEN);
                        ConstantsMethods.cancleProgessDialog();
                    }

                    //ConstantsMethods.showServerError(e, LoginActivity.this);
                }
            });

        }


    }

    private void restartApp() {
       /* Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);*/

        finishAffinity();
        Intent intent = new Intent(LoginPreviousActivity.this, IntroActivity.class);
        startActivity(intent);
    }

    private void getUserInformation() {
        // get organization details
        final UserApi organizationApi = retrofit.create(UserApi.class);
        Single<UserInfoPojo> orgSingleObservable = organizationApi.getOrganizationInformation();
        orgSingleObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<UserInfoPojo>() {
                    @Override
                    public void onSuccess(@NonNull final UserInfoPojo userInfoPojo) {
                        ConstantsMethods.cancleProgessDialog();

                        if (userInfoPojo != null) {
                            String userInfoJson = new Gson().toJson(userInfoPojo);
                            EasySP.init(LoginPreviousActivity.this).put(ConstantsEasySP.SP_USER_INFO, userInfoJson);
                            EasySP.init(LoginPreviousActivity.this).put(ConstantsEasySP.SP_USER_ID, userInfoPojo.getId());
                            if (userInfoPojo.getOrganizationPojo() != null) {
                                EasySP.init(LoginPreviousActivity.this).put(ConstantsEasySP.SP_USER_ORG_ID, userInfoPojo.getOrganizationPojo().getId());
                            }
                            EasySP.init(LoginPreviousActivity.this).putBoolean(ConstantsEasySP.SP_IS_LOGIN, true);


                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginPreviousActivity.this, SweetAlertDialog.SUCCESS_TYPE).setTitleText("Congratulations.").setContentText("Login Successfully").setConfirmText("   OK   ");
                            sweetAlertDialog.show();
                            sweetAlertDialog.setCancelable(false);
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    //                                   checkAndRequestPermissions();
                                    Intent intent = new Intent(LoginPreviousActivity.this, MainActivity.class);
                                    // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            });

                            // show home screen


                           /* } else {

                                //show qr scan code
                               *//* Intent intent = new Intent(LoginActivity.this, DataDownloadActivity.class);
                                startActivity(intent);
                                finish();*//*
                            }*/
                        } else {
                            EasySP.init(LoginPreviousActivity.this).putBoolean(ConstantsEasySP.SP_IS_LOGIN, false);
                            EasySP.init(LoginPreviousActivity.this).putString(ConstantsEasySP.SP_TOKEN, "");
                            EasySP.init(LoginPreviousActivity.this).putString(ConstantsEasySP.SP_USER_INFO, "");
                            EasySP.init(LoginPreviousActivity.this).putString(ConstantsEasySP.SP_REFRESH_TOKEN, "");
                            ConstantsMethods.cancleProgessDialog();

                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginPreviousActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText("Invalid Credentials");
                            sweetAlertDialog.setContentText("Authentication fail.");
                            sweetAlertDialog.show();
                            sweetAlertDialog.setCancelable(false);
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                }
                            });
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ConstantsMethods.showServerError(e, LoginPreviousActivity.this);
                        EasySP.init(LoginPreviousActivity.this).putBoolean(ConstantsEasySP.SP_IS_LOGIN, false);
                        EasySP.init(LoginPreviousActivity.this).putString(ConstantsEasySP.SP_TOKEN, "");
                        EasySP.init(LoginPreviousActivity.this).putString(ConstantsEasySP.SP_REFRESH_TOKEN, "");
                        ConstantsMethods.cancleProgessDialog();

                    }
                });
    }

    private void showErrorwithFinish(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginPreviousActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (clicked = true) {
            if (requestCode == 100) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    if (ConstantsMethods.isConnected(LoginPreviousActivity.this)) {
                        ConstantsMethods.showProgessDialog(LoginPreviousActivity.this, "Please Wait...");
                        if ((account.getIdToken() != null) && (account.getIdToken().length() > 0)) {
                            getTokenFromServer(account.getIdToken());
                        }

                    }
                } catch (ApiException e) {
                    showError("Something Went Wrong", " Please Try Again");
                }
            }
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        LoginManager.getInstance().logOut();
        //facebookLogin.setVisibility(View.VISIBLE);


        AppEventsLogger.activateApp(LoginPreviousActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(LoginPreviousActivity.this);
    }

}
