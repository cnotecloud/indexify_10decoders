package com.indexify.indexify.breceiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.indexify.indexify.MainActivity;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.services.SendScanToServerService;

/**
 * Created by amar on 23-01-2018.
 */

public class AlarmServiceReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (ConstantsMethods.isConnectedWithoutMessage(context)) {
            Intent serviceIntent = new Intent(context, SendScanToServerService.class);
            context.startService(serviceIntent);
        }


    }
}
