package com.indexify.indexify;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.indexify.indexify.adapter.UserScanSearchAdapter;
import com.indexify.indexify.api.UserScanApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.interfaces.DeleteOrganizationScanById;
import com.indexify.indexify.pojo.UserScanGetPojo;
import com.indexify.indexify.pojo.UserScanPojo;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class MyScanSearchListActivity extends AppCompatActivity implements DeleteOrganizationScanById {

    RecyclerView rvMyScanList;
    LinearLayoutManager mLayoutManager;
    ProgressBar progressbar;
    List<UserScanPojo> alSearchUserScans = new ArrayList<>();
    UserScanSearchAdapter adapter = null;
    SearchView search;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 5;
    int page = 0;
    String queryText = "";
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_scan_search_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getControls();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getControls() {


        queryText = getIntent().getStringExtra("searchText");
        resetValues();
        rvMyScanList = (RecyclerView) findViewById(R.id.rvMyScanList);
        mLayoutManager = new LinearLayoutManager(MyScanSearchListActivity.this, LinearLayoutManager.VERTICAL, false);
        rvMyScanList.setHasFixedSize(true);
        rvMyScanList.setLayoutManager(mLayoutManager);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.INVISIBLE);

        rvMyScanList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = rvMyScanList.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (ConstantsMethods.isConnected(MyScanSearchListActivity.this)) {
                                SearchMoreUserScans();
                            }
                        }
                    });
                    // Do something
                    loading = true;
                }
            }
        });

        if (ConstantsMethods.isConnected(MyScanSearchListActivity.this)) {
            getData();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_scan_search_list_activity, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search = (SearchView) menu.findItem(R.id.action_search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() < 3) {
                    Toast.makeText(MyScanSearchListActivity.this, "Enter minimum 3 chars", Toast.LENGTH_SHORT).show();
                } else {
                    // check internet connectin
                    if (ConstantsMethods.isConnected(MyScanSearchListActivity.this)) {
                        resetValues();
                        queryText = query;
                        searchUserScanContaining(query);
                    }
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                /*if (newText.length() == 0) {
                    rvUserList.setAdapter(adapter);
                }*/
                return true;
            }
        });
        return true;
    }

    private void searchUserScanContaining(String query) {
        if (ConstantsMethods.isConnected(MyScanSearchListActivity.this)) {
            try {
                ConstantsMethods.showProgessDialogWithCancelable(MyScanSearchListActivity.this, "Searching...");

                UserScanApi userScanApi = retrofit.create(UserScanApi.class);
                Observable<UserScanGetPojo> observable = userScanApi.searchUserScanContaining(query.toLowerCase(), ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_USER_SCAN_LOADING_SIZE, 0);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserScanGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(UserScanGetPojo userScanGetPojo) {
                                ConstantsMethods.cancleProgessDialog();

                                if (userScanGetPojo != null && userScanGetPojo.getEmbedded().getUserScans().size() > 0) {

                                    alSearchUserScans.addAll(userScanGetPojo.getEmbedded().getUserScans());
                                    adapter = new UserScanSearchAdapter(alSearchUserScans, MyScanSearchListActivity.this);
                                    rvMyScanList.setAdapter(adapter);

                                } else {
                                    // show not found message
                                    showErrorWithoutFinish("Not found", "User scan data not found", 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                                showErrorWithoutFinish("Not found", "User scan data not found", 1);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {

            } finally {

            }
        }
    }

    private void SearchMoreUserScans() {
        if (ConstantsMethods.isConnectedWithoutMessage(MyScanSearchListActivity.this)) {
            progressbar.setVisibility(View.VISIBLE);
            try {
                UserScanApi userScanApi = retrofit.create(UserScanApi.class);
                Observable<UserScanGetPojo> observable = userScanApi.searchUserScanContaining(queryText, ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_USER_SCAN_LOADING_SIZE, ++page);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserScanGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(UserScanGetPojo userScanGetPojo) {
                                progressbar.setVisibility(View.INVISIBLE);
                                if (userScanGetPojo != null && userScanGetPojo.getEmbedded().getUserScans().size() > 0) {
                                    alSearchUserScans.addAll(userScanGetPojo.getEmbedded().getUserScans());
                                        adapter = new UserScanSearchAdapter(alSearchUserScans, MyScanSearchListActivity.this);
                                        rvMyScanList.setAdapter(adapter);

                                } else {
                                    // show not found message
                                    //showError("Not found", "User scan data not found", 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                progressbar.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {
                progressbar.setVisibility(View.INVISIBLE);
            } finally {

            }

        }
    }

    public void getData() {
        //get my scan from server
        List<UserScanPojo> alUserScans = (List<UserScanPojo>) getIntent().getSerializableExtra("userScans");

        if (alUserScans != null && alUserScans.size() > 0) {

            alSearchUserScans.addAll(alUserScans);
            adapter = new UserScanSearchAdapter(alUserScans, MyScanSearchListActivity.this);
            rvMyScanList.setAdapter(adapter);

        } else {
            // show not found message
            //showError("Not found", "User scan data not found", 1);
        }
    }

    private void showError(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MyScanSearchListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        });

    }

    private void showErrorWithoutFinish(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MyScanSearchListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                // finish();
            }
        });

    }

    public void resetValues() {
        alSearchUserScans.clear();
        adapter = null;
        page = 0;
        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        loading = true;
        previousTotal = 0;
        visibleThreshold = 5;
    }

    public void refreshList(List alRefreshSearchUserScans) {
        adapter = new UserScanSearchAdapter(alRefreshSearchUserScans, MyScanSearchListActivity.this);
        adapter.notifyDataSetChanged();
        rvMyScanList.setAdapter(adapter);
    }


    @Override
    public void deleteOrganizationScanById(String orgScanId) {
        for (int i = 0; i < alSearchUserScans.size(); i++) {
            if (alSearchUserScans.get(i).getId().equalsIgnoreCase(orgScanId)) {
                alSearchUserScans.remove(i);
                adapter = new UserScanSearchAdapter(alSearchUserScans, MyScanSearchListActivity.this);
                adapter.notifyDataSetChanged();
                rvMyScanList.setAdapter(adapter);
            }
        }


    }
}
