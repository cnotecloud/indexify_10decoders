package com.indexify.indexify;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.indexify.indexify.constants.ConstantsEasySP;
import com.white.easysp.EasySP;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends AppCompatActivity {
    ImageView imgText;
    ImageView imgLogo;
    LinearLayout llContainer;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        imgText = (ImageView) findViewById(R.id.imgText);
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        llContainer = (LinearLayout) findViewById(R.id.llContainer);
        getControls();

    }

    private void getControls() {

        Animation animation;
        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_donw);


        /*imgText.startAnimation(animation);
        imgText.startAnimation(animation);*/
        llContainer.startAnimation(animation);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    sleep(2500);

                    getData();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private void getData() {
        // check intro
        //checkAndRequestPermissions();
        boolean isIntro = EasySP.init(SplashActivity.this).getBoolean(ConstantsEasySP.SP_INTRO_FLAG);
        boolean isLogin = EasySP.init(SplashActivity.this).getBoolean(ConstantsEasySP.SP_IS_LOGIN);


        if (isIntro) {
            if (isLogin) {
                finish();
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
            } else {

                finish();
                Intent intent = new Intent(SplashActivity.this, LoginPreviousActivity.class);
                startActivity(intent);
            }
        } else {
            finish();
            Intent intent = new Intent(SplashActivity.this, IntroActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            String info = data.getStringExtra("scanned_info");
            Toast.makeText(this, "" + info, Toast.LENGTH_SHORT).show();
        }
    }
    private boolean checkAndRequestPermissions() {
        int permissionCamera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }

        return true;
    }
}
