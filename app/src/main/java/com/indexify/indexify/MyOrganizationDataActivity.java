package com.indexify.indexify;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.indexify.indexify.adapter.MyBreadScrumbAdapter;
import com.indexify.indexify.api.NodeApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsBreadScrumb;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.constants.ConstantsTables;
import com.indexify.indexify.database.DatabaseAccess;
import com.indexify.indexify.fragments.FragmentMyNode;
import com.indexify.indexify.fragments.FragmentMyProducts;
import com.indexify.indexify.fragments.FragmentNode;
import com.indexify.indexify.fragments.FragmentProduct;
import com.indexify.indexify.interfaces.SetBreadScrumb;
import com.indexify.indexify.pojo.BreadScrumbPojo;
import com.indexify.indexify.pojo.NodePojo;
import com.indexify.indexify.pojo.NodesGetPojo;
import com.indexify.indexify.pojo.ResponsePojo;
import com.white.easysp.EasySP;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class MyOrganizationDataActivity extends AppCompatActivity implements SetBreadScrumb, View.OnClickListener {

    List<BreadScrumbPojo> alBreadScrumb = new ArrayList<>();
    MyBreadScrumbAdapter adapter = null;
    RecyclerView rvMyBreadScrumb;
    FloatingActionButton fabSync;
    LinearLayoutManager mLayoutManager;
    SearchView search;
    TextView tvLastSyncDate;
    long orgId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_organization_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra("orgName"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getControls();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {

            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getControls() {
        fabSync = (FloatingActionButton) findViewById(R.id.fabSync);
        fabSync.setOnClickListener(this);
        rvMyBreadScrumb = (RecyclerView) findViewById(R.id.rvMyBreadScrumb);
        tvLastSyncDate = (TextView) findViewById(R.id.tvLastSyncDate);
        mLayoutManager = new LinearLayoutManager(MyOrganizationDataActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvMyBreadScrumb.setHasFixedSize(true);
        rvMyBreadScrumb.setLayoutManager(mLayoutManager);

        adapter = new MyBreadScrumbAdapter(alBreadScrumb, MyOrganizationDataActivity.this);
        rvMyBreadScrumb.setAdapter(adapter);
        String lastSynced = EasySP.init(MyOrganizationDataActivity.this).getString("lastSyncedDate");
        tvLastSyncDate.setText(lastSynced);


       getData();

    }

    private void getData() {
     orgId = getIntent().getLongExtra("orgId",0);
        attachData((int) orgId);
    }

    private void attachData(int orgId) {
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(MyOrganizationDataActivity.this);
        databaseAccess.open();
        List<NodePojo> alRootNodes = databaseAccess.getMyRootNodes();
        databaseAccess.close();

        if (alRootNodes.size() > 0) {
            int i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
            FragmentMyNode fragmentMyNode = new FragmentMyNode();
            Bundle bundle = new Bundle();
            bundle.putSerializable("nodes", (Serializable) alRootNodes);
            bundle.putInt("orgId", orgId);
            bundle.putString("nodeName", "Home");
            bundle.putString("nodeId", String.valueOf(i));
            fragmentMyNode.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.myFrameLayout, fragmentMyNode, String.valueOf(i));
            // transaction.replace(R.id.frameLayout, fragmentNode, "root");
            transaction.commitAllowingStateLoss();

        } else if (alRootNodes.size() == 0) {
            Toast.makeText(this, "not found", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void setBreadScrumb(BreadScrumbPojo breadScrumb) {
        alBreadScrumb.add(breadScrumb);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void removeBreadScrumb(String tag) {
        for (int i = 0; i < alBreadScrumb.size(); i++) {
            if (alBreadScrumb.get(i).getTag().equalsIgnoreCase(tag)) {
                alBreadScrumb.remove(i);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void removeBreadScrumbOnClik(String tag) {
        try {
            int to = Integer.parseInt(tag);

            for (int from = ConstantsBreadScrumb.BREADSCRUMB_INIT; from > to; from--) {

                Fragment fragmentNode = getSupportFragmentManager().findFragmentByTag(String.valueOf(from));
                if (fragmentNode != null) {
                    try {
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.remove(fragmentNode);
                        transaction.commit();
                        getSupportFragmentManager().popBackStack();
                    } catch (Exception e) {

                    }
                }
            }
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_my_organization_data_activity, menu);

        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        search = (SearchView) menu.findItem(R.id.action_search).getActionView();

        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() < 3) {
                    Toast.makeText(MyOrganizationDataActivity.this, "Enter minimum 3 chars", Toast.LENGTH_SHORT).show();
                } else {
                    DatabaseAccess databaseAccess = DatabaseAccess.getInstance(MyOrganizationDataActivity.this);
                    databaseAccess.open();
                    //List<ProductPojo> alProducts = databaseAccess.getProductsByLeafNode(nodePojo.getId());
                    int totalCount = databaseAccess.getTotalCountProductsSearch(query.toLowerCase().trim());
                    databaseAccess.close();

                    if (totalCount > 0) {
                        int i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
                        FragmentMyProducts fragmentMyProducts = new FragmentMyProducts();
                        Bundle bundle = new Bundle();
                        bundle.putString("nodeName", query.toLowerCase());
                        bundle.putString("nodeId", "");
                        bundle.putBoolean("found", true);
                        bundle.putString("type", "search");
                        fragmentMyProducts.setArguments(bundle);
                        FragmentManager manager = MyOrganizationDataActivity.this.getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        // transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        transaction.add(R.id.myFrameLayout, fragmentMyProducts, String.valueOf(i));
                        transaction.addToBackStack(String.valueOf(fragmentMyProducts));
                        transaction.commit();
                        hideSoftKeyboard(MyOrganizationDataActivity.this);
                        search.onActionViewCollapsed();


                    } else if (totalCount == 0) {
                        Toast.makeText(MyOrganizationDataActivity.this, "not found", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }



                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                /*if (newText.length() == 0) {
                    rvUserList.setAdapter(adapter);
                }*/
                return true;
            }
        });
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fabSync){
            if (ConstantsMethods.isConnected(MyOrganizationDataActivity.this)){
                Intent intent = new Intent(MyOrganizationDataActivity.this,MySyncDataActivity.class);
                intent.putExtra("orgId",orgId);
                startActivityForResult(intent,203);

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 203  && resultCode == Activity.RESULT_OK){
            if (data != null){
                EasySP.init(MyOrganizationDataActivity.this).putString("lastSyncedDate",data.getStringExtra("lastSynced"));
                tvLastSyncDate.setText(data.getStringExtra("lastSynced"));
                getData();
            }
        }
    }
    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
