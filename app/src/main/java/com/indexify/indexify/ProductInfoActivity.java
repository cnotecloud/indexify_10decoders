package com.indexify.indexify;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.indexify.indexify.api.ProductApi;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.pojo.ProductPojo;
import com.indexify.indexify.pojo.ResponsePojo;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.io.Serializable;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class ProductInfoActivity extends AppCompatActivity implements View.OnClickListener {
    ImageButton cancelButton;
    ImageView imgProduct;
    TextView tvProductName;
    ViewPager pagerImages;
    PagerAdapter pagerImagesAdapter;
    DotsIndicator dotsIndicator;
    LinearLayout llData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_info);

        getControls();
    }

    private void getControls() {
        pagerImages = (ViewPager)findViewById(R.id.pagerImages);
        cancelButton = (ImageButton)findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(ProductInfoActivity.this);
        imgProduct = (ImageView) findViewById(R.id.imgProduct);
        tvProductName = (TextView) findViewById(R.id.tvProductName);
        dotsIndicator = (DotsIndicator) findViewById(R.id.dots_indicator);

        llData = (LinearLayout) findViewById(R.id.llData);

        if (ConstantsMethods.isConnected(ProductInfoActivity.this)) {
            getData();
        } else {
            finish();
        }
    }

    private void getData() {
        if (ConstantsMethods.isConnectedWithoutMessage(ProductInfoActivity.this)) {
            String productId = getIntent().getStringExtra("id");
            long orgId = NumberUtils.toLong(getIntent().getStringExtra("orgId"));

            try {
                ConstantsMethods.showProgessDialogWithCancelable(ProductInfoActivity.this, "Loading proudct...");


                ProductApi productApi = retrofit.create(ProductApi.class);
                Single<ProductPojo> observable = productApi.getProductById(orgId,productId, ConstantsProjection.PROJECTION_DETAIL);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<ProductPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(ProductPojo productPojo) {
                                ConstantsMethods.cancleProgessDialog();
                                if (productPojo != null) {
                                    //set controls
                                    if ((productPojo.getControls() != null) && (productPojo.getControls().size() > 0)) {
                                        for (int i = 0; i < productPojo.getControls().size(); i++) {


                                            View controlView = getLayoutInflater().inflate(R.layout.layout_controls, null, false);

                                            TextView tvTitle = controlView.findViewById(R.id.tvTitle);
                                            TextView tvValue = controlView.findViewById(R.id.tvValue);

                                            tvTitle.setText(WordUtils.capitalize(productPojo.getControls().get(i).getName()));
                                            tvValue.setText(String.valueOf(productPojo.getControls().get(i).getValue()));

                                            llData.addView(controlView);

                                        }
                                    }
                                    //set viewpager
                                    pagerImagesAdapter = new ImagesSlidePagerAdapter(productPojo);
                                    pagerImages.setAdapter(pagerImagesAdapter);
                                    if ((productPojo.getName() != null) && (productPojo.getName().length() > 0)){
                                        tvProductName.setText(productPojo.getName());
                                    }
                                    dotsIndicator.setViewPager(pagerImages);


                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                                try {
                                    HttpException ex = (HttpException) e;
                                    String er = ex.response().errorBody().string();
                                    ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                                    if (responsePojo != null) {
                                        if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.INVALID_USERNAME_PASSWORD) {
                                            showError("Unauthorized", "Please check email and password.");
                                        } else if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.EMAIL_NOT_VERIFIED) {
                                            showError("User not verified", "Please verify through email and try again.");
                                        } else if (responsePojo.getStatus() == 403) {
                                            showError("Not access", "Please try again or contact admin.");
                                        } else if (responsePojo.getStatus() == 500) {
                                            showError("Intermal server error", "Something went wrong please try later.");
                                        }
                                    }
                                } catch (Exception e1) {
                                    showError("Error", "Something went wrong please try later.");
                                } finally {
                                    ConstantsMethods.cancleProgessDialog();
                                }
                            }

                        });


            } catch (Exception e) {

            }

        } else {
            finish();
        }
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.cancelButton){
            finish();
        }

    }
    public class ImagesSlidePagerAdapter extends PagerAdapter {
        ProductPojo productPojo;

        public ImagesSlidePagerAdapter(ProductPojo productPojo) {

            this.productPojo = productPojo;

            // mLayoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        }


        @Override
        public int getCount() {
            return productPojo.getImgUrls().size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = getLayoutInflater().inflate(R.layout.image_viewpager, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.product_image);

            if ((productPojo.getImgUrls() != null) && (productPojo.getImgUrls().size() > 0)) {
                if (ConstantsMethods.isConnectedWithoutMessage(ProductInfoActivity.this)) {
                    Glide.with(ProductInfoActivity.this).load(productPojo.getImgUrls().get(position)).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);
                }
            }


            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ((productPojo.getImgUrls() != null) && (productPojo.getImgUrls().size() > 0)) {
                        if (ConstantsMethods.isConnectedWithoutMessage(ProductInfoActivity.this)) {
                            Intent intent = new Intent(ProductInfoActivity.this, VisualetActivity.class);
                            //intent.putExtra("productPojo", productPojo);
                            intent.putExtra("imgUrls", (Serializable) productPojo.getImgUrls());
                            intent.putExtra("imagePosition", position);
                            startActivity(intent);
                        }
                    } else {
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ProductInfoActivity.this, SweetAlertDialog.ERROR_TYPE);
                        sweetAlertDialog.setTitleText("No Image");
                        sweetAlertDialog.setContentText("No Image Found !");
                        sweetAlertDialog.show();
                    }

                }
            });

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
        private void showError(String title, String message) {
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ProductInfoActivity.this, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText(title);
            sweetAlertDialog.setContentText(message);
            sweetAlertDialog.show();
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismissWithAnimation();
                }
            });

        }
}
