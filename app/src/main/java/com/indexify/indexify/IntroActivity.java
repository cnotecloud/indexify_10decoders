package com.indexify.indexify;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.fragments.FragmentIntro1;
import com.indexify.indexify.fragments.FragmentIntro2;
import com.indexify.indexify.fragments.FragmentIntro3;
import com.indexify.indexify.fragments.FragmentIntro4;
import com.white.easysp.EasySP;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by amar on 08-01-2018.
 */

public class IntroActivity extends AppIntro {
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        boolean isIntro = EasySP.init(IntroActivity.this).getBoolean(ConstantsEasySP.SP_INTRO_FLAG);

        if (isIntro) {
            finish();
            Intent intent = new Intent(IntroActivity.this, SplashActivity.class);
            startActivity(intent);
        } else {
            FragmentIntro1 fragmentIntro1 = new FragmentIntro1();
            FragmentIntro2 fragmentIntro2 = new FragmentIntro2();
            FragmentIntro3 fragmentIntro3 = new FragmentIntro3();
            FragmentIntro4 fragmentIntro4 = new FragmentIntro4();
            addSlide(fragmentIntro1);
            addSlide(fragmentIntro2);
            addSlide(fragmentIntro3);
            addSlide(fragmentIntro4);
            setDepthAnimation();
            setDoneText("Let's Start");

        }
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.

        finish();
        EasySP.init(IntroActivity.this).putBoolean(ConstantsEasySP.SP_INTRO_FLAG, true);
        Intent intent = new Intent(IntroActivity.this, LoginPreviousActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.

        finish();
        EasySP.init(IntroActivity.this).putBoolean(ConstantsEasySP.SP_INTRO_FLAG, true);
        Intent intent = new Intent(IntroActivity.this, LoginPreviousActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }


}
