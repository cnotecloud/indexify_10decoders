package com.indexify.indexify;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.indexify.indexify.api.ImageApi;
import com.indexify.indexify.api.NodeApi;
import com.indexify.indexify.api.ProductApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.constants.ConstantsTables;
import com.indexify.indexify.database.DatabaseAccess;
import com.indexify.indexify.pojo.NodePojo;
import com.indexify.indexify.pojo.NodesGetPojo;
import com.indexify.indexify.pojo.ProductPojo;
import com.indexify.indexify.pojo.ProductsGetPojo;
import com.indexify.indexify.pojo.ResponsePojo;
import com.white.easysp.EasySP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.indexify.indexify.interceptor.ApplicationController.executorService;
import static com.indexify.indexify.interceptor.ApplicationController.retrofit;


public class DataDownloadActivity extends AppCompatActivity {
    TextView tvCount;
    TextView tvLoading;
    TextView tvTotal;
    TextView tvName;
    ImageView imgLoading;
    TextView tvCountProducts;
    TextView tvTotalProducts;
    boolean isImageDownloadError = false;
    long orgId;
    String orgName = "";
    int displayCount = 1;
    int totalSize = 50;
    boolean isError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_download);
        orgName = getIntent().getStringExtra("orgName");
        this.setFinishOnTouchOutside(false);
        getControls();
    }

    private void getControls() {
        imgLoading = (ImageView) findViewById(R.id.imgLoading);
        tvCount = (TextView) findViewById(R.id.tvCount);
        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvName = (TextView) findViewById(R.id.tvName);
        tvLoading = (TextView) findViewById(R.id.tvLoading);
        tvCountProducts = (TextView) findViewById(R.id.tvCountProducts);
        tvTotalProducts = (TextView) findViewById(R.id.tvTotalProducts);
        orgId = getIntent().getLongExtra("orgId", 0);
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(2000); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        tvName.startAnimation(anim);


        Glide.with(DataDownloadActivity.this).load(R.drawable.loading)
                .priority(Priority.IMMEDIATE)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imgLoading);

        if (ConstantsMethods.isConnectedWithoutMessage(DataDownloadActivity.this)) {
            clearTables();
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    getData();
                }
            });

        } else {
            ConstantsMethods.cancleProgessDialog();
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(DataDownloadActivity.this, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText("No Internet");
            sweetAlertDialog.setContentText("Please check Intenet connection.");
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    finish();
                }
            });
            sweetAlertDialog.show();
        }
    }

    private void clearTables() {
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(DataDownloadActivity.this);
        databaseAccess.open();
        databaseAccess.clearMyNodesAndMyProductsTables();
        databaseAccess.close();
    }

    private void getData() {
        try {


            //get Nodes

            final NodeApi nodeApi = retrofit.create(NodeApi.class);
            Observable<NodesGetPojo> getMyNodes = nodeApi.getMyNodesByOrganizationId(orgId, ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_MY_PRODUCT_GET_SIZE, Constants.DEFAULT_PAGE);
            getMyNodes.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<NodesGetPojo>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(NodesGetPojo nodesGetPojo) {
                    if (nodesGetPojo != null) {
                        //insert Nodes to DataBase
                        insertMyNodes(nodesGetPojo);
                        //get Products

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tvName.setText("Products");
                                    }
                                });
                                int page = 0;
                                int size = 50;

                                while (true) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (displayCount == 1) {
                                                tvTotal.setText(displayCount + " - " + totalSize + " products )");
                                            } else {
                                                displayCount = displayCount + 49;
                                                totalSize = totalSize + 50;
                                                tvTotal.setText(displayCount + " - " + totalSize + " products )");
                                            }
                                        }
                                    });

                                    ProductApi productApi = retrofit.create(ProductApi.class);
                                    Call<ProductsGetPojo> getPojoCall = productApi.getMyProducts(ConstantsProjection.PROJECTION_DETAIL, size, page);
                                    try {
                                        ProductsGetPojo productsGetPojo = getPojoCall.execute().body();

                                        if (productsGetPojo != null) {
                                            if (productsGetPojo.getEmbedded().getProducts() != null) {
                                                if (productsGetPojo.getEmbedded().getProducts().size() > 1) {
                                                    insertProduct(productsGetPojo);
                                                    page++;
                                                } else if (productsGetPojo.getEmbedded().getProducts().size() == 0) {
                                                    break;
                                                }
                                            }

                                        }


                                    } catch (Exception e) {
                                        isError = true;
                                        break;
                                    }

                                    displayCount++;
                                }

                                if (isError == true) {
                                    showErrorWithFinish("Error", "Something went Wrong please try again");

                                } else {
                                    DatabaseAccess databaseAccess = DatabaseAccess.getInstance(DataDownloadActivity.this);
                                    databaseAccess.open();
                                    int totalImages = databaseAccess.getImagesOrNotForProducts();
                                    databaseAccess.close();

                                    if (totalImages == 0) {
                                        dataDownloadComplete();
                                    } else {
                                        List<ProductPojo> alProductPojos = new ArrayList<>();
                                        databaseAccess.open();
                                        alProductPojos = databaseAccess.getProductsForImageDownload();
                                        databaseAccess.close();

                                        int count = alProductPojos.size();

                                        //for images download
                                        //declare okHttp builder and attach it to retrofit builder
                                        if (alProductPojos.size() > 0) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    tvName.setText("Product Images");
                                                    tvTotal.setText(" / " + count + " )");
                                                }
                                            });
                                            Retrofit retrofit;
                                            OkHttpClient okhttpBulider = new OkHttpClient.Builder()
                                                    .connectTimeout(600, TimeUnit.SECONDS)
                                                    .readTimeout(600, TimeUnit.SECONDS)
                                                    .build();

                                            exitLabel:
                                            for (int j = 0; j < alProductPojos.size(); j++) {
                                                int finalJ = j;
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        tvCount.setText(String.valueOf(1 + finalJ));
                                                    }
                                                });
                                                ProductPojo productPojo = alProductPojos.get(j);
                                                List<String> alNewLocalImagePaths = new ArrayList<>();
                                                //check img urls are not null
                                                if ((productPojo.getImgUrls() != null) && (productPojo.getImgUrls().size() > 0)) {
                                                    //get images from list using for loop
                                                    for (int k = 0; k < productPojo.getImgUrls().size(); k++) {
                                                        try {

                                                            if ((productPojo.getImgUrls().get(k) != null) && (productPojo.getImgUrls().get(k).length() > 0)) {
                                                                String fileName = getFileName(productPojo.getImgUrls().get(k)).trim();
                                                                String endPoint = productPojo.getImgUrls().get(k).substring(0, productPojo.getImgUrls().get(k).length() - fileName.length());
                                                                Retrofit.Builder builder = new Retrofit.Builder()
                                                                        .baseUrl(endPoint)                         // set Base url (Endpoint)
                                                                        .client(okhttpBulider)                                   // adding the okhttp clint in retrofit object
                                                                        .addConverterFactory(GsonConverterFactory.create())
                                                                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

                                                                retrofit = builder.build();


                                                                ImageApi imageApi = retrofit.create(ImageApi.class);

                                                                Call<ResponseBody> call = imageApi.res(fileName);
                                                                ResponseBody body = call.execute().body();


                                                                if (body != null) {
                                                                    File file = saveImage(fileName, body);
                                                                    Uri uri = Uri.fromFile(file);
                                                                    alNewLocalImagePaths.add(uri.toString());
                                                                    Log.i("uri", uri.getPath());
                                                                }
                                                            }
                                                        } catch (Exception error) {
                                                            isImageDownloadError = true;
                                                            break exitLabel;
                                                        }
                                                    }

                                                }
                                                //update to localimage in database

                                                if (alNewLocalImagePaths.size() > 0) {
                                                    DatabaseAccess databaseAccess2 = DatabaseAccess.getInstance(DataDownloadActivity.this);
                                                    databaseAccess2.open();
                                                    int x = databaseAccess2.updateLocalImageUrlOfProdcut(productPojo.getId(), alNewLocalImagePaths);
                                                    databaseAccess2.close();
                                                }
                                            }
                                        }
                                        if ((nodesGetPojo != null) && (alProductPojos.size() > 0) && (!isImageDownloadError)){
                                            dataDownloadComplete();
                                        }else{
                                            showDataDownloadError();
                                        }

                                    }
                                }
                            }
                        }).start();


                    }
                }

                @Override
                public void onError(Throwable e) {
                    try {
                        HttpException ex = (HttpException) e;
                        String er = ex.response().errorBody().string();
                        ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                        if (responsePojo != null) {
                            if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.INVALID_USERNAME_PASSWORD) {
                                showError("Unauthorized", "Please check email and password.");
                            } else if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.EMAIL_NOT_VERIFIED) {
                                showError("User not verified", "Please verify through email and try again.");
                            } else if (responsePojo.getStatus() == 403) {
                                showError("Not access", "Please try again or contact admin.");
                            } else if (responsePojo.getStatus() == 500) {
                                showError("Internal server error", "Something went wrong please try later.");
                            }
                        }
                    } catch (Exception e1) {
                        showDataDownloadError();
                    } finally {
                        ConstantsMethods.cancleProgessDialog();
                    }
                }

                @Override
                public void onComplete() {

                }
            });

        }catch (Exception error){
            showDataDownloadError();
        }
    }

    private File saveImage(String fileName, ResponseBody response) {
        // File imageDir = new File(Environment.getExternalStorageDirectory() + File.separator + "imagesx");
        //File imageDir = new File(this.getFilesDir() + File.separator + "imagesx");
        File imageDir = new File(this.getFilesDir() + File.separator + "imagesx");
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
        Log.d("sdf", "saveImage: " + imageDir + File.separator + fileName);
        File file = new File(imageDir + File.separator + fileName);

        FileOutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            inputStream = response.byteStream();
            outputStream = new FileOutputStream(file);
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    String getFileName(String url) {
        return url.substring(url.lastIndexOf(File.separatorChar) + 1, url.length());
    }

    private void showDataDownloadError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ConstantsMethods.cancleProgessDialog();
                EasySP.init(DataDownloadActivity.this).putBoolean(ConstantsEasySP.SP_IS_DATA_DOWNLOAD, false);
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(DataDownloadActivity.this, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText("Data download fail");
                sweetAlertDialog.setContentText("Try again or contact admin");
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        finish();
                    }
                });
                sweetAlertDialog.show();
            }
        });
    }

    private void insertMyNodes(NodesGetPojo nodesGetPojo) {
        try {
            for (int i = 0; i < nodesGetPojo.getEmbedded().getNodes().size(); i++) {
                DatabaseAccess databaseAccess = DatabaseAccess.getInstance(DataDownloadActivity.this);
                databaseAccess.open();
                if ((nodesGetPojo.getEmbedded().getNodes() != null) && (nodesGetPojo.getEmbedded().getNodes().size() > 0)) {
                    NodePojo nodePojo = nodesGetPojo.getEmbedded().getNodes().get(i);

                    ContentValues values = new ContentValues();
                    values.put(ConstantsTables.T_MY_NODES_ORG_ID, orgId);
                    values.put(ConstantsTables.T_MY_NODES_SERVER_ID, nodePojo.getId());
                    values.put(ConstantsTables.T_MY_NODES_DISPLAY_NAME, nodePojo.getDisplayName());
                    values.put(ConstantsTables.T_MY_NODES_PARENT_SERVER_ID, nodePojo.getParentId());

                    if (nodePojo.isLeaf()) {
                        values.put(ConstantsTables.T_MY_NODES_IS_LEAF, 1);
                    } else {
                        values.put(ConstantsTables.T_MY_NODES_IS_LEAF, 0);
                    }

                    values.put(ConstantsTables.T_MY_NODES_CREATED, nodePojo.getCreated());
                    values.put(ConstantsTables.T_MY_NODES_UPDATED, nodePojo.getUpdated());

                    long x = databaseAccess.insertMyNodes(values);
                    Log.i("myNode insert", String.valueOf(x));
                    databaseAccess.close();
                }

            }
        } catch (Exception e) {
            Log.e("node error : ", e.toString());
        }

    }


    private void dataDownloadComplete() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EasySP.init(DataDownloadActivity.this).putBoolean(ConstantsEasySP.SP_IS_DATA_DOWNLOAD, true);

                ConstantsMethods.cancleProgessDialog();
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(DataDownloadActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                sweetAlertDialog.setTitleText("Done");
                sweetAlertDialog.setContentText("Products download completed.");
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a");
                        String lastSyncedDate = df.format(calendar.getTime());
                        EasySP.init(DataDownloadActivity.this).putString("lastSyncedDate",lastSyncedDate);
                        Intent intent = new Intent(DataDownloadActivity.this, MyOrganizationDataActivity.class);
                        intent.putExtra("orgName", orgName);
                        intent.putExtra("orgId",orgId);
                        intent.putExtra("lastSynced", lastSyncedDate);
                        startActivity(intent);
                        finish();
                    }
                });
                sweetAlertDialog.show();
            }
        });
    }


    private void insertProduct(ProductsGetPojo productGetPojo) {
        Gson gson = new Gson();
        try {
            for (int i = 0; i < productGetPojo.getEmbedded().getProducts().size(); i++) {
                DatabaseAccess databaseAccess = DatabaseAccess.getInstance(DataDownloadActivity.this);
                databaseAccess.open();
                ProductPojo product = productGetPojo.getEmbedded().getProducts().get(i);
                ContentValues values = new ContentValues();
                values.put(ConstantsTables.T_MY_PRODUCTS_ORG_ID, orgId);
                values.put(ConstantsTables.T_MY_PRODUCTS_SERVER_ID, product.getId());
                values.put(ConstantsTables.T_MY_PRODUCTS_NAME, product.getName());
                values.put(ConstantsTables.T_MY_PRODUCTS_LEAF_NODE_SERVER_ID, product.getLeafId());

                String urlJsonStirng = new Gson().toJson(product.getImgUrls());
                values.put(ConstantsTables.T_MY_PRODUCTS_IMG_URLS, urlJsonStirng);

                if (product.getControls() != null) {
                    if (product.getControls().size() > 0) {
                        String controls = gson.toJson(product.getControls());
                        values.put(ConstantsTables.T_MY_PRODUCTS_CONTROLS, controls);
                    } else if (product.getControls().size() == 0) {
                        values.put(ConstantsTables.T_MY_PRODUCTS_CONTROLS, "");
                    }
                } else {
                    values.put(ConstantsTables.T_MY_PRODUCTS_CONTROLS, "");
                }
                long x = databaseAccess.insertMyProducts(values);
                databaseAccess.close();
                Log.i("product ", String.valueOf(x));
            }

        } catch (Exception e) {
            Log.e("insert product error:", " " + productGetPojo.getEmbedded().getProducts().size());
        }
    }
    private void showError(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(DataDownloadActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });

    }
    private void showErrorWithFinish(String title, String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(DataDownloadActivity.this, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText(title);
                sweetAlertDialog.setContentText(message);
                sweetAlertDialog.show();
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        finish();
                    }
                });
            }
        });


    }
}
