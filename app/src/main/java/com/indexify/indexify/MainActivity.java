package com.indexify.indexify;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.facebook.FacebookActivity;
import com.facebook.login.LoginManager;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.Gson;
import com.indexify.indexify.breceiver.AlarmServiceReceiver;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsTables;
import com.indexify.indexify.database.DatabaseAccess;
import com.indexify.indexify.pojo.OrganizationPojo;
import com.indexify.indexify.pojo.QRPojo;
import com.indexify.indexify.pojo.UserInfoPojo;
import com.indexify.indexify.services.SendScanToServerService;
import com.white.easysp.EasySP;

import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.HttpUrl;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    Button btnMyScans;
    Button btnMyViewer;
    Button btnFavourite;

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        boolean isScanShow = EasySP.init(MainActivity.this).getBoolean(ConstantsEasySP.SP_IS_SCAN_SHOW, false);
        if (!isScanShow) {
            try {
                //Intent intent = new Intent(MainActivity.this, ScanQRcodeActivity.class);
                Intent intent = new Intent(MainActivity.this, ScanQrCodeFromVisionActivity.class);
                EasySP.init(MainActivity.this).putBoolean(ConstantsEasySP.SP_IS_SCAN_SHOW, true);
                startActivityForResult(intent, 100);
            } catch (Exception e) {
                showMessage("Not found", "Scan screen not found.", 1);
            }
        }
        Uri data = getIntent().getData();
        if (data != null){
            String url = data.toString();
            try{
                if (url.length()>0) {
                    HttpUrl url1 = HttpUrl.parse(url);
                    if (url1 != null) {
                        long orgId = Long.parseLong(url1.queryParameter("id"));

                        if (orgId > 0){
                            Intent intent = new Intent(MainActivity.this, OrganizationDataActivity.class);
                            intent.putExtra("orgId",orgId);
                            startActivity(intent);
                        }
                    }
                }
            }catch(Exception error){

            }
        }

        getControls();

        if (ConstantsMethods.isConnectedWithoutMessage(MainActivity.this)) {
            Intent intent = new Intent(MainActivity.this, SendScanToServerService.class);
            startService(intent);
        }

        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/
    }

    private void getControls() {

        btnMyScans = (Button) findViewById(R.id.btnMyScans);
        btnMyViewer = (Button) findViewById(R.id.btnMyViewer);
        btnFavourite = (Button) findViewById(R.id.btnFavourite);
        btnMyScans.setOnClickListener(this);
        btnMyViewer.setOnClickListener(this);
        btnFavourite.setOnClickListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //Intent intent = new Intent(MainActivity.this, ScanQRcodeActivity.class);
                    Intent intent = new Intent(MainActivity.this, ScanQrCodeFromVisionActivity.class);
                    EasySP.init(MainActivity.this).putBoolean(ConstantsEasySP.SP_IS_SCAN_SHOW, true);
                    startActivityForResult(intent, 100);
                } catch (Exception e) {
                    showMessage("Not found", "Scan screen not found.", 1);
                }
            }
        });
        setAlarm();

        //App updater code


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
            sweetAlertDialog.setTitleText("Logout !");
            sweetAlertDialog.setContentText("Are you sure ?");
            sweetAlertDialog.setCancelText("Cancel");
            sweetAlertDialog.setConfirmText("OK");
            sweetAlertDialog.showCancelButton(true);
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.cancel();
                }
            });
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    EasySP.init(MainActivity.this).putBoolean(ConstantsEasySP.SP_IS_LOGIN, false);
                    EasySP.init(MainActivity.this).putBoolean(ConstantsEasySP.SP_IS_SCAN_SHOW, false);
                    EasySP.init(MainActivity.this).putString(ConstantsEasySP.SP_TOKEN, "");
                    EasySP.init(MainActivity.this).putString(ConstantsEasySP.SP_USER_INFO, "");
                    EasySP.init(MainActivity.this).putLong(ConstantsEasySP.SP_USER_ID, 0);
                    EasySP.init(MainActivity.this).putLong(ConstantsEasySP.SP_USER_ORG_ID, 0);
                    EasySP.init(MainActivity.this).putString(ConstantsEasySP.SP_REFRESH_TOKEN, "");
                    EasySP.init(MainActivity.this).putBoolean(ConstantsEasySP.SP_IS_DATA_DOWNLOAD, false);

                    // reset
                    try {
                        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(MainActivity.this);
                        databaseAccess.open();
                        databaseAccess.clearAllTables();
                        databaseAccess.clearMyNodesAndMyProductsTables();
                        databaseAccess.close();
                        LoginManager.getInstance().logOut();
                    } catch (Exception e) {

                    }
                    Intent intent = new Intent(MainActivity.this, LoginPreviousActivity.class);
                    startActivity(intent);
                    finishAffinity();
                }
            });
            sweetAlertDialog.show();
            return true;
        } else if (id == R.id.action_my_profile) {
            Intent intent = new Intent(MainActivity.this, UserInfoActivity.class);
            startActivity(intent);
        }  else if (id == R.id.action_my_qr) {
            Intent intent = new Intent(MainActivity.this, MyQrActivity.class);
            startActivity(intent);
        }else if (id == R.id.action_my_catalogue) {
            String userinfoJson = EasySP.init(MainActivity.this).getString(ConstantsEasySP.SP_USER_INFO);
            if (userinfoJson != null) {
                try {
                    UserInfoPojo userInfoPojo = new Gson().fromJson(userinfoJson, UserInfoPojo.class);
                    if (userInfoPojo != null) {
                        if (userInfoPojo.getOrganizationPojo() != null) {
                            boolean isDataDownload = EasySP.init(MainActivity.this).getBoolean(ConstantsEasySP.SP_IS_DATA_DOWNLOAD);
                            if (isDataDownload){
                                Intent intent = new Intent(MainActivity.this, MyOrganizationDataActivity.class);
                                intent.putExtra("orgName", userInfoPojo.getOrganizationPojo().getDisplayName());
                                intent.putExtra("orgId", userInfoPojo.getOrganizationPojo().getId());
                                startActivity(intent);
                            }else{
                                if (ConstantsMethods.isConnected(MainActivity.this)) {
                                    Intent intent = new Intent(MainActivity.this, DataDownloadActivity.class);
                                    intent.putExtra("orgName", userInfoPojo.getOrganizationPojo().getDisplayName());
                                    intent.putExtra("orgId", userInfoPojo.getOrganizationPojo().getId());
                                    startActivity(intent);
                                }
                            }

                        }else{
                            showMessage("No Organization Linked","Please Register Your Organization",SweetAlertDialog.ERROR_TYPE);
                        }
                    }
                } catch (Exception e) {

                }

            }


        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            long userId = EasySP.init(MainActivity.this).getLong(ConstantsEasySP.SP_USER_ID, 0);
            long userOrgId = EasySP.init(MainActivity.this).getLong(ConstantsEasySP.SP_USER_ORG_ID, 0);
            Barcode barcode = data.getParcelableExtra("scanned_info");
            if (barcode != null) {
                String info = barcode.displayValue;
                try {
                    QRPojo qrPojo = new Gson().fromJson(info, QRPojo.class);
                    if (qrPojo != null && qrPojo.getId() > 0) {


                        if (qrPojo.getName() != null && qrPojo.getName().length() > 0 && qrPojo.getDisplayName() == null) {
                            // means user scan user qr code
                            if (qrPojo.getId() == userId) {
                                // means user scan own qr
                                showMessage("Self Org QR", "Cant scan self QR", 1);
                            } else {
                                // save in database is org = 0
                                saveIntoDatabase(qrPojo.getId(), qrPojo.getName(), 0, userId, userOrgId);
                            }
                        } else if (qrPojo.getDisplayName() != null && qrPojo.getDisplayName().length() > 0 && qrPojo.getName() == null) {
                            // means user scan organization qr code
                            if (qrPojo.getId() == userOrgId) {
                                // means self org qr code
                                showMessage("Self Org QR", "Cant scan self QR", 1);
                            } else {
                                // save in database is org = 1
                                saveIntoDatabase(qrPojo.getId(), qrPojo.getDisplayName(), 1, userId, userOrgId);
                            }
                        }
                    } else {
                        showMessage("invalid QR", "Please scan ecatalogue QR code.", 1);
                    }
                } catch (Exception e) {
                    showMessage("invalid QR", "Please scan ecatalogue QR code.", 1);
                }
            }else{
                try {
                    int indexifyId = Integer.parseInt(data.getStringExtra("indexify_id"));
                    if (indexifyId == userOrgId) {
                        showMessage("Self Org QR", "Cant scan self QR", 1);
                    }else{
                        if (indexifyId > 1) {
                            saveIntoDatabase(indexifyId, "", 1, userId, userOrgId);

                        } else {
                            showMessage("Invalid Id", "Please Enter Valid Id", 1);
                        }
                    }



                }catch(Exception error){
                    showMessage("invalid Id", "Please Enter Valid Id.", 1);
                }
            }
        }

    }

    private void showMessage(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MainActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });
        sweetAlertDialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnMyScans) {
            if (ConstantsMethods.isConnected(MainActivity.this)) {
                Intent intent = new Intent(MainActivity.this, MyScanListActivity.class);
                startActivity(intent);
            }
        } else if (v.getId() == R.id.btnMyViewer) {
            long id = EasySP.init(MainActivity.this).getLong(ConstantsEasySP.SP_USER_ORG_ID, 0);
            if (id == 0) {
                showMessage("Organization Not Found", "You must link with organization", 1);
            } else if (id > 0) {
                if (ConstantsMethods.isConnected(MainActivity.this)) {
                    Intent intent = new Intent(MainActivity.this, MyViewersListActivity.class);
                    startActivity(intent);
                }
            }
        } else if (v.getId() == R.id.btnFavourite) {
            if (ConstantsMethods.isConnected(MainActivity.this)) {
                Intent intent = new Intent(MainActivity.this, FavouriteListActivity.class);
                startActivity(intent);
            }
        }
    }
    private boolean checkAndRequestPermissions() {
        int permissionCamera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }

        return true;
    }
    private void setAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 03);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);

        Intent intent = new Intent(getApplicationContext(), AlarmServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmManager.INTERVAL_DAY, pendingIntent);

    }

    private void saveIntoDatabase(int scannedId, String scannedName, int isOrg, long userId, long userOrgId) throws Exception {
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(MainActivity.this);
        databaseAccess.open();
        ContentValues values = new ContentValues();
        values.put(ConstantsTables.T_SCANS_SCANNED_ID, scannedId);
        values.put(ConstantsTables.T_SCANS_SCANNED_NAME, scannedName);
        values.put(ConstantsTables.T_SCANS_IS_ORG, isOrg);
        values.put(ConstantsTables.T_SCANS_USER_ID, userId);
        values.put(ConstantsTables.T_SCANS_USER_ORG_ID, userOrgId);
        long x = databaseAccess.insertMyScanOrgInfo(values);
        databaseAccess.close();

        if (x > 0) {
            showMessage("QR Scanned", WordUtils.capitalize(scannedName), 2);
            if (ConstantsMethods.isConnectedWithoutMessage(MainActivity.this)) {
                Intent intent = new Intent(MainActivity.this, SendScanToServerService.class);
                startService(intent);
            }
        } else if (x <= 0) {
            showMessage("Error", "Please try again.", 1);
        }
    }


}
