package com.indexify.indexify.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.indexify.indexify.api.UserScanApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.database.DatabaseAccess;
import com.indexify.indexify.pojo.ScanPojo;
import com.white.easysp.EasySP;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

/**
 * Created by amar on 05-02-2018.
 */

public class SendScanToServerService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sendScan();
        return START_STICKY;
    }

    private void sendScan() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    long userId = EasySP.init(getApplicationContext()).getLong(ConstantsEasySP.SP_USER_ID, 0);
                    long userOrgId = EasySP.init(getApplicationContext()).getLong(ConstantsEasySP.SP_USER_ORG_ID, 0);
                    // get user scans from database
                    DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
                    databaseAccess.open();
                    List<ScanPojo> alScans = databaseAccess.getOfflineScanListByUserIdAndUserOrgId(userId, userOrgId);
                    databaseAccess.close();
                    if (alScans.size() > 0) {
                        for (int i = 0; i < alScans.size(); i++) {
                            ScanPojo scanPojo = alScans.get(i);
                            if (ConstantsMethods.isConnectedWithoutMessage(getApplicationContext())) {
                                try {
                                    final UserScanApi userScanApi = retrofit.create(UserScanApi.class);
                                    if (scanPojo.isOrg()) {
                                        // user scan organization QR
                                        JsonObject jsonObject = new JsonObject();


                                        Call<ResponseBody> callUserScan = userScanApi.postUserScanOrganization(scanPojo.getScannedId(), jsonObject);
                                        Response<ResponseBody> response = callUserScan.execute();
                                        if (response.code() == 201) {
                                            // means successfully post delete record
                                            DatabaseAccess databaseAccess1 = DatabaseAccess.getInstance(getApplicationContext());
                                            databaseAccess1.open();
                                            int x = databaseAccess1.deleteScanByInternalId(scanPojo.getId());
                                            databaseAccess.close();
                                        } else if (response.code() == 403) {
                                            Toast.makeText(getApplicationContext(), "Access denied", Toast.LENGTH_SHORT).show();
                                        } else if (response.code() == 400) {
                                            Toast.makeText(getApplicationContext(), "bad request", Toast.LENGTH_SHORT).show();
                                        } else if (response.code() == 409) {
                                            DatabaseAccess databaseAccess1 = DatabaseAccess.getInstance(getApplicationContext());
                                            databaseAccess1.open();
                                            int x = databaseAccess1.deleteScanByInternalId(scanPojo.getId());
                                            databaseAccess.close();
                                        }else if (response.code() == 404) {
                                            DatabaseAccess databaseAccess1 = DatabaseAccess.getInstance(getApplicationContext());
                                            databaseAccess1.open();
                                            int x = databaseAccess1.deleteScanByInternalId(scanPojo.getId());
                                            databaseAccess.close();
                                        }
                                    } else {
                                        // user scan user QR
                                        JsonObject jsonObject = new JsonObject();
                                      //  jsonObject.addProperty("tag", Constants.EVENT_NAME);
                                        jsonObject.addProperty("userId", scanPojo.getScannedId());

                                        Call<ResponseBody> callUserScan = userScanApi.postUserScanUser(jsonObject);
                                        Response<ResponseBody> response = callUserScan.execute();
                                        if (response.code() == 201) {
                                            // means successfully post delete record
                                            DatabaseAccess databaseAccess1 = DatabaseAccess.getInstance(getApplicationContext());
                                            databaseAccess1.open();
                                            int x = databaseAccess1.deleteScanByInternalId(scanPojo.getId());
                                            databaseAccess.close();
                                        } else if (response.code() == 403) {
                                            Toast.makeText(getApplicationContext(), "Access denied", Toast.LENGTH_SHORT).show();
                                        } else if (response.code() == 400) {
                                            Toast.makeText(getApplicationContext(), "bad request", Toast.LENGTH_SHORT).show();
                                        } else if (response.code() == 409) {
                                            DatabaseAccess databaseAccess1 = DatabaseAccess.getInstance(getApplicationContext());
                                            databaseAccess1.open();
                                            int x = databaseAccess1.deleteScanByInternalId(scanPojo.getId());
                                            databaseAccess.close();
                                        } else if (response.code() == 404) {
                                            DatabaseAccess databaseAccess1 = DatabaseAccess.getInstance(getApplicationContext());
                                            databaseAccess1.open();
                                            int x = databaseAccess1.deleteScanByInternalId(scanPojo.getId());
                                            databaseAccess.close();
                                        }
                                    }
                                } catch (Exception e) {
                                    Log.e("post scan error: ", e.toString());
                                }
                            }
                        }
                        //    Toast.makeText(getApplicationContext(), "found " + alScans.size(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                }
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
