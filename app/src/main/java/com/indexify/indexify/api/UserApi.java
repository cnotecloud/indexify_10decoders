package com.indexify.indexify.api;




import com.indexify.indexify.pojo.MyScanUserInfoPojo;
import com.indexify.indexify.pojo.UserInfoPojo;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by amar on 01-06-2017.
 */

public interface UserApi {
    @GET("user/me")
    Single<UserInfoPojo> getOrganizationInformation();

  //  @GET("v1/users/search/findById")
    @GET("v1/users/search/getById")
    Single<MyScanUserInfoPojo> getScannedUserInfo(@Header("X-Requested-User") long userId);

}
