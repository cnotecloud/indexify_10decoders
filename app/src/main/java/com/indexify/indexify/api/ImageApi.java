package com.indexify.indexify.api;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by amar on 22-09-2016.
 */

public interface ImageApi {
    @GET("{imageName}")
    Call<ResponseBody> res(@Path("imageName") String imageName);
}
