package com.indexify.indexify.api;


import com.indexify.indexify.pojo.ProductPojo;
import com.indexify.indexify.pojo.ProductsGetPojo;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by amar on 20-01-2018.
 */

public interface ProductApi {

    @GET("v1/products")
        //@GET("v1/products/search/findByOrganizationId")
    Call<ProductsGetPojo> getProducts(@Query("projection") String projection,
                                      @Query("size") int size,
                                      @Query("page") int page);


    @GET("v1/products/search/findByLeafId")
    Observable<ProductsGetPojo> getProductsByOrgIdAndLeafId(@Header("X-Requested-organization") long organizationId,
                                                            @Query("leafId") String leafId,
                                                            @Query("projection") String projection,
                                                            @Query("size") int size,
                                                            @Query("page") int page);

    @GET("v1/products/search/findByNameContainingIgnoreCase")
    Observable<ProductsGetPojo> searchProductsByContainingNameAndOrgId(@Header("X-Requested-organization") long organizationId,
                                                                       @Query("name") String name,
                                                                       @Query("projection") String projection,
                                                                       @Query("size") int size,
                                                                       @Query("page") int page);

    //@GET("v1/products/search/findById")
    @GET("v1/products/search/getById")
    Single<ProductPojo> getProductById(@Header("X-Requested-organization") long organizationId,
                                       @Query("productId") String productId,
                                       @Query("projection") String projection);


    //get My Organization Products

    @GET("v1/products")
        //@GET("v1/products/search/findByOrganizationId")
    Call<ProductsGetPojo> getMyProducts(@Query("projection") String projection,
                                              @Query("size") int size,
                                              @Query("page") int page);

    @GET("v1/products/{productId}")
        //@GET("v1/products/search/findByOrganizationId")
    Single<ProductPojo> getMyFavouriteProductViewList(@Path("productId") String productId,
                                                        @Query("projection") String projection);

}
