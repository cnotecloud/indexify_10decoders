package com.indexify.indexify.api;

import com.indexify.indexify.pojo.OrganizationScanGetPojo;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by amar on 06-02-2018.
 */

public interface OrganizationScanApi {

    @GET("v1/organizationScans/search/findByOrderByCreatedDesc")
    Observable<OrganizationScanGetPojo> getOrganizationScanList(@Query("projection") String projection,
                                                                @Query("size") int size,
                                                                @Query("page") int page);

    @GET("v1/organizationScans/search/findByNameContainingIgnoreCaseOrderByCreatedDesc")
    Observable<OrganizationScanGetPojo> searchOrgScanListContainingName(@Query("name") String username,
                                                                        @Query("projection") String projection,
                                                                        @Query("size") int size,
                                                                        @Query("page") int page);

    @DELETE("organization-scans/{userScanId}")
    Single<Response<Void>> deleteScannedUser(@Path("userScanId") String userScanId);

}
