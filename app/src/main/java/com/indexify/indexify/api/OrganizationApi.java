package com.indexify.indexify.api;

import com.indexify.indexify.pojo.OrganizationInfoPojo;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by cnote on 06-02-2018.
 */

public interface OrganizationApi {

    //@GET("v1/organizations/search/findById")
    @GET("v1/organizations/search/getById")
    Single<OrganizationInfoPojo> getOrgDetails(@Header("X-Requested-Organization") long orgId);
}
