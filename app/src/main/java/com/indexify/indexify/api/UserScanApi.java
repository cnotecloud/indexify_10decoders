package com.indexify.indexify.api;

import com.google.gson.JsonObject;
import com.indexify.indexify.pojo.UserScanGetPojo;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by amar on 05-02-2018.
 */

public interface UserScanApi {

    @POST("user-scan")
    Call<ResponseBody> postUserScan(@Header("X-Requested-Organization") long organizationId,
                                    @Body JsonObject jsonObject);

    // if user scan user QR
    @POST("user-scan")
    Call<ResponseBody> postUserScanUser(@Body JsonObject jsonObject);

    // if user scan organization QR
    @POST("user-scan")
    Call<ResponseBody> postUserScanOrganization(@Header("X-Requested-Organization") long organizationId,
                                                @Body JsonObject jsonObject);



    @GET("v1/userScans/search/findByOrderByCreatedDesc")
    Observable<UserScanGetPojo> getUserScanList(@Query("projection") String projection,
                                                @Query("size") int size,
                                                @Query("page") int page);

    @GET("v1/userScans/search/findByNameContainingIgnoreCaseOrderByCreatedDesc")
    Observable<UserScanGetPojo> searchUserScanContaining(@Query("name") String displayName,
                                                         @Query("projection") String projection,
                                                         @Query("size") int size,
                                                         @Query("page") int page);

    @DELETE("user-scan/{organizationScanId}")
    Single<Response<Void>> deleteScannedOrganization(@Path("organizationScanId") String organizationScanId);

}
