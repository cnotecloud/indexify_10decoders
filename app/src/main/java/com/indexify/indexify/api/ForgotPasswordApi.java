package com.indexify.indexify.api;

import com.indexify.indexify.pojo.ForgotPasswordPojo;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by Kalyan on 24-08-2017.
 */

public interface ForgotPasswordApi {
    @Headers({
            "X-Requested-With: XMLHttpRequest",
            "Content-Type: application/json",
            "Cache-Control: no-cache",
            "Accept: application/json"
    })

    @GET("user/forget-password")
    Single<ForgotPasswordPojo> getNewPassword(@Query("email") String userName);


}
