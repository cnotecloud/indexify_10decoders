package com.indexify.indexify.api;

import com.google.gson.JsonObject;
import com.indexify.indexify.pojo.ResponsePojo;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by cnote on 01-02-2018.
 */

public interface SignUpApi {
    @Headers({
            "X-Requested-With: XMLHttpRequest",
            "Content-Type: application/json",
            "Cache-Control: no-cache",
            "Accept: application/json"
    })
    @POST("user/registration")
    Single<ResponsePojo> registerUser(@Body JsonObject jsonObject);
}
