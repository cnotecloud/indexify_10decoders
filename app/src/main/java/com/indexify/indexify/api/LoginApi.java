package com.indexify.indexify.api;


import com.google.gson.JsonObject;
import com.indexify.indexify.pojo.TokenPojo;


import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by amar on 31-05-2017.
 */

public interface LoginApi {


    @Headers({
            "X-Requested-With: XMLHttpRequest",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Content-Type: application/json"
    })
    @GET("auth/token")
    Call<TokenPojo> getNewTokenFromRefreshToken(@Header("Authorization") String authorization);


    @POST("auth/login")
    Single<TokenPojo> getTokens(@Body JsonObject jsonObject);

    //step 1 login with username

    @Headers({
            "X-Requested-With: XMLHttpRequest",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Content-Type: application/json"
    })
    @POST("auth/username")
    Single<TokenPojo> getStep1Token(@Body JsonObject jsonObject);


    @Headers({
            "X-Requested-With: XMLHttpRequest",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "X-Otp: true"
    })
    @POST("auth/username")
    Single<TokenPojo> getStep1TokenWithTagOtp(@Body JsonObject jsonObject);


/*
    Observable<TokenPojo> getTokens(@Query("grant_type") String grantType,
                                @Query("scope") String scope,
                                @Query("username") String userName,
                                @Query("password") String userPassword);
*/

    //login with otp
    @Headers({
            "X-Requested-With: XMLHttpRequest",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Content-Type: application/json"
    })
    @POST("auth/otp")
    Single<TokenPojo> getLoginWithOtp(@Header("Authorization") String accessToken ,
                                      @Body JsonObject jsonObject);
    // login with password
    @Headers({
            "X-Requested-With: XMLHttpRequest",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Content-Type: application/json"
    })
    @POST("auth/password")
    Single<TokenPojo> getLoginWithPassword(@Header("Authorization") String accessToken ,
                                      @Body JsonObject jsonObject);


    //login with facebook
    @Headers({
            "X-Requested-With: XMLHttpRequest",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Content-Type: application/json"
    })
    @POST("auth/social")
    Single<TokenPojo> getLoginWithFacebookOrGoogle(@Body JsonObject jsonObject);

}
