package com.indexify.indexify.api;

import com.google.gson.JsonObject;
import com.indexify.indexify.pojo.FavouriteOrganizationGetPojo;
import com.indexify.indexify.pojo.FavouriteProductsGetPojo;
import com.indexify.indexify.pojo.MyProductFavouriteViewerGetPojo;
import com.indexify.indexify.pojo.MyProductFavouriteViewerPojo;
import com.indexify.indexify.pojo.ResponsePojo;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by amar on 12-02-2018.
 */

public interface FavouriteApi {

    @GET("favourite-products/organizations")
    Observable<List<FavouriteOrganizationGetPojo>> getFavouriteOrganizationList(@Query("size") int size,
                                                                                @Query("page") int page);

    @GET("v1/favouriteProducts/search/findByOrganizationId")
    Observable<FavouriteProductsGetPojo> getFavouriteProductsByOrganizationId(@Header("X-Requested-Organization") Long organizationId,
                                                                              @Query("projection") String projection,
                                                                              @Query("size") int size,
                                                                              @Query("page") int page);

    @DELETE("favourite-products/{favouriteProductId}")
    Observable<Response<Void>> deleteFavouriteProduct(@Path("favouriteProductId") String favouriteProductId);


    @POST("favourite-products")
    Single<ResponsePojo> postOrganizationFavouriteProduct(@Header("X-Requested-Organization") long organizationId,
                                                          @Body JsonObject jsonObject);

    @GET("v1/reverseFavouriteProducts")
    Observable<MyProductFavouriteViewerGetPojo> getMyProductFavouriteList(@Query("projection") String projection,
                                                                          @Query("size") int size,
                                                                          @Query("page") int page);

    @DELETE("v1/reverseFavouriteProducts/{favouriteProductId}")
    Single<Response<Void>> deleteMyFavouriteViewer(@Path("favouriteProductId") String favouriteProductId);
}
