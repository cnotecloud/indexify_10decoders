package com.indexify.indexify.api;

import com.indexify.indexify.pojo.NodesGetPojo;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by amar on 19-01-2018.
 */

public interface NodeApi {

   /* @GET("v1/nodes")
    Call<NodesGetPojo> getNodes(@Query("projection") String projection,
                                @Query("size") int size,
                                @Query("page") int page);*/

    @GET("v1/nodes")
    Single<NodesGetPojo> getNodesByOrganizationId(@Header("X-Requested-organization") long organizationId,
                                                  @Query("projection") String projection,
                                                  @Query("size") int size,
                                                  @Query("page") int page);

    //get My Organization Data

    @GET("v1/nodes")
    Observable<NodesGetPojo> getMyNodesByOrganizationId(@Header("X-Requested-organization") long organizationId,
                                                        @Query("projection") String projection,
                                                        @Query("size") int size,
                                                        @Query("page") int page);




}
