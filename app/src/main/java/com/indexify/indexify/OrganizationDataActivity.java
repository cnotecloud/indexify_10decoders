package com.indexify.indexify;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.indexify.indexify.adapter.BreadScrumbAdapter;
import com.indexify.indexify.api.NodeApi;
import com.indexify.indexify.api.OrganizationApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsBreadScrumb;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.constants.ConstantsTables;
import com.indexify.indexify.database.DatabaseAccess;
import com.indexify.indexify.fragments.FragmentNode;
import com.indexify.indexify.fragments.FragmentProduct;
import com.indexify.indexify.interfaces.SetBreadScrumb;
import com.indexify.indexify.pojo.BreadScrumbPojo;
import com.indexify.indexify.pojo.NodePojo;
import com.indexify.indexify.pojo.NodesGetPojo;
import com.indexify.indexify.pojo.OrganizationInfoPojo;
import com.indexify.indexify.pojo.ResponsePojo;
import com.white.easysp.EasySP;

import org.apache.commons.lang3.text.WordUtils;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import retrofit2.HttpException;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class OrganizationDataActivity extends AppCompatActivity implements SetBreadScrumb, View.OnClickListener {

    List<BreadScrumbPojo> alBreadScrumb = new ArrayList<>();
    BreadScrumbAdapter adapter = null;
    RecyclerView rvBreadScrumb;
    FloatingActionButton fabFavourites;
    FloatingActionButton fabBack;
    LinearLayoutManager mLayoutManager;
    SearchView search;
    LinearLayout llPdf;
    ProgressBar progressPdf;
    TextView tvPdf;
    long orgId;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization_data);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        String orgName = getIntent().getStringExtra("orgName");
        if (orgName!= null) {
            toolbar.setTitle(orgName);
        }else {
            toolbar.setTitle("");
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getControls();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {

            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getControls() {
        fabFavourites = (FloatingActionButton) findViewById(R.id.fabFavourites);
        fabBack = (FloatingActionButton) findViewById(R.id.fabBack);
        fabFavourites.setOnClickListener(this);
        fabBack.setOnClickListener(this);
        rvBreadScrumb = (RecyclerView) findViewById(R.id.rvBreadScrumb);
        mLayoutManager = new LinearLayoutManager(OrganizationDataActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvBreadScrumb.setHasFixedSize(true);
        rvBreadScrumb.setLayoutManager(mLayoutManager);

        adapter = new BreadScrumbAdapter(alBreadScrumb, OrganizationDataActivity.this);
        rvBreadScrumb.setAdapter(adapter);

        llPdf = (LinearLayout) findViewById(R.id.llPdf);
        progressPdf = (ProgressBar) findViewById(R.id.progressPdf);
        tvPdf = (TextView) findViewById(R.id.tvPdf);


        if (ConstantsMethods.isConnected(OrganizationDataActivity.this)) {
            ConstantsMethods.showProgessDialogWithCancelable(OrganizationDataActivity.this, "Fetching structure...");
            getData();

        }

    }

    private void getData() {
        if (ConstantsMethods.isConnected(OrganizationDataActivity.this)) {
            orgId = getIntent().getLongExtra("orgId", 0);
            if (orgId > 0 ){
                getOrgInfoFromServer(orgId);
            }
            final NodeApi nodeApi = retrofit.create(NodeApi.class);

            Single<NodesGetPojo> orgNodeBoservable = nodeApi.getNodesByOrganizationId(orgId, ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_NODE_SIZE, Constants.DEFAULT_PAGE);
            orgNodeBoservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<NodesGetPojo>() {
                        @Override
                        public void onSuccess(NodesGetPojo nodesGetPojo) {
                            try {
                                ConstantsMethods.cancleProgessDialog();
                                // clear table
                                if (nodesGetPojo != null) {
                                    if (nodesGetPojo.getEmbedded().getNodes().size() > 0) {
                                        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(OrganizationDataActivity.this);
                                        databaseAccess.open();
                                        databaseAccess.clearNodesTables();
                                        databaseAccess.close();
                                        insertNodes(nodesGetPojo);
                                        ConstantsMethods.cancleProgessDialog();
                                        attachData((int) orgId);
                                        // show fragments
                                    }
                                }


                            } catch (Exception e) {

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            ConstantsMethods.cancleProgessDialog();
                            try {
                                HttpException ex = (HttpException) e;
                                String er = ex.response().errorBody().string();
                                ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                                if (responsePojo != null) {
                                    if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.INVALID_USERNAME_PASSWORD) {
                                        showError("Unauthorized", "Please check email and password.");
                                    } else if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.EMAIL_NOT_VERIFIED) {
                                        showError("User not verified", "Please verify through email and try again.");
                                    } else if (responsePojo.getStatus() == 403) {
                                        showError("Not access", "Please try again or contact admin.");
                                    } else if (responsePojo.getStatus() == 500) {
                                        showError("Intermal server error", "Something went wrong please try later.");
                                    }
                                }
                            } catch (Exception e1) {
                                showError("Error", "Something went wrong please try later.");
                            } finally {
                                ConstantsMethods.cancleProgessDialog();
                            }
                        }
                    });
        }
    }


    private void getOrgInfoFromServer(long orgId) {
        //  ConstantsMethods.showProgessDialog(OrganizationDataActivity.this, "Products Not found\nFetching PDF");
        OrganizationApi detailsApi = retrofit.create(OrganizationApi.class);
        Single<OrganizationInfoPojo> orgResponse = detailsApi.getOrgDetails(orgId);
        orgResponse.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<OrganizationInfoPojo>() {
                    @Override
                    public void onSuccess(OrganizationInfoPojo organizationInfoPojo) {
                        ConstantsMethods.cancleProgessDialog();
                        if (organizationInfoPojo != null) {
                            if ((organizationInfoPojo.getDisplayName() != null) && (organizationInfoPojo.getDisplayName().length() >0)){
                               if (toolbar != null){
                                   toolbar.setTitle(organizationInfoPojo.getDisplayName());
                               }
                            }
                            if (organizationInfoPojo.getCatalogues() != null && organizationInfoPojo.getCatalogues().size() > 0) {
                                progressPdf.setVisibility(View.GONE);
                                tvPdf.setText("View PDF (" + organizationInfoPojo.getCatalogues().size() + ")");
                                Set<String> stringSet = new HashSet<>(organizationInfoPojo.getCatalogues());
                                EasySP.init(OrganizationDataActivity.this).remove(ConstantsEasySP.SP_CATALOGUE_LIST);
                                EasySP.init(OrganizationDataActivity.this).putStringSet(ConstantsEasySP.SP_CATALOGUE_LIST,stringSet);
                                llPdf.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(OrganizationDataActivity.this, PdfListActivity.class);
                                        intent.putExtra("pdfList", (Serializable) organizationInfoPojo.getCatalogues());
                                        startActivity(intent);
                                    }
                                });
                                // show button for pdf with count.

                                       /* Intent i = new Intent(Intent.ACTION_VIEW,
                                                Uri.parse(organizationInfoPojo.getCatalogue()));
                                        startActivity(i);
                                        finish();*/

                            } else {
                                llPdf.setVisibility(View.GONE);
                            }
                        } else {
                            llPdf.setVisibility(View.GONE);

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            llPdf.setVisibility(View.GONE);
                        } catch (Exception e1) {
                            llPdf.setVisibility(View.GONE);
                        } finally {
                            llPdf.setVisibility(View.GONE);
                        }
                    }
                });

    }

    private void attachData(int orgId) {
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(OrganizationDataActivity.this);
        databaseAccess.open();
        List<NodePojo> alRootNodes = databaseAccess.getRootNodes();
        databaseAccess.close();

        if (alRootNodes.size() > 0) {
            try {
                int i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
                FragmentNode fragmentNode = new FragmentNode();
                Bundle bundle = new Bundle();
                bundle.putSerializable("nodes", (Serializable) alRootNodes);
                bundle.putInt("orgId", orgId);
                bundle.putString("nodeName", "Home");
                bundle.putString("nodeId", String.valueOf(i));
                fragmentNode.setArguments(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frameLayout, fragmentNode, String.valueOf(i));
                // transaction.replace(R.id.frameLayout, fragmentNode, "root");
                transaction.commit();
            } catch (Exception e) {

            }
        } else if (alRootNodes.size() == 0) {
            Toast.makeText(this, "not found", Toast.LENGTH_SHORT).show();
        }

    }

    private void showError(String title, String message) {
        try {
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(OrganizationDataActivity.this, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText(title);
            sweetAlertDialog.setContentText(message);
            sweetAlertDialog.show();
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismissWithAnimation();
                    finish();
                }
            });
        } catch (Exception e) {

        }
    }

    private void insertNodes(NodesGetPojo nodesGetPojo) {
        try {
            for (int i = 0; i < nodesGetPojo.getEmbedded().getNodes().size(); i++) {
                DatabaseAccess databaseAccess = DatabaseAccess.getInstance(OrganizationDataActivity.this);
                databaseAccess.open();
                NodePojo nodePojo = nodesGetPojo.getEmbedded().getNodes().get(i);

                ContentValues values = new ContentValues();
                values.put(ConstantsTables.T_NODES_SERVER_ID, nodePojo.getId());
                values.put(ConstantsTables.T_NODES_DISPLAY_NAME, nodePojo.getDisplayName());
                values.put(ConstantsTables.T_NODES_PARENT_SERVER_ID, nodePojo.getParentId());

                if (nodePojo.isLeaf()) {
                    values.put(ConstantsTables.T_NODES_IS_LEAF, 1);
                } else {
                    values.put(ConstantsTables.T_NODES_IS_LEAF, 0);
                }

                values.put(ConstantsTables.T_NODES_CREATED, nodePojo.getCreated());
                values.put(ConstantsTables.T_NODES_UPDATED, nodePojo.getUpdated());

                long x = databaseAccess.insertNode(values);
                Log.i("node insert", String.valueOf(x));
                databaseAccess.close();
            }
        } catch (Exception e) {
            Log.e("node error : ", e.toString());
        }

    }

    @Override
    public void setBreadScrumb(BreadScrumbPojo breadScrumb) {
        alBreadScrumb.add(breadScrumb);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void removeBreadScrumb(String tag) {
        for (int i = 0; i < alBreadScrumb.size(); i++) {
            if (alBreadScrumb.get(i).getTag().equalsIgnoreCase(tag)) {
                alBreadScrumb.remove(i);
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void removeBreadScrumbOnClik(String tag) {
        try {
            int to = Integer.parseInt(tag);

            for (int from = ConstantsBreadScrumb.BREADSCRUMB_INIT; from > to; from--) {

                Fragment fragmentNode = getSupportFragmentManager().findFragmentByTag(String.valueOf(from));
                if (fragmentNode != null) {
                    try {
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.remove(fragmentNode);
                        transaction.commit();
                        getSupportFragmentManager().popBackStack();
                    } catch (Exception e) {

                    }
                }
            }
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_organization_data_activity, menu);

        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        search = (SearchView) menu.findItem(R.id.action_search).getActionView();

        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() < 3) {
                    Toast.makeText(OrganizationDataActivity.this, "Enter minimum 3 chars", Toast.LENGTH_SHORT).show();
                } else {
                    // check internet connectin


                    if (ConstantsMethods.isConnected(OrganizationDataActivity.this)) {
                        search.setIconified(true);
                        search.clearFocus();

                        long orgId = getIntent().getLongExtra("orgId", 0);
                        String orgName = getIntent().getStringExtra("orgName");

                        int i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
                        FragmentProduct fragmentProduct = new FragmentProduct();
                        Bundle bundle = new Bundle();
                        bundle.putString("nodeName", query.toLowerCase()); // org name
                        bundle.putLong("orgId", orgId); // org Id
                        bundle.putString("type", "search");
                        fragmentProduct.setArguments(bundle);
                        FragmentManager manager = getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        // transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        transaction.add(R.id.frameLayout, fragmentProduct, String.valueOf(i));
                        transaction.addToBackStack(String.valueOf(fragmentProduct));
                        transaction.commit();
                    }
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                /*if (newText.length() == 0) {
                    rvUserList.setAdapter(adapter);
                }*/
                return true;
            }
        });
        return true;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.fabFavourites) {
            if (ConstantsMethods.isConnected(OrganizationDataActivity.this)) {
                getFavouritesByOrganization();
            }
        } else {
            if (v.getId() == R.id.fabBack) {
                Intent intent = new Intent();
                setResult(506, intent);
                finish();
            }
        }

    }

    private void getFavouritesByOrganization() {
        Intent intent = new Intent(OrganizationDataActivity.this, FavouriteProductListActivity.class);
        intent.putExtra("orgName", getIntent().getStringExtra("orgName"));
        intent.putExtra("orgId", getIntent().getLongExtra("orgId", 0));
        startActivity(intent);
    }
}
