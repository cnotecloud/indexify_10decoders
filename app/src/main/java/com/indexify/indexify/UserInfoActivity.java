package com.indexify.indexify;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indexify.indexify.api.UserApi;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.fragments.FragmentOrganizationInfo;
import com.indexify.indexify.fragments.FragmentUserInfo;
import com.indexify.indexify.pojo.UserInfoPojo;
import com.white.easysp.EasySP;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class UserInfoActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    TextView tvName;
    CircleImageView imgUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getControls();
    }

    private void getControls() {
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tvName = (TextView) findViewById(R.id.tvName);
        imgUser = (CircleImageView) findViewById(R.id.imgUser);
        setupViewPager(viewPager);
        getData();
    }

    private void getData() {
//        String userInfojson = EasySP.init(UserInfoActivity.this).getString(ConstantsEasySP.SP_USER_INFO);
//        UserInfoPojo userInfoPojo = new Gson().fromJson(userInfojson, UserInfoPojo.class);
//
//        if (userInfoPojo != null){
//            if ((userInfoPojo.getFirstName() != null)  && (userInfoPojo.getFirstName().length() > 0)){
//
//            }
//            if ((userInfoPojo.getLastName() != null)  && (userInfoPojo.getLastName().length() > 0)){
//
//            }
//            if ((userInfoPojo.getEmail() != null)  && (userInfoPojo.getEmail().length() > 0)){
//
//            }
//            if ((userInfoPojo.getContact() != null)  && (userInfoPojo.getContact().length() > 0)){
//
//            }
//        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(ViewPager viewPager) {

        if (ConstantsMethods.isConnectedWithoutMessage(UserInfoActivity.this)){
            ConstantsMethods.showProgessDialog(UserInfoActivity.this,"Loading...");
            final UserApi organizationApi = retrofit.create(UserApi.class);
            Single<UserInfoPojo> orgSingleObservable = organizationApi.getOrganizationInformation();
            orgSingleObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<UserInfoPojo>() {
                        @Override
                        public void onSuccess(@NonNull final UserInfoPojo userInfoPojo) {
                            ConstantsMethods.cancleProgessDialog();
                            if (userInfoPojo != null) {
                                String userInfojson = new Gson().toJson(userInfoPojo);
                                sendDataToFragment(userInfojson);
                                //EasySP.init(UserInfoActivity.this).put(ConstantsEasySP.SP_USER_INFO, userInfojson);



                                // show home screen


                           /* } else {

                                //show qr scan code
                               *//* Intent intent = new Intent(LoginActivity.this, DataDownloadActivity.class);
                                startActivity(intent);
                                finish();*//*
                            }*/
                            } else {
                                ConstantsMethods.cancleProgessDialog();

                                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(UserInfoActivity.this, SweetAlertDialog.ERROR_TYPE);
                                sweetAlertDialog.setTitleText("No User Details");
                                sweetAlertDialog.setContentText("User Details Not Found!");
                                sweetAlertDialog.show();
                                sweetAlertDialog.setCancelable(false);
                                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        finish();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                            ConstantsMethods.showServerError(e, UserInfoActivity.this);

                        }
                    });
        }else{
            String userInfojson = EasySP.init(UserInfoActivity.this).getString(ConstantsEasySP.SP_USER_INFO);
            sendDataToFragment(userInfojson);
        }


    }

    private void sendDataToFragment(String userInfojson) {
        UserInfoPojo userInfoPojo = new Gson().fromJson(userInfojson, UserInfoPojo.class);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putString("userinfo", userInfojson);

        FragmentUserInfo fragmentUserInfo = new FragmentUserInfo();
        fragmentUserInfo.setArguments(bundle);

        FragmentOrganizationInfo fragmentOrganizationInfo = new FragmentOrganizationInfo();
        fragmentOrganizationInfo.setArguments(bundle);


        adapter.addFragment(fragmentUserInfo, "User Details");
        if (userInfoPojo.getOrganizationPojo() != null ){
            adapter.addFragment(fragmentOrganizationInfo, "Organization Details");
        }

        viewPager.setAdapter(adapter);

        tvName.setText(userInfoPojo.getFirstName());
        imgUser.setImageResource(R.drawable.user_icon);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    if (userInfoPojo != null){
                        tvName.setText(userInfoPojo.getFirstName());
                        imgUser.setImageResource(R.drawable.user_icon);
                    }

                } else if (position == 1) {
                    if (userInfoPojo.getOrganizationPojo() != null){
                        tvName.setText(userInfoPojo.getOrganizationPojo().getDisplayName());
                        imgUser.setImageResource(R.drawable.organization_icon);
                    }


                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
