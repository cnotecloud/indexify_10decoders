package com.indexify.indexify.constants;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.indexify.indexify.ui.ClientPlayer;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.HttpException;


/**
 * Created by amar on 21-09-2016.
 */

public class ConstantsMethods {

    /*public static class StringConverter implements Converter {
        @Override
        public Object fromBody(TypedInput typedInput, Type type) throws ConversionException {
            String text = null;
            try {
                text = fromStream(typedInput.in());
            } catch (IOException ignored) {*//*NOP*//* }
            return text;
        }

        @Override
        public TypedOutput toBody(Object o) {
            return null;
        }

        public static String fromStream(InputStream in) throws IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder out = new StringBuilder();
            String newLine = System.getProperty("line.separator");
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }
            return out.toString();
        }
    }
*/
    public static void showProgessDialog(final Context context, final String message) {
        ClientPlayer.runOnUI(new Runnable() {
            @Override
            public void run() {
                try {
                    ConstantsControls.progressDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                    ConstantsControls.progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    ConstantsControls.progressDialog.setTitleText(message);
                    ConstantsControls.progressDialog.setCancelable(false);
                    ConstantsControls.progressDialog.show();
                } catch (Exception e) {

                }
            }
        });
    }

    public static void showProgessDialogWithCancelable(final Context context, final String message) {
        ClientPlayer.runOnUI(new Runnable() {
            @Override
            public void run() {
                try {
                    ConstantsControls.progressDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                    ConstantsControls.progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    ConstantsControls.progressDialog.setTitleText(message);
                    ConstantsControls.progressDialog.setCancelable(true);
                    ConstantsControls.progressDialog.show();
                } catch (Exception e) {

                }
            }
        });
    }

    public static void cancleProgessDialog() {
        ClientPlayer.runOnUI(new Runnable() {
            @Override
            public void run() {
                try {
                    if (ConstantsControls.progressDialog.isShowing()) {
                        ConstantsControls.progressDialog.cancel();
                    }
                } catch (Exception e) {

                }
            }
        });
    }


    public static void showServerError(Throwable e, Context context) {
        try {
            HttpException error = (HttpException) e;
            if (error.code() == 401) {
                ConstantsMethods.cancleProgessDialog();
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText("Unauthorized.");
                sweetAlertDialog.setContentText("unauthorized");
                sweetAlertDialog.show();
            } else if (error.code() == 400) {
                ConstantsMethods.cancleProgessDialog();
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText("Invalid Credentials.");
                sweetAlertDialog.setContentText("Please check username and password.");
                sweetAlertDialog.show();
            } else if (error.code() == 500) {
                ConstantsMethods.cancleProgessDialog();
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText("Internal server error");
                sweetAlertDialog.setContentText("Internal server error.");
                sweetAlertDialog.show();
            } else {
                ConstantsMethods.cancleProgessDialog();
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText("unknown error");
                sweetAlertDialog.setContentText("unknown error.");
                sweetAlertDialog.show();
            }
        } catch (Exception ex) {
            ConstantsMethods.cancleProgessDialog();
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText("unknown error");
            sweetAlertDialog.setContentText("unknown error.");
            sweetAlertDialog.show();
        }
    }


    public static boolean isConnected(final Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }

        if ((haveConnectedMobile == false) && (haveConnectedWifi == false)) {
            ClientPlayer.runOnUI(new Runnable() {
                @Override
                public void run() {
                    try {
                        ConstantsControls.sadNoInternet = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                        ConstantsControls.sadNoInternet.setTitleText("No Internet");
                        ConstantsControls.sadNoInternet.setContentText("Please check Intenet connection.");
                        ConstantsControls.sadNoInternet.show();
                    } catch (Exception e) {
                        Toast.makeText(context, "Please check your internet connection...!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static boolean isConnectedWithoutMessage(final Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }

        return haveConnectedWifi || haveConnectedMobile;
    }



    /*public static TypedInput convertJsonToTypedInput(JsonObject jsonObject) {
        TypedInput in = null;
        try {
            in = null;
            Gson gson = new Gson();
            String data = gson.toJson(jsonObject);
            in = new TypedByteArray("application/json", data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return in;
    }*/

    public static String convertBase64(String uname, String password) {

        String inputData = uname.trim() + ":" + password.trim();
        byte[] byteData = new byte[0];
        try {
            byteData = inputData.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(byteData, Base64.NO_WRAP);
    }

    public static String generateAuth(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String username = preferences.getString("uname", "");
        final String password = preferences.getString("pass", "");


        String inputData = username.trim() + ":" + password.trim();
        byte[] byteData = new byte[0];
        try {
            byteData = inputData.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(byteData, Base64.NO_WRAP);
    }

    public static Date convertStringToDate(String date1) {
        TimeZone tz = TimeZone.getDefault();
        Date date = null;
        String FORMAT_DATETIME = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        // String FORMAT_DATETIME = "yyyy-MM-dd'T'hh:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATETIME);
        sdf.setTimeZone(tz);
        try {
            date = sdf.parse(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String convertStringToDateString(String date1) {
        String convertString = "";
        TimeZone tz = TimeZone.getDefault();
        Date date = null;
        String FORMAT_DATETIME = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        // String FORMAT_DATETIME = "yyyy-MM-dd'T'hh:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATETIME);
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM");
        sdf.setTimeZone(tz);
        try {
            date = sdf.parse(date1);
            convertString = sdf2.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertString;
    }

    public static String convertStringToDateStringFull(String date1) {
        String convertString = "";
        TimeZone tz = TimeZone.getDefault();
        Date date = null;
        String FORMAT_DATETIME = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        // String FORMAT_DATETIME = "yyyy-MM-dd'T'hh:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATETIME);
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        sdf.setTimeZone(tz);
        try {
            date = sdf.parse(date1);
            convertString = sdf2.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertString;
    }

    public static String getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        return sdf.format(cal.getTime());
    }

    public static String getCurrentDateOnly() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        return sdf.format(cal.getTime());
    }

    public static String getCurrentTimeOnly() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        return sdf.format(cal.getTime());
    }

    public static String getCurrentDateWithMill() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
        return sdf.format(cal.getTime());
    }

    public static Calendar getCalenderFromDate(int year, int month, int day) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.YEAR, year);
        return calendar;
    }


    public final static boolean isValidEmail(String target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /*public static void unauthorizedLogout(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("uname", "");
        editor.putString("user_details", "");
        editor.putString("pass", "");
        editor.putString("role", "");
        editor.putString("is_login", "");
        editor.apply();
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }*/


    public static String splitCamelCase(String s) {
        return s.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"
                ),
                " "
        );
    }

    public static String getIdFromLink(String link) {
        return link.substring(link.lastIndexOf("/") + 1, link.length());
    }

    public static boolean checkUserName(String username) {
        //Pattern pattern= Pattern.compile("^[a-z0-9_.]{8,30}$");
        Pattern pattern = Pattern.compile("^[a-z0-9]+([a-z0-9._](_|.)[a-z0-9])*[a-z0-9]*$");

        Matcher matcher = pattern.matcher(username);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    public static boolean checkUserPassword(String password) {

        Pattern pattern = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%_!()^&*:;<>,./?{}']).{8,30})");

        Matcher matcher = pattern.matcher(password);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    public static String getFileNameFromLink(String url) {
        return url.substring(url.lastIndexOf(File.separatorChar) + 1, url.length());
    }

    public static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String formatFileSize(long size) {
        String hrSize = null;

        double b = size;
        double k = size / 1024.0;
        double m = ((size / 1024.0) / 1024.0);
        double g = (((size / 1024.0) / 1024.0) / 1024.0);
        double t = ((((size / 1024.0) / 1024.0) / 1024.0) / 1024.0);

        DecimalFormat dec = new DecimalFormat("0.00");

        if (t > 1) {
            hrSize = dec.format(t).concat(" TB");
        } else if (g > 1) {
            hrSize = dec.format(g).concat(" GB");
        } else if (m > 1) {
            hrSize = dec.format(m).concat(" MB");
        } else if (k > 1) {
            hrSize = dec.format(k).concat(" KB");
        } else {
            hrSize = dec.format(b).concat(" Bytes");
        }

        return hrSize;
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static String getMimeType2(Uri uri, Context context) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    public static boolean checkPhoneLib(String contact) {
        try {
            PhoneNumberUtil phoneNumberUtil = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber result = phoneNumberUtil.parse(contact, "");
            return phoneNumberUtil.isValidNumber(result);
        } catch (NumberParseException e) {
            return false;
        }
    }

}
