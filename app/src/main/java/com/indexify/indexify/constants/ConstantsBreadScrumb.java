package com.indexify.indexify.constants;

/**
 * Created by amar on 08-01-2018.
 */

public class ConstantsBreadScrumb {
    public static final String TAG_B_NODE = "node";
    public static final String TAG_B_NODE_HOME = "home";
    public static int BREADSCRUMB_INIT = 1;
}
