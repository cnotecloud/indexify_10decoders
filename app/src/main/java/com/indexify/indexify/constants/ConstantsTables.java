package com.indexify.indexify.constants;

/**
 * Created by amar on 20-01-2018.
 */

public class ConstantsTables {

    // NODES table fields constants
    public static final String T_NODES = "NODES";
    public static final String T_NODES_ID = "ID";
    public static final String T_NODES_SERVER_ID = "SERVER_ID";
    public static final String T_NODES_DISPLAY_NAME = "DISPLAY_NAME";
    public static final String T_NODES_PARENT_SERVER_ID = "PARENT_SERVER_ID";
    public static final String T_NODES_IS_LEAF = "IS_LEAF";
    public static final String T_NODES_CREATED = "CREATED";
    public static final String T_NODES_UPDATED = "UPDATED";

    // PRODUCTS table fields
    public static final String T_PRODUCTS = "PRODUCTS";
    public static final String T_PRODUCTS_ID = "ID";
    public static final String T_PRODUCTS_SERVER_ID = "SERVER_ID";
    public static final String T_PRODUCTS_LEAF_NODE_SERVER_ID = "LEAF_NODE_SERVER_ID";
    public static final String T_PRODUCTS_NAME = "NAME";
    public static final String T_PRODUCTS_IMG_URLS = "IMG_URLS";
    public static final String T_PRODUCTS_LOCAL_IMG_URLS = "LOCAL_IMG_URLS";
    public static final String T_PRODUCTS_CONTROLS = "CONTROLS";

    // SCAN table fields
//    public static final String T_SCANS = "SCANS";
//    public static final String T_SCANS_ID = "ID";
//    public static final String T_SCANS_ORG_ID = "ORG_ID";
//    public static final String T_SCANS_ORG_NAME = "ORG_NAME";
//    public static final String T_SCANS_ORG_INFO = "ORG_INFO";
//    public static final String T_SCANS_USER_ID = "USER_ID";
//    public static final String T_SCANS_USER_ORG_ID = "USER_ORG_ID";

    public static final String T_SCANS = "SCANS";
    public static final String T_SCANS_ID = "ID";
    public static final String T_SCANS_SCANNED_ID = "SCANNED_ID";
    public static final String T_SCANS_SCANNED_NAME = "SCANNED_NAME";
    public static final String T_SCANS_IS_ORG = "IS_ORG";
    public static final String T_SCANS_USER_ID = "USER_ID";
    public static final String T_SCANS_USER_ORG_ID = "USER_ORG_ID";

    // MY_NODES table fields constants
    public static final String T_MY_NODES = "MY_NODES";
    public static final String T_MY_NODES_ID = "ID";
    public static final String T_MY_NODES_SERVER_ID = "SERVER_ID";
    public static final String T_MY_NODES_DISPLAY_NAME = "DISPLAY_NAME";
    public static final String T_MY_NODES_PARENT_SERVER_ID = "PARENT_SERVER_ID";
    public static final String T_MY_NODES_IS_LEAF = "IS_LEAF";
    public static final String T_MY_NODES_CREATED = "CREATED";
    public static final String T_MY_NODES_UPDATED = "UPDATED";
    public static final String T_MY_NODES_ORG_ID = "ORG_ID";

    // MY_PRODUCTS table fields
    public static final String T_MY_PRODUCTS = "MY_PRODUCTS";
    public static final String T_MY_PRODUCTS_ID = "ID";
    public static final String T_MY_PRODUCTS_SERVER_ID = "SERVER_ID";
    public static final String T_MY_PRODUCTS_LEAF_NODE_SERVER_ID = "LEAF_NODE_SERVER_ID";
    public static final String T_MY_PRODUCTS_NAME = "NAME";
    public static final String T_MY_PRODUCTS_IMG_URLS = "IMG_URLS";
    public static final String T_MY_PRODUCTS_LOCAL_IMG_URLS = "LOCAL_IMG_URLS";
    public static final String T_MY_PRODUCTS_CONTROLS = "CONTROLS";
    public static final String T_MY_PRODUCTS_ORG_ID = "ORG_ID";
}
