package com.indexify.indexify.constants;

/**
 * Created by amar on 18-01-2018.
 */

public class ConstantsEasySP {
    public static final String SP_INTRO_FLAG = "isIntro";
    public static final String SP_IS_LOGIN = "isLogin";
    public static final String SP_IS_SCAN_SHOW = "isScanShow";
    public static final String SP_TOKEN = "token";
    public static final String SP_REFRESH_TOKEN = "refreshToken";
    public static final String SP_USER_INFO = "userInfo";
    public static final String SP_USER_ORG_ID = "userOrgId";
    public static final String SP_USER_ID = "userId";

    public static final String SP_IS_DATA_DOWNLOAD = "isDataDownload";
    public static final String SP_IS_PRODUCTS_DOWNLOAD = "isProductsDownload";
    public static final String SP_IS_IMAGES_DOWNLOAD = "isImagesDownload";
    public static final String SP_IS_IMAGES_FOUND = "isImagesFound";
    public static final String SP_CATALOGUE_LIST = "CataloguesPdf";
}
