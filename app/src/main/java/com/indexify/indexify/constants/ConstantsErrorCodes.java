package com.indexify.indexify.constants;

/**
 * Created by amar on 31-01-2018.
 */

public class ConstantsErrorCodes {

    public static final int INVALID_USERNAME_PASSWORD = 10;
    public static final int EMAIL_NOT_VERIFIED = 17;
    public static final int EMAIL_ALREADY_EXISTS = 14;
    public static final int CONTACT_ALREADY_EXISTS = 29;
    public static final int VALIDATION_FAILED = 13;
    public static final int AUTHENTICATION_FAILED = 10;
    public static final int TOKEN_EXPIRED = 11;
}
