package com.indexify.indexify.constants;

import com.indexify.indexify.interceptor.ApplicationController;

/**
 * Created by amar on 10-01-2018.
 */

public class Constants {
    public static String BASE_ENDPOINT = ApplicationController.getBaseUrl();

    public static final int DEFAULT_PAGE = 0;
    public static final int DEFAULT_NODE_SIZE = 10000;
    public static final int DEFAULT_PRODUCT_GET_SIZE = 1000;
    public static final int DEFAULT_MY_PRODUCT_GET_SIZE = 100;
    public static final int DEFAULT_LAZY_LOADING_SIZE = 10;
    public static final int DEFAULT_USER_SCAN_LOADING_SIZE = 10;

    public static String IF_EMPTY = "-";

}
