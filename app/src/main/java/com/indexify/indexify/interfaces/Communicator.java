package com.indexify.indexify.interfaces;

/**
 * Created by Kalyan on 03-07-2017.
 */

public interface Communicator {
    public void pauseBackground();
}
