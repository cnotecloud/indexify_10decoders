package com.indexify.indexify.interfaces;

/**
 * Created by cnote on 13-02-2018.
 */

public interface DeleteOrganizationScanById {
    void deleteOrganizationScanById(String orgScanId);
}
