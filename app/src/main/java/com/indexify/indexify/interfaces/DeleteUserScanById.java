package com.indexify.indexify.interfaces;

/**
 * Created by cnote on 13-02-2018.
 */

public interface DeleteUserScanById {
    void deleteUserScanById(String userScanId);
}
