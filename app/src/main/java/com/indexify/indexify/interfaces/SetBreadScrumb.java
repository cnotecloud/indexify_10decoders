package com.indexify.indexify.interfaces;


import com.indexify.indexify.pojo.BreadScrumbPojo;

/**
 * Created by amar on 02-08-2017.
 */

public interface SetBreadScrumb {
    public void setBreadScrumb(BreadScrumbPojo breadScrumb);

    public void removeBreadScrumb(String tag);

    public void removeBreadScrumbOnClik(String tag);
}
