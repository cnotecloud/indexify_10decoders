package com.indexify.indexify.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.indexify.indexify.constants.ConstantsTables;
import com.indexify.indexify.pojo.ControlsPojo;
import com.indexify.indexify.pojo.NodePojo;
import com.indexify.indexify.pojo.OrganizationPojo;
import com.indexify.indexify.pojo.ProductPojo;
import com.indexify.indexify.pojo.ScanPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amar on 24-08-2016.
 */
public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    public SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }


    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public void clearAllTables() {
        try {
            if (!database.isOpen()) {
                open();
            }
            database.execSQL("delete from NODES");
         //   database.execSQL("delete from MY_SCANS");
            database.execSQL("delete from PRODUCTS");
        } catch (Exception e) {

        }
    }

    public void clearNodesTables() {
        try {
            if (!database.isOpen()) {
                open();
            }
            database.execSQL("delete from NODES");
        } catch (Exception e) {

        }
    }
    //clear Node and Product tables

    public void clearMyNodesAndMyProductsTables() {
        try {
            if (!database.isOpen()) {
                open();
            }
            database.execSQL("delete from MY_NODES");
            database.execSQL("delete from MY_PRODUCTS");
        } catch (Exception e) {

        }
    }


    public long insertNode(ContentValues values) {
        long x = 0L;
        try {
            if (!database.isOpen()) {
                open();
            }
            return database.insert(ConstantsTables.T_NODES, null, values);
        } catch (Exception e) {
            Log.e("insert node: ", e.toString());
        }
        return x;
    }

    //insert My Organization Nodes

    public long insertMyNodes(ContentValues values) {
        long x = 0L;
        try {
            if (!database.isOpen()) {
                open();
            }
            return database.insert(ConstantsTables.T_MY_NODES, null, values);
        } catch (Exception e) {
            Log.e("insert myNode: ", e.toString());
        }
        return x;
    }

    public long insertMyScanOrgInfo(ContentValues values) {
        long x = 0L;
        try {
            if (!database.isOpen()) {
                open();
            }
            return database.insert(ConstantsTables.T_SCANS, null, values);
        } catch (Exception e) {
            Log.e("insert scan: ", e.toString());
            x = -2;
        }
        return x;
    }

    public long insertProduct(ContentValues values) {
        long x = 0L;
        try {
            if (!database.isOpen()) {
                open();
            }
            return database.insert(ConstantsTables.T_PRODUCTS, null, values);
        } catch (Exception e) {
            Log.e("insert product : ", e.toString());
        }
        return x;
    }

    //insert My products in database

    public long insertMyProducts(ContentValues values) {
        long x = 0L;
        try {
            if (!database.isOpen()) {
                open();
            }
            return database.insert(ConstantsTables.T_MY_PRODUCTS, null, values);
        } catch (Exception e) {
            Log.e("insert myProduct : ", e.toString());
        }
        return x;
    }


    public List<NodePojo> getRootNodes() {
        List<NodePojo> alNodes = new ArrayList<>();
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select * from NODES where PARENT_SERVER_ID is null or PARENT_SERVER_ID = ''", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                NodePojo nodePojo = new NodePojo();

                nodePojo.setDisplayName(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_NODES_DISPLAY_NAME)));
                nodePojo.setId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_NODES_SERVER_ID)));
                nodePojo.setParentId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_NODES_PARENT_SERVER_ID)));
                nodePojo.setLeaf(cursor.getInt(cursor.getColumnIndex(ConstantsTables.T_NODES_IS_LEAF)) > 0);

                alNodes.add(nodePojo);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alNodes;
    }
    public List<NodePojo> getMyRootNodes() {
        List<NodePojo> alNodes = new ArrayList<>();
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select * from MY_NODES where PARENT_SERVER_ID is null or PARENT_SERVER_ID = ''", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                NodePojo nodePojo = new NodePojo();

                nodePojo.setDisplayName(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_NODES_DISPLAY_NAME)));
                nodePojo.setId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_NODES_SERVER_ID)));
                nodePojo.setParentId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_NODES_PARENT_SERVER_ID)));
                nodePojo.setLeaf(cursor.getInt(cursor.getColumnIndex(ConstantsTables.T_MY_NODES_IS_LEAF)) > 0);

                alNodes.add(nodePojo);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alNodes;
    }

    public List<NodePojo> getNodes(String node) {
        List<NodePojo> alNodes = new ArrayList<>();
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select * from NODES where PARENT_SERVER_ID like '" + node + "'", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                NodePojo nodePojo = new NodePojo();
                nodePojo.setDisplayName(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_NODES_DISPLAY_NAME)));
                nodePojo.setId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_NODES_SERVER_ID)));
                nodePojo.setParentId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_NODES_PARENT_SERVER_ID)));
                nodePojo.setLeaf(cursor.getInt(cursor.getColumnIndex(ConstantsTables.T_NODES_IS_LEAF)) > 0);
                alNodes.add(nodePojo);
                cursor.moveToNext();
            }
        } catch (Exception e) {

        }

        return alNodes;
    }

    //get My organization Nodes

    public List<NodePojo> getMyNodes(String node) {
        List<NodePojo> alNodes = new ArrayList<>();
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select * from MY_NODES where PARENT_SERVER_ID like '" + node + "'", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                NodePojo nodePojo = new NodePojo();
                nodePojo.setDisplayName(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_NODES_DISPLAY_NAME)));
                nodePojo.setId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_NODES_SERVER_ID)));
                nodePojo.setParentId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_NODES_PARENT_SERVER_ID)));
                nodePojo.setLeaf(cursor.getInt(cursor.getColumnIndex(ConstantsTables.T_MY_NODES_IS_LEAF)) > 0);
                alNodes.add(nodePojo);
                cursor.moveToNext();
            }
        } catch (Exception e) {

        }

        return alNodes;
    }

    public int getTotalCountProductsByLeafNode(String leafNode) {
        int totalCount = 0;
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select count(*) as C from PRODUCTS where LEAF_NODE_SERVER_ID like '" + leafNode + "'", null);
            if (cursor != null && cursor.moveToFirst()) {
                totalCount = cursor.getInt(cursor.getColumnIndex("C"));
            }
            cursor.close();

        } catch (Exception e) {

        }
        return totalCount;
    }
    // get my product list from database for search activity
    public int getTotalCountProductsSearch(String searchText) {
        int totalCount = 0;
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select count(*) as C from MY_PRODUCTS where NAME like '%" + searchText + "%' or CONTROLS like '%" + searchText + "%'", null);
            if (cursor != null && cursor.moveToFirst()) {
                totalCount = cursor.getInt(cursor.getColumnIndex("C"));
            }
            cursor.close();

        } catch (Exception e) {

        }
        return totalCount;
    }

    public int getImagesOrNotForProducts() {
        int totalCount = 0;
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select count(*) as C from MY_PRODUCTS where IMG_URLS is not null", null);
            if (cursor != null && cursor.moveToFirst()) {
                totalCount = cursor.getInt(cursor.getColumnIndex("C"));
            }
            cursor.close();

        } catch (Exception e) {

        }
        return totalCount;
    }
//get products from database
    public List<ProductPojo> getProductsByLeafNode(String leafNode) {
        Gson gson = new Gson();
        List<ProductPojo> alProducts = new ArrayList<>();
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select * from MY_PRODUCTS where LEAF_NODE_SERVER_ID like '" + leafNode + "'", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ProductPojo productPojo = new ProductPojo();
                productPojo.setName(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_NAME)));
                productPojo.setId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_SERVER_ID)));
                productPojo.setLeafId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_LEAF_NODE_SERVER_ID)));
                List<String> alImgUrl = gson.fromJson(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_IMG_URLS)), new TypeToken<List<String>>() {
                }.getType());
                productPojo.setImgUrls(alImgUrl);
                List<ControlsPojo> alControls = gson.fromJson(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_CONTROLS)), new TypeToken<List<ControlsPojo>>() {
                }.getType());
                productPojo.setControls(alControls);
                List<String> alLocalImgUrls = gson.fromJson(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_LOCAL_IMG_URLS)), new TypeToken<List<String>>() {
                }.getType());

                productPojo.setLocalImgUrls(alLocalImgUrls);
                alProducts.add(productPojo);
                cursor.moveToNext();
            }

        } catch (Exception e) {

        }

        return alProducts;
    }


    public List<OrganizationPojo> getMyScanLists() {
        Gson gson = new Gson();
        List<OrganizationPojo> alOrgizations = new ArrayList<>();
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select * from MY_SCANS order by  ID desc", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                String orgInfoJson = cursor.getString(cursor.getColumnIndex("ORG_INFO"));

                if (orgInfoJson != null && orgInfoJson.length() > 0) {
                    try {
                        OrganizationPojo organizationPojo = gson.fromJson(orgInfoJson, OrganizationPojo.class);
                        if (organizationPojo != null) {
                            alOrgizations.add(organizationPojo);
                        }
                    } catch (Exception e) {

                    }
                }
                cursor.moveToNext();
            }
        } catch (Exception e) {
        }
        return alOrgizations;
    }

    public List<ScanPojo> getOfflineScanListByUserIdAndUserOrgId(long userId, long userOrgId) {
        List<ScanPojo> alScans = new ArrayList<>();
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select * from " + ConstantsTables.T_SCANS + " where " + ConstantsTables.T_SCANS_USER_ID + " = " + userId + " and " + ConstantsTables.T_SCANS_USER_ORG_ID + " = " + userOrgId + " order by ID", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ScanPojo scanPojo = new ScanPojo();
                scanPojo.setId(cursor.getInt(cursor.getColumnIndex(ConstantsTables.T_SCANS_ID)));
                scanPojo.setScannedId(cursor.getInt(cursor.getColumnIndex(ConstantsTables.T_SCANS_SCANNED_ID)));
                scanPojo.setScannedName(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_SCANS_SCANNED_NAME)));
                scanPojo.setOrg(cursor.getInt(cursor.getColumnIndex(ConstantsTables.T_SCANS_IS_ORG)) > 0);
                scanPojo.setUserId(cursor.getInt(cursor.getColumnIndex(ConstantsTables.T_SCANS_USER_ID)));
                scanPojo.setUserOrgId(cursor.getInt(cursor.getColumnIndex(ConstantsTables.T_SCANS_USER_ORG_ID)));

                alScans.add(scanPojo);
                cursor.moveToNext();
            }
        } catch (Exception e) {
        }
        return alScans;
    }


    //search my products by name
    public List<ProductPojo> searchProductsByName(String searchText) {
        Gson gson = new Gson();
        List<ProductPojo> alProducts = new ArrayList<>();
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select * from MY_PRODUCTS where NAME like '%" + searchText + "%' or CONTROLS like '%" + searchText + "%'", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ProductPojo productPojo = new ProductPojo();
                productPojo.setName(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_NAME)));
                productPojo.setId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_SERVER_ID)));
                productPojo.setLeafId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_LEAF_NODE_SERVER_ID)));
                List<String> alImgUrl = gson.fromJson(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_IMG_URLS)), new TypeToken<List<String>>() {
                }.getType());
                productPojo.setImgUrls(alImgUrl);
                List<ControlsPojo> alControls = gson.fromJson(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_CONTROLS)), new TypeToken<List<ControlsPojo>>() {
                }.getType());
                productPojo.setControls(alControls);
                List<String> alLocalImgUrls = gson.fromJson(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_LOCAL_IMG_URLS)), new TypeToken<List<String>>() {
                }.getType());

                productPojo.setLocalImgUrls(alLocalImgUrls);
                alProducts.add(productPojo);
                cursor.moveToNext();
            }

        } catch (Exception e) {

        }

        return alProducts;
    }

    public List<ProductPojo> getProductsForImageDownload() {
        Gson gson = new Gson();
        List<ProductPojo> alProducts = new ArrayList<>();
        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("select * from MY_PRODUCTS ORDER BY ID", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ProductPojo productPojo = new ProductPojo();
                productPojo.setName(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_NAME)));
                productPojo.setId(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_SERVER_ID)));
                String imgUrl = cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_IMG_URLS));
                productPojo.setImgUrlString(imgUrl);
                List<String> alImgUrl = gson.fromJson(imgUrl, new TypeToken<List<String>>() {
                }.getType());
                productPojo.setImgUrls(alImgUrl);
                productPojo.setLocalImgUrlString(cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_LOCAL_IMG_URLS)));

                alProducts.add(productPojo);
                cursor.moveToNext();
            }

        } catch (Exception e) {

        }

        return alProducts;
    }

    public int updateLocalImageUrlOfProdcut(String server_id, List<String> imageUrls) {
        int i = 0;
        try {
            String localUrlJsonStirng = new Gson().toJson(imageUrls);


            ContentValues values = new ContentValues();
            values.put(ConstantsTables.T_MY_PRODUCTS_LOCAL_IMG_URLS, localUrlJsonStirng);
            i = database.update(ConstantsTables.T_MY_PRODUCTS, values, ConstantsTables.T_MY_PRODUCTS_SERVER_ID + " = '" + server_id + "'", null);
        } catch (SQLException e) {
            Log.e("update local url", e.toString());
        }
        return i;
    }
    // for syncing activity check whether node exists or not and if any new nodes are added
    // check if nodes exists in database and update the corresponding row
    // if new node is added add that node to database
    public String checkNodeExistOrNot(ContentValues values) {
        String nodeId = "";

        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("SELECT * FROM MY_NODES WHERE " + ConstantsTables.T_MY_NODES_SERVER_ID + " = '" + values.get(ConstantsTables.T_MY_NODES_SERVER_ID) + "'", null);
            if (cursor != null && cursor.moveToFirst()) {
                nodeId = cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_NODES_SERVER_ID));
            }
            cursor.close();

            if (nodeId != null && nodeId.length() > 0) {
                // update
                int updateCount = database.update(ConstantsTables.T_MY_NODES, values, ConstantsTables.T_MY_NODES_SERVER_ID + "= '" + nodeId + "'", null);
                Log.d("node update", String.valueOf(updateCount));
            } else {
                // insert
                long insertedId = database.insert(ConstantsTables.T_MY_NODES, null, values);
                Log.d("node insert", String.valueOf(insertedId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return nodeId;
    }

    public String checkProductExistOrNot(ContentValues values) {
        String productServerId = "";

        try {
            if (!database.isOpen()) {
                open();
            }
            Cursor cursor = database.rawQuery("SELECT * FROM MY_PRODUCTS WHERE " + ConstantsTables.T_MY_PRODUCTS_SERVER_ID + " = '" + values.get(ConstantsTables.T_MY_PRODUCTS_SERVER_ID) + "'", null);
            if (cursor != null && cursor.moveToFirst()) {
                productServerId = cursor.getString(cursor.getColumnIndex(ConstantsTables.T_MY_PRODUCTS_SERVER_ID));
            }
            cursor.close();

            if (productServerId != null && productServerId.length() > 0) {
                // update
                int updateCount = database.update(ConstantsTables.T_MY_PRODUCTS, values, ConstantsTables.T_MY_PRODUCTS_SERVER_ID + " = '" + productServerId + "'", null);
                Log.d("p update", String.valueOf(updateCount));
            } else {
                // insert
                long insertedId = database.insert(ConstantsTables.T_MY_PRODUCTS, null, values);
                Log.d("p insert", String.valueOf(insertedId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productServerId;
    }

    public int deleteMyscans(int orgId) {
        return database.delete("MY_SCANS", "ORG_ID=" + orgId, null);
    }

    public int deleteScanByInternalId(int id) {
        return database.delete(ConstantsTables.T_SCANS, ConstantsTables.T_SCANS_ID + "=" + id, null);
    }


}