package com.indexify.indexify;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eftimoff.viewpagertransformers.CubeOutTransformer;
import com.indexify.indexify.constants.ConstantsViewPager;
import com.indexify.indexify.database.DatabaseAccess;
import com.indexify.indexify.fragments.MyProductViewFragment;
import com.indexify.indexify.fragments.ProductViewFragment;
import com.indexify.indexify.interfaces.Communicator;
import com.indexify.indexify.interfaces.MyTouchListner;
import com.indexify.indexify.pojo.ProductPojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MyProductViewActivity extends FragmentActivity implements View.OnClickListener, Communicator, MyTouchListner {
    Timer swipeTimer;
    ImageView imgMinus;
    ImageView imgPlayStop;
    ImageView imgPlus;
    TextView tvSlideShow;
    ImageView imgLeftIndicator;
    ImageView imgRightIndicator;
    RelativeLayout rlSlideShowLayout;
    String slideShow = "Slide Show      ";
    List<ProductPojo> alProducts = new ArrayList<>();
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private int NUM_PAGES = 0;
    public int position1 = 0;


    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // getWindow().setStatusBarColor(ContextCompat.getColor(MyProductViewActivity.this,R.color.colorPrimaryDark));
        setContentView(R.layout.activity_my_product_view);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        imgMinus = (ImageView) findViewById(R.id.imgMinus);
        imgPlayStop = (ImageView) findViewById(R.id.imgPlayStop);
        imgPlayStop.setTag("stop");
        imgPlus = (ImageView) findViewById(R.id.imgPlus);
        imgMinus.setOnClickListener(this);
        imgPlayStop.setOnClickListener(this);
        imgPlus.setOnClickListener(this);
        tvSlideShow = (TextView) findViewById(R.id.tvSlideShow);
        rlSlideShowLayout = (RelativeLayout) findViewById(R.id.rlSlideShowLayout);
        tvSlideShow.setText(slideShow + (ConstantsViewPager.timerTime / 1000) + "  Sec.");

        // get data from database
        String type = getIntent().getStringExtra("type");
        position1 = getIntent().getIntExtra("position", 0);

        imgLeftIndicator = (ImageView) findViewById(R.id.imgLeftIndicator);
        imgRightIndicator = (ImageView) findViewById(R.id.imgRightIndicator);
        imgLeftIndicator.setOnClickListener(MyProductViewActivity.this);
        imgRightIndicator.setOnClickListener(MyProductViewActivity.this);


        if (type.equalsIgnoreCase("normal")) {
            alProducts = (List<ProductPojo>) getIntent().getSerializableExtra("products");
            toggleArrowVisibility(NUM_PAGES == 0, NUM_PAGES == alProducts.size() - 1);
            NUM_PAGES = alProducts.size();

        } else if (type.equalsIgnoreCase("search")) {
            String text = getIntent().getStringExtra("text");
            DatabaseAccess databaseAccess = DatabaseAccess.getInstance(MyProductViewActivity.this);
            databaseAccess.open();
            alProducts = databaseAccess.searchProductsByName(text);
            toggleArrowVisibility(NUM_PAGES == 0, NUM_PAGES == alProducts.size() - 1);
            NUM_PAGES = alProducts.size();
            databaseAccess.close();
        }


        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        // mPager.setPageTransformer(true, new ZoomOutPageTransformer());

        ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                toggleArrowVisibility(position == 0, position == alProducts.size() - 1);
                position1 = position;


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        mPager.addOnPageChangeListener(onPageChangeListener);
        mPager.setAdapter(mPagerAdapter);
        //  mPager.setPageTransformer(false, new Transformer());
        mPager.setPageTransformer(true, new CubeOutTransformer());
        mPager.setCurrentItem(position1);

        // auto slide

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (position1 == NUM_PAGES) {
                    position1 = 0;
                }
                mPager.setCurrentItem(position1++, true);
            }
        };

        swipeTimer = new Timer();
//        swipeTimer.schedule(new TimerTask() {
//
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, 1000, 3000);

        imgPlayStop.setTag("play");
        imgPlayStop.setImageResource(R.drawable.play);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        swipeTimer.cancel();
    }

    @Override
    public void onBackPressed() {
        /*if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }*/
        if (swipeTimer != null) {
            swipeTimer.cancel();
            finish();
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imgMinus) {
            if (ConstantsViewPager.timerTime <= 3000) {
                ConstantsViewPager.timerTime = 3000;
            } else {
                ConstantsViewPager.timerTime = ConstantsViewPager.timerTime - 1000;
            }

            tvSlideShow.setText(slideShow + (ConstantsViewPager.timerTime / 1000) + "  Sec.");

            // swipeTimer.cancel();

            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (position1 == NUM_PAGES) {
                        position1 = 0;
                    }
                    mPager.setCurrentItem(position1++, true);
                }
            };

           /* swipeTimer = new Timer();

            swipeTimer.schedule(new TimerTask() {

                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 1000, ConstantsViewPager.timerTime);*/


        } else if (v.getId() == R.id.imgPlus) {

            if (ConstantsViewPager.timerTime >= 10000) {
                ConstantsViewPager.timerTime = 10000;
            } else {
                ConstantsViewPager.timerTime = ConstantsViewPager.timerTime + 1000;
            }
            tvSlideShow.setText(slideShow + (ConstantsViewPager.timerTime / 1000) + "  Sec.");

            // swipeTimer.cancel();

            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (position1 == NUM_PAGES) {
                        position1 = 0;
                    }
                    mPager.setCurrentItem(position1++, true);
                }
            };

            //swipeTimer = new Timer();

           /* swipeTimer.schedule(new TimerTask() {

                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 1000, ConstantsViewPager.timerTime);*/

        } else if (v.getId() == R.id.imgPlayStop) {

            if (imgPlayStop.getTag().equals("stop")) {
                if (swipeTimer != null) {
                    swipeTimer.cancel();
                    imgPlayStop.setTag("play");

                    imgPlayStop.setImageResource(R.drawable.play);
                }


                //   Glide.with(ProductDetailsViewActivity.this).load(R.drawable.play).into(imgPlayStop);

            } else if (imgPlayStop.getTag().equals("play")) {

                final Handler handler = new Handler();
                final Runnable Update = new Runnable() {
                    public void run() {
                        if (position1 == NUM_PAGES) {
                            position1 = 0;
                        }
                        mPager.setCurrentItem(position1++, true);
                    }
                };

                swipeTimer = new Timer();

                swipeTimer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, 1000, ConstantsViewPager.timerTime);

                imgPlayStop.setTag("stop");

                imgPlayStop.setImageResource(R.drawable.stop);
            }

        } else if (v.getId() == R.id.imgLeftIndicator) {
            mPager.arrowScroll(View.FOCUS_LEFT);
        } else if (v.getId() == R.id.imgRightIndicator) {
            mPager.arrowScroll(View.FOCUS_RIGHT);
        }
    }

    @Override
    public void pauseBackground() {
        if (swipeTimer != null) {
            swipeTimer.cancel();
            imgPlayStop.setTag("play");
            imgPlayStop.setImageResource(R.drawable.play);
        }
    }

    @Override
    public void touch() {
        Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);


        if (rlSlideShowLayout.getVisibility() == View.INVISIBLE) {
            rlSlideShowLayout.setVisibility(View.VISIBLE);
            rlSlideShowLayout.setAnimation(slide_up);


        } else if (rlSlideShowLayout.getVisibility() == View.VISIBLE) {
            rlSlideShowLayout.setAnimation(slide_down);
            rlSlideShowLayout.setVisibility(View.INVISIBLE);

        }

    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */

    public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        // static ScreenSlidePageFragment s = new ScreenSlidePageFragment();

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            MyProductViewFragment myProductViewFragment = new MyProductViewFragment();
            Bundle b = new Bundle();
            b.putInt("test", position);
            b.putLong("orgId", getIntent().getLongExtra("orgId", 0));
            b.putSerializable("productPojo", alProducts.get(position));
            myProductViewFragment.setArguments(b);
            //return new ScreenSlidePageFragment();

            return myProductViewFragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }


    }

    public void toggleArrowVisibility(boolean isAtZeroIndex, boolean isAtLastIndex) {
        if (isAtZeroIndex)
            imgLeftIndicator.setVisibility(View.INVISIBLE);
        else
            imgLeftIndicator.setVisibility(View.VISIBLE);
        if (isAtLastIndex)
            imgRightIndicator.setVisibility(View.INVISIBLE);
        else
            imgRightIndicator.setVisibility(View.VISIBLE);

    }
}