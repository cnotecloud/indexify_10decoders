package com.indexify.indexify;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.indexify.indexify.adapter.PdfAdapter;

import java.util.List;

public class PdfListActivity extends AppCompatActivity {

    RecyclerView rvPdfList;
    LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getControls();
    }

    private void getControls() {

        rvPdfList = (RecyclerView) findViewById(R.id.rvPdfList);
        mLayoutManager = new LinearLayoutManager(PdfListActivity.this, LinearLayoutManager.VERTICAL, false);
        rvPdfList.setHasFixedSize(true);
        rvPdfList.setLayoutManager(mLayoutManager);

        getData();
    }

    private void getData() {
        List<String> alPdfList = (List<String>) getIntent().getSerializableExtra("pdfList");

        if (alPdfList != null && alPdfList.size() > 0) {
            PdfAdapter adapter = new PdfAdapter(alPdfList, PdfListActivity.this);
            rvPdfList.setAdapter(adapter);
            getSupportActionBar().setTitle("PDF List ( " + alPdfList.size() + " )");
        } else {
            Toast.makeText(this, "Not found", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
