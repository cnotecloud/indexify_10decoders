package com.indexify.indexify;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.hbb20.CountryCodePicker;
import com.indexify.indexify.api.SignUpApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.ResponsePojo;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;

import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{
    EditText txtFirstName;
    EditText txtLastName;
    EditText txtEmailId;
    EditText txtPassword;
    EditText txtConfirmPassword;
    EditText txtMobileNumber;
    TextView tvLogin;
    Button btnRegister;
    TextView tvFirstNameError;
    TextView tvLastNameError;
    TextView tvEmailIdError;
    TextView tvPasswordError;
    TextView tvConfirmPasswordError;
    TextView tvGuidelines;
    CountryCodePicker countryCodePicker;

    Typeface playBold;
    Typeface playRegular;

    TextView tvAlreadyHaveAccount;
    TextView tvSignUpTitle;
    TextView tvBySigning;
    TextView tvTermsandConditions;
    TextView tvIndexify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getControls();


    }

    private void getControls() {
        txtFirstName = (EditText) findViewById(R.id.txtFirstName);
        txtLastName = (EditText) findViewById(R.id.txtLastName);
        txtEmailId = (EditText) findViewById(R.id.txtEmailId);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtConfirmPassword = (EditText) findViewById(R.id.txtConfirmPassword);
        txtMobileNumber = (EditText) findViewById(R.id.txtMobileNumber);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        countryCodePicker = (CountryCodePicker) findViewById(R.id.ccp);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        tvGuidelines = (TextView) findViewById(R.id.tvGuidelines);
        tvFirstNameError = (TextView) findViewById(R.id.tvFirstNameError);
        tvLastNameError = (TextView) findViewById(R.id.tvLastNameError);
        tvEmailIdError = (TextView) findViewById(R.id.tvEmailIdError);
        tvPasswordError = (TextView) findViewById(R.id.tvPasswordError);
        tvConfirmPasswordError = (TextView) findViewById(R.id.tvConfirmPasswordError);
        tvLogin.setOnClickListener(SignUpActivity.this);
        btnRegister.setOnClickListener(SignUpActivity.this);
        tvAlreadyHaveAccount = (TextView) findViewById(R.id.tvAlreadyHaveAccount);
        tvSignUpTitle = (TextView) findViewById(R.id.tvSignUpTitle);
        tvBySigning = (TextView) findViewById(R.id.tvBySigning);
        tvTermsandConditions = (TextView) findViewById(R.id.tvTermsandConditions);
        tvIndexify = (TextView) findViewById(R.id.tvIndexify);
        //get font
        playBold = Typeface.createFromAsset(SignUpActivity.this.getAssets(),
                "fonts/play_bold.ttf");

        playRegular = Typeface.createFromAsset(SignUpActivity.this.getAssets(),
                "fonts/play_regular.ttf");
        //set font
        btnRegister.setTypeface(playBold);
        tvAlreadyHaveAccount.setTypeface(playRegular);
        tvLogin.setTypeface(playRegular);
        tvSignUpTitle.setTypeface(playBold);
        tvGuidelines.setTypeface(playRegular);
        tvBySigning.setTypeface(playRegular);
        tvTermsandConditions.setTypeface(playRegular);
        tvIndexify.setTypeface(playRegular);
        getData();
    }

    private void getData() {
//        String email = getIntent().getStringExtra("email");
//        String firstName = getIntent().getStringExtra("first_name");
//        String lastName = getIntent().getStringExtra("last_name");
//
//        if (StringUtils.isNotBlank(email)){
//            txtEmailId.setText(email);
//        }
//
//        if (StringUtils.isNotBlank(firstName)){
//            txtFirstName.setText(firstName);
//        }
//        if (StringUtils.isNotBlank(lastName)){
//            txtLastName.setText(lastName);
//        }
       txtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
           @Override
           public void onFocusChange(View view, boolean b) {
               if (b){
                   tvGuidelines.setVisibility(View.VISIBLE);
               }else{
                   tvGuidelines.setVisibility(View.GONE);
               }
           }
       });

        txtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvLastNameError.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        txtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvFirstNameError.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        txtConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvConfirmPasswordError.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        txtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvPasswordError.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        txtEmailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               tvEmailIdError.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void validateUserDetails() {
        String message = "";
        String firstName = "";
        String lastName = "";
        String emailId = "";
        String password = "";
        String confirmPassword = "";
        String mobileNumber = "";
        String countryCode = "";
        boolean mobileNumberValid = false;

        firstName = txtFirstName.getText().toString().trim();
        lastName = txtLastName.getText().toString().trim();
        emailId = txtEmailId.getText().toString().trim();
        password = txtPassword.getText().toString().trim();
        confirmPassword = txtConfirmPassword.getText().toString().trim();
        mobileNumber = txtMobileNumber.getText().toString().trim();
        countryCode = countryCodePicker.getSelectedCountryCode();

        if ((mobileNumber != null) && (mobileNumber.length() > 0)) {
            if ((countryCode != null) && (countryCode.length() > 0)) {
                mobileNumber = "+"+countryCode + mobileNumber;
                mobileNumberValid = ConstantsMethods.checkPhoneLib(mobileNumber);
            }


        }

        if (firstName.length() == 0) {
            tvFirstNameError.setVisibility(View.VISIBLE);
            message = message + "FirstName\n";
        }
        if (lastName.length() == 0) {
            tvLastNameError.setVisibility(View.VISIBLE);
            message = message + "LastName\n";
        }
        if (emailId.length() == 0) {
            tvEmailIdError.setVisibility(View.VISIBLE);
            message = message + "EmailId\n";
        }
        if (password.length() == 0) {
            tvPasswordError.setVisibility(View.VISIBLE);
            message = message + "Password\n";
        }
        if (confirmPassword.length() == 0) {
            tvConfirmPasswordError.setVisibility(View.VISIBLE);
            message = message + "Confirm Password\n";
        }

        if (message.length() == 0) {
            if ((firstName.contains(" ") || (lastName.contains(" "))) ) {
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText("No Spacs Allowed");
                sweetAlertDialog.setContentText("White Spaces are not allowed in Firstname and Last Name");
                sweetAlertDialog.setCancelable(true);
                sweetAlertDialog.setConfirmText("    OK    ");
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                    }
                });
                sweetAlertDialog.show();
            } else {


                boolean emailIdValid = false;

                emailIdValid = org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(emailId);
                //if mobile number is entered and is valid
                if (mobileNumber.length() > 0) {
                    if (mobileNumberValid) {
                        if (emailIdValid) {
                            if ((password.length() >= 6) && (password.length() <= 15)) {
                                if (password.equalsIgnoreCase(confirmPassword)) {
                                    JsonObject jsonObject = new JsonObject();
                                    jsonObject.addProperty("email", emailId);
                                    jsonObject.addProperty("contact", mobileNumber);
                                    jsonObject.addProperty("password", password);
                                    jsonObject.addProperty("matchingPassword", confirmPassword);
                                    jsonObject.addProperty("firstName", firstName);
                                    jsonObject.addProperty("lastName", lastName);
                                    if (ConstantsMethods.isConnected(SignUpActivity.this)) {
                                        ConstantsMethods.showProgessDialog(SignUpActivity.this, "Registering...");
                                        registerUser(jsonObject);
                                    }
                                } else {
                                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
                                    sweetAlertDialog.setTitleText("Mismatch!");
                                    sweetAlertDialog.setContentText("Password Does Not Match");
                                    sweetAlertDialog.setCancelable(true);
                                    sweetAlertDialog.setConfirmText("    OK    ");
                                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.cancel();
                                        }
                                    });
                                    sweetAlertDialog.show();
                                }


                            } else {
                                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
                                sweetAlertDialog.setTitleText("Invalid");
                                sweetAlertDialog.setContentText("Password Should be Between Six and Fifteen Characters");
                                sweetAlertDialog.setCancelable(true);
                                sweetAlertDialog.setConfirmText("    OK    ");
                                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.cancel();
                                    }
                                });
                                sweetAlertDialog.show();
                            }
                        } else {
                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText("Invalid!");
                            sweetAlertDialog.setContentText("Please Enter Valid Email Id");
                            sweetAlertDialog.setCancelable(true);
                            sweetAlertDialog.setConfirmText("    OK    ");
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.cancel();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    } else {
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
                        sweetAlertDialog.setTitleText("Invalid!");
                        sweetAlertDialog.setContentText("Please Enter Valid Mobile Number");
                        sweetAlertDialog.setCancelable(true);
                        sweetAlertDialog.setConfirmText("    OK    ");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.cancel();
                            }
                        });
                        sweetAlertDialog.show();
                    }
                } else if (mobileNumber.length() == 0) {
                    //mobile number not entered and post remaining data
                    if (emailIdValid) {
                        if ((password.length() >= 6) && (password.length() <= 15)) {
                            if (password.equalsIgnoreCase(confirmPassword)) {
                                JsonObject jsonObject = new JsonObject();
                                jsonObject.addProperty("email", emailId);
                                jsonObject.addProperty("password", password);
                                jsonObject.addProperty("matchingPassword", confirmPassword);
                                jsonObject.addProperty("firstName", firstName);
                                jsonObject.addProperty("lastName", lastName);
                                if (ConstantsMethods.isConnected(SignUpActivity.this)) {
                                    ConstantsMethods.showProgessDialog(SignUpActivity.this, "Registering...");
                                    registerUser(jsonObject);
                                }

                            } else {
                                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
                                sweetAlertDialog.setTitleText("Mismatch!");
                                sweetAlertDialog.setContentText("Password Does Not Match");
                                sweetAlertDialog.setCancelable(true);
                                sweetAlertDialog.setConfirmText("    OK    ");
                                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.cancel();
                                    }
                                });
                                sweetAlertDialog.show();
                            }


                        } else {
                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText("Invalid");
                            sweetAlertDialog.setContentText("Password Should be Between Six and Fifteen Characters");
                            sweetAlertDialog.setCancelable(true);
                            sweetAlertDialog.setConfirmText("    OK    ");
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.cancel();
                                }
                            });
                            sweetAlertDialog.show();
                        }
                    } else {
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
                        sweetAlertDialog.setTitleText("Invalid!");
                        sweetAlertDialog.setContentText("Please Enter Valid Email Id");
                        sweetAlertDialog.setCancelable(true);
                        sweetAlertDialog.setConfirmText("    OK    ");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.cancel();
                            }
                        });
                        sweetAlertDialog.show();
                    }
                }
            }
        }
    }

    private void registerUser(JsonObject jsonObject) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Constants.BASE_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        Retrofit retrofit = builder.build();

        SignUpApi signUpApi = retrofit.create(SignUpApi.class);
        Single<ResponsePojo> createUserResponse = signUpApi.registerUser(jsonObject);
        createUserResponse.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<ResponsePojo>() {
            @Override
            public void onSuccess(@NonNull ResponsePojo responsePojo) {
                ConstantsMethods.cancleProgessDialog();
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                sweetAlertDialog.setTitleText("Registration Successful");
                sweetAlertDialog.setContentText("An Email For Confirmation Has Been Sent to your Registered Email Id, Please Verify");
                sweetAlertDialog.setCancelable(false);
                sweetAlertDialog.setConfirmText("    OK    ");
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        finish();
                    }
                });
                sweetAlertDialog.show();
            }

            @Override
            public void onError(@NonNull Throwable e) {
                try {
                    HttpException ex = (HttpException) e;
                    String er = ex.response().errorBody().string();
                    ResponsePojo erroPojo = new Gson().fromJson(er, ResponsePojo.class);

                    if (erroPojo != null) {
                        if (erroPojo.getStatus() == 409 && erroPojo.getResponseCode() == ConstantsErrorCodes.EMAIL_ALREADY_EXISTS) {
                            showError("Email Already Exists!", "Please check your email.");
                        } else if (erroPojo.getStatus() == 400 && erroPojo.getResponseCode() == ConstantsErrorCodes.VALIDATION_FAILED) {
                            showError("Validation Failed", "Please check your Email and Password");
                        }else if (erroPojo.getStatus() == 409 && erroPojo.getResponseCode() == ConstantsErrorCodes.CONTACT_ALREADY_EXISTS) {
                            showError("Phone Number Already Exists!", "Please check your PhoneNumber.");
                        } else if (erroPojo.getStatus() == 403) {
                            showError("Not access", "Please try again or contact admin.");
                        } else if (erroPojo.getStatus() == 500) {
                            showError("Intermal server error", "Something went wrong please try later.");
                        }
                    }
                } catch (Exception e1) {
                    showError("Error", "Something went wrong please try later.");
                } finally {
                    ConstantsMethods.cancleProgessDialog();
                }
            }
        });

    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnRegister) {
            validateUserDetails();
        } else if (view.getId() == R.id.tvLogin) {
            finish();
        }
    }

    private void showError(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });

    }
}
