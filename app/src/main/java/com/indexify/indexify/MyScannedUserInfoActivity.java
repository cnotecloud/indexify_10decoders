package com.indexify.indexify;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indexify.indexify.api.UserApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.MyScanUserInfoPojo;
import com.indexify.indexify.pojo.ResponsePojo;

import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class MyScannedUserInfoActivity extends AppCompatActivity {
    TextView tvFirstName;
    TextView tvLastName;
    TextView tvContactNumber;
    TextView tvEmailId;
    TextView tvCountry;
    TextView tvRegion;
    TextView tvCity;
    TextView tvAddress;
    TextView tvPostalCode;
    long userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_scanned_user_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(WordUtils.capitalize(getIntent().getStringExtra("userName")));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        userId = getIntent().getLongExtra("userId",0);
        getControls();

    }

    private void getControls() {
        tvFirstName = (TextView) findViewById(R.id.tvFirstName);
        tvLastName = (TextView) findViewById(R.id.tvLastName);
        tvContactNumber = (TextView) findViewById(R.id.tvContactNumber);
        tvEmailId = (TextView) findViewById(R.id.tvEmailId);
        tvCountry = (TextView) findViewById(R.id.tvCountry);
        tvRegion = (TextView) findViewById(R.id.tvRegion);
        tvCity = (TextView) findViewById(R.id.tvCity);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvPostalCode = (TextView) findViewById(R.id.tvPostalCode);
        getData();
    }

    private void getData() {
        if (ConstantsMethods.isConnected(MyScannedUserInfoActivity.this)) {
            ConstantsMethods.showProgessDialog(MyScannedUserInfoActivity.this, "Loading...");
            getUserInfoFromServer();
        }
    }

    private void getUserInfoFromServer() {
        UserApi userApi = retrofit.create(UserApi.class);
        Single<MyScanUserInfoPojo> userInfo = userApi.getScannedUserInfo(userId);
        userInfo.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<MyScanUserInfoPojo>() {
            @Override
            public void onSuccess(MyScanUserInfoPojo myScanUserInfoPojo) {
                ConstantsMethods.cancleProgessDialog();
                setUserDetails(myScanUserInfoPojo);
            }

            @Override
            public void onError(Throwable e) {
                try {
                    HttpException ex = (HttpException) e;
                    String er = ex.response().errorBody().string();
                    ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                    if (responsePojo != null) {
                        if (responsePojo.getStatus() == 404 && responsePojo.getResponseCode() == ConstantsErrorCodes.INVALID_USERNAME_PASSWORD) {
                            showErrorwithFinish("Not Found", "No Organization Found");
                        } else if (responsePojo.getStatus() == 403) {
                            showErrorwithFinish("No access", "Please try again or contact admin.");
                        } else if (responsePojo.getStatus() == 500) {
                            showErrorwithFinish("Internal server error", "Something went wrong please try later.");
                        }else if (responsePojo.getStatus() == 400) {
                            showErrorwithFinish("Bad Request", "Something went wrong please try later.");
                        }
                    }
                } catch (Exception e1) {
                    showErrorwithFinish("Error", "Something went wrong please try later.");
                } finally {
                    ConstantsMethods.cancleProgessDialog();
                }
            }
        });
    }
    private void showError(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MyScannedUserInfoActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });

    }

    private void setUserDetails(MyScanUserInfoPojo myScanUserInfoPojo) {
        if (myScanUserInfoPojo != null) {
            if ((myScanUserInfoPojo.getFirstName() != null) && (myScanUserInfoPojo.getFirstName().length() > 0)) {
                tvFirstName.setText(WordUtils.capitalize(myScanUserInfoPojo.getFirstName()));
            } else {
                tvFirstName.setText(Constants.IF_EMPTY);
            }
            if ((myScanUserInfoPojo.getLastName() != null) && (myScanUserInfoPojo.getLastName().length() > 0)) {
                tvLastName.setText(WordUtils.capitalize(myScanUserInfoPojo.getLastName()));
            } else {
                tvLastName.setText(Constants.IF_EMPTY);
            }
            if (myScanUserInfoPojo.getEmail() != null) {
                if ((myScanUserInfoPojo.getEmail().getValue() != null) && (myScanUserInfoPojo.getEmail().getValue().toString().length() > 0)){
                    tvEmailId.setText(myScanUserInfoPojo.getEmail().getValue().toString());
                }else{
                    tvEmailId.setText(Constants.IF_EMPTY);
                }

            } else {
                tvEmailId.setText(Constants.IF_EMPTY);
            }
            if (myScanUserInfoPojo.getContact() != null) {
                if ((myScanUserInfoPojo.getContact().getValue() != null) && (myScanUserInfoPojo.getContact().getValue().toString().length() > 0)){
                    tvContactNumber.setText(myScanUserInfoPojo.getContact().getValue().toString());
                }else{
                    tvContactNumber.setText(Constants.IF_EMPTY);
                }
            } else {
                tvContactNumber.setText(Constants.IF_EMPTY);
            }
            if (myScanUserInfoPojo.getLocationPojo() != null) {
                if ((myScanUserInfoPojo.getLocationPojo().getAddress() != null) && (myScanUserInfoPojo.getLocationPojo().getAddress().length() > 0)) {
                    tvAddress.setText(WordUtils.capitalize(myScanUserInfoPojo.getLocationPojo().getAddress()));
                } else {
                    tvAddress.setText(Constants.IF_EMPTY);
                }
                if ((myScanUserInfoPojo.getLocationPojo().getCountry() != null) && (myScanUserInfoPojo.getLocationPojo().getCountry().length() > 0)) {
                    tvCountry.setText(WordUtils.capitalize(myScanUserInfoPojo.getLocationPojo().getCountry()));
                } else {
                    tvCountry.setText(Constants.IF_EMPTY);
                }
                if ((myScanUserInfoPojo.getLocationPojo().getRegion() != null) && (myScanUserInfoPojo.getLocationPojo().getRegion().length() > 0)) {
                    tvRegion.setText(WordUtils.capitalize(myScanUserInfoPojo.getLocationPojo().getRegion()));
                } else {
                    tvRegion.setText(Constants.IF_EMPTY);
                }
                if ((myScanUserInfoPojo.getLocationPojo().getCity() != null) && (myScanUserInfoPojo.getLocationPojo().getCity().length() > 0)) {
                    tvCity.setText(WordUtils.capitalize(myScanUserInfoPojo.getLocationPojo().getCity()));
                } else {
                    tvCity.setText(Constants.IF_EMPTY);
                }
                if ((myScanUserInfoPojo.getLocationPojo().getPostalCode() != null) && (myScanUserInfoPojo.getLocationPojo().getPostalCode().length() > 0)) {
                    tvPostalCode.setText(myScanUserInfoPojo.getLocationPojo().getPostalCode());
                } else {
                    tvPostalCode.setText(Constants.IF_EMPTY);
                }
            } else {
                tvAddress.setText(Constants.IF_EMPTY);
                tvCountry.setText(Constants.IF_EMPTY);
                tvRegion.setText(Constants.IF_EMPTY);
                tvCity.setText(Constants.IF_EMPTY);
                tvPostalCode.setText(Constants.IF_EMPTY);
            }
        }else{
            tvFirstName.setText(Constants.IF_EMPTY);
            tvLastName.setText(Constants.IF_EMPTY);
            tvContactNumber.setText(Constants.IF_EMPTY);
            tvEmailId.setText(Constants.IF_EMPTY);
            tvAddress.setText(Constants.IF_EMPTY);
            tvCountry.setText(Constants.IF_EMPTY);
            tvRegion.setText(Constants.IF_EMPTY);
            tvCity.setText(Constants.IF_EMPTY);
            tvPostalCode.setText(Constants.IF_EMPTY);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showErrorwithFinish(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MyScannedUserInfoActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        });

    }

}
