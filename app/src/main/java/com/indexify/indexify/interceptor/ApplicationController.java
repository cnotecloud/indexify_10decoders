package com.indexify.indexify.interceptor;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.indexify.indexify.constants.Constants;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by amar on 03-03-2017.
 */

public class ApplicationController extends MultiDexApplication {
    private static Context context;
    public static Retrofit retrofit;
    public static ExecutorService executorService;
    public static String profile = Dev.NAME;
    public static boolean isShowLoginScreen = false;

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationController.context = getApplicationContext();
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
        // createing okhttpclient object
        OkHttpClient okhttpBulider = new OkHttpClient.Builder()
                .addInterceptor(new Myinterceptor(context))          // this is an interceptor adding in okhttpclient
                .connectTimeout(600, TimeUnit.SECONDS)
                //    .authenticator(new TokenAuthenticator(getApplicationContext())) // for token authenticator
                .readTimeout(600, TimeUnit.SECONDS)
                .build();

        // create a retrofitBuilder object
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Constants.BASE_ENDPOINT)                         // set Base url (Endpoint)
                .client(okhttpBulider)                                   // adding the okhttp clint in retrofit object
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        // adding converter factory in retofit object

        // createing retrofit object using retrofitbuilder object
        retrofit = builder.build();
    }
    public static Context getAppContext() {
        return ApplicationController.context;
    }

    public static class Dev {
        public static final String BASE_URL = "http://event1.cnote.in:8080/indexify/api/";
        public static final String NAME = "dev";
    }

    public static class Prod {
       // public static final String BASE_URL = "http://api.indexify.co:8080/indexify/api/";
        public static final String BASE_URL = "http://api1.indexify.co:8080/indexify/api/";
        public static final String NAME = "prod";
    }


    public static String getBaseUrl() {
        if (Dev.NAME.equalsIgnoreCase(profile)) {
            return Dev.BASE_URL;
        } else if (Prod.NAME.equalsIgnoreCase(profile)) {
            return Prod.BASE_URL;
        } else {
            return Dev.BASE_URL;
        }
    }
}
