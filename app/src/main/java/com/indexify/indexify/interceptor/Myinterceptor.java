package com.indexify.indexify.interceptor;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

import com.indexify.indexify.LoginPreviousActivity;
import com.indexify.indexify.api.LoginApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.pojo.TokenPojo;
import com.white.easysp.EasySP;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by amar on 02-03-2017.
 */

public class Myinterceptor implements Interceptor {
    Context context;

    public Myinterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        String token = EasySP.init(context).getString(ConstantsEasySP.SP_TOKEN, "");
        //token = token+ "_expired";

        Request request = chain.request();
        Request.Builder builder = request.newBuilder()
                .header("X-Requested-With", "XMLHttpRequest")
                .header("Content-Type", "application/json")
                .header("Cache-Control", "no-cache")
                .header("Accept", "application/json");

        if (token.length() > 0) {
            setAuthHeader(builder, token); // token
        }
        request = builder.build();
        Response response = chain.proceed(request);

        if (response.code() == 401) {
            try {
                String refreshToken = EasySP.init(context).getString(ConstantsEasySP.SP_REFRESH_TOKEN);
                //refreshToken = refreshToken + "_expired";
                Retrofit.Builder builderToken = new Retrofit.Builder()
                        .baseUrl(Constants.BASE_ENDPOINT)                         // set Base url (Endpoint)                                                         // adding the okhttp clint in retrofit object
                        .addConverterFactory(GsonConverterFactory.create());

                Retrofit retrofitToken = builderToken.build();
                final LoginApi loginApi = retrofitToken.create(LoginApi.class);

                Call<TokenPojo> newTokenFromRefreshToken = loginApi.getNewTokenFromRefreshToken(refreshToken);
                retrofit2.Response<TokenPojo> response1 = newTokenFromRefreshToken.execute();

                if (response1.code() == 200) {
                    TokenPojo tokenPojo = response1.body();
                    // if token pojo call gets 401 then show login screen.
                    if (tokenPojo != null) {
                        EasySP.init(context).putString(ConstantsEasySP.SP_TOKEN, "Bearer " + tokenPojo.getToken());
                        setAuthHeader(builder, "Bearer " + tokenPojo.getToken()); //set auth token to updated
                        request = builder.build();
                        // Response response2 = chain.proceed(request);

                        return chain.proceed(request);
                    } else {
                        // if token pojo null then show login screen;
                    }
                } else if (response1.code() == 401) {
                    if (!ApplicationController.isShowLoginScreen) {
                        EasySP.init(ApplicationController.getAppContext()).remove(ConstantsEasySP.SP_TOKEN);
                        EasySP.init(ApplicationController.getAppContext()).remove(ConstantsEasySP.SP_REFRESH_TOKEN);
                        EasySP.init(ApplicationController.getAppContext()).putBoolean(ConstantsEasySP.SP_IS_LOGIN, false);
                        EasySP.init(ApplicationController.getAppContext()).putBoolean(ConstantsEasySP.SP_IS_SCAN_SHOW, false);
                        EasySP.init(ApplicationController.getAppContext()).putString(ConstantsEasySP.SP_TOKEN, "");
                        EasySP.init(ApplicationController.getAppContext()).putString(ConstantsEasySP.SP_USER_INFO, "");
                        EasySP.init(ApplicationController.getAppContext()).putLong(ConstantsEasySP.SP_USER_ID, 0);
                        EasySP.init(ApplicationController.getAppContext()).putLong(ConstantsEasySP.SP_USER_ORG_ID, 0);

                        Intent intent = new Intent(ApplicationController.getAppContext(), LoginPreviousActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra("fromInterceptor",true);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        ApplicationController.getAppContext().startActivity(intent);
                        ApplicationController.isShowLoginScreen = true;
                        return response;
                    }
                }
            } catch (Exception e) {

                if (!ApplicationController.isShowLoginScreen) {
                    EasySP.init(ApplicationController.getAppContext()).remove(ConstantsEasySP.SP_TOKEN);
                    EasySP.init(ApplicationController.getAppContext()).remove(ConstantsEasySP.SP_REFRESH_TOKEN);
                    EasySP.init(ApplicationController.getAppContext()).putBoolean(ConstantsEasySP.SP_IS_LOGIN, false);
                    EasySP.init(ApplicationController.getAppContext()).putBoolean(ConstantsEasySP.SP_IS_SCAN_SHOW, true);
                    EasySP.init(ApplicationController.getAppContext()).putString(ConstantsEasySP.SP_TOKEN, "");
                    EasySP.init(ApplicationController.getAppContext()).putString(ConstantsEasySP.SP_USER_INFO, "");
                    EasySP.init(ApplicationController.getAppContext()).putLong(ConstantsEasySP.SP_USER_ID, 0);
                    EasySP.init(ApplicationController.getAppContext()).putLong(ConstantsEasySP.SP_USER_ORG_ID, 0);

                    Intent intent = new Intent(ApplicationController.getAppContext(), LoginPreviousActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    ApplicationController.getAppContext().startActivity(intent);
                    ApplicationController.isShowLoginScreen = true;
                    return response;
                }
            }
        }
        return response;
    }

    private void setAuthHeader(Request.Builder builder, String token) {
        if (token != null) //Add Auth token to each request if authorized
            builder.header("Authorization", token);
    }
}
