package com.indexify.indexify;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.pojo.UserInfoPojo;
import com.white.easysp.EasySP;


import net.glxn.qrgen.android.QRCode;
import net.glxn.qrgen.core.image.ImageType;

import org.apache.commons.lang3.text.WordUtils;

public class MyQrActivity extends AppCompatActivity {
    ImageView imgQr;
    TextView tvName;
    TextView tvUserorOrg;
    TextView tvShowQrMessage;
    TextView tvIndexifyId;
    ShareActionProvider shareActionProvider;
    boolean isOrg = false;
    UserInfoPojo userInfoPojo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_qr);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getControls();
    }

    private void getControls() {
        imgQr = (ImageView) findViewById(R.id.imgQr);
        tvName = (TextView) findViewById(R.id.tvName);
        tvUserorOrg = (TextView) findViewById(R.id.tvUserorOrg);
        tvShowQrMessage = (TextView) findViewById(R.id.tvShowQrMessage);
        tvIndexifyId = (TextView) findViewById(R.id.tvIndexifyId);

        getData();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (isOrg) {
            getMenuInflater().inflate(R.menu.menu_share_catalogue, menu);
            MenuItem item = menu.findItem(R.id.menu_item_share);
            shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
            setShareIntent(createShareIntent());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }else if (id == R.id.menu_item_share){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        String userInfo = EasySP.init(MyQrActivity.this).getString(ConstantsEasySP.SP_USER_INFO);
        if (userInfo != null && userInfo.length() > 0) {
            try {
                Gson gson = new Gson();
                userInfoPojo = gson.fromJson(userInfo, UserInfoPojo.class);
                if (userInfoPojo != null) {

                  /*  QRUserCreatePojo qrUserCreatePojo = new QRUserCreatePojo();
                    qrUserCreatePojo.setId(userInfoPojo.getId());
                    qrUserCreatePojo.setName(userInfoPojo.getFirstName() + " " + userInfoPojo.getLastName());
                    qrUserCreatePojo.setEmail(userInfoPojo.getEmail());
                    qrUserCreatePojo.setMobile(userInfoPojo.getContact());*/
                    if ((userInfoPojo.getOrganizationPojo() != null) && (userInfoPojo.getOrganizationPojo().getId() > 0)) {
                        tvIndexifyId.setVisibility(View.VISIBLE);
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("id", userInfoPojo.getOrganizationPojo().getId());
                        if ((userInfoPojo.getOrganizationPojo().getDisplayName() != null) && (userInfoPojo.getOrganizationPojo().getDisplayName().length() > 0)) {
                            jsonObject.addProperty("displayName", userInfoPojo.getOrganizationPojo().getDisplayName());
                            tvName.setText(WordUtils.capitalize(userInfoPojo.getOrganizationPojo().getDisplayName()));
                            tvUserorOrg.setText("( PRODUCT CATALOGUE )");
                            tvIndexifyId.setText("Indexify ID : "+userInfoPojo.getOrganizationPojo().getId());
                            tvShowQrMessage.setText("Show this QR to share digital product catalogue\n(My QR)");
                            isOrg = true;
                        }


                        String jsonQrUserPojo = gson.toJson(jsonObject);
                        Bitmap myBitmap = QRCode.from(jsonQrUserPojo).withSize(400, 400).to(ImageType.PNG).bitmap();
                        myBitmap = Bitmap.createScaledBitmap(myBitmap, 400, 400, true);
                        imgQr.setImageBitmap(myBitmap);
                    }else{
                        isOrg = false;
                        tvIndexifyId.setVisibility(View.GONE);
                        if (userInfoPojo.getId() > 0){
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty("id", userInfoPojo.getId());
                            if ((userInfoPojo.getFirstName() != null && userInfoPojo.getLastName() != null) && (userInfoPojo.getFirstName().length() > 0  && userInfoPojo.getLastName().length() > 0)) {
                                jsonObject.addProperty("name", userInfoPojo.getFirstName() + " " + userInfoPojo.getLastName());
                                tvName.setText(WordUtils.capitalize(userInfoPojo.getFirstName() + " " + userInfoPojo.getLastName()));
                                tvUserorOrg.setText("( USER )");
                                tvShowQrMessage.setText("Show this QR to share User Information\n(My QR)");
                            }
                            String jsonQrUserPojo = gson.toJson(jsonObject);
                            Bitmap myBitmap = QRCode.from(jsonQrUserPojo).withSize(400, 400).to(ImageType.PNG).bitmap();
                            myBitmap = Bitmap.createScaledBitmap(myBitmap, 400, 400, true);
                            imgQr.setImageBitmap(myBitmap);
                        }
                    }


                  /*  JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("id", userInfoPojo.getId());
                    jsonObject.addProperty("name", userInfoPojo.getFirstName() + " " + userInfoPojo.getLastName());
                    jsonObject.addProperty("email", userInfoPojo.getEmail());
                    jsonObject.addProperty("mobile", userInfoPojo.getContact());

                    String j = gson.toJson(jsonObject);*/


                }
            } catch (Exception e) {

            }
        } else {
            Toast.makeText(this, "not found", Toast.LENGTH_SHORT).show();
        }


    }


    private void setShareIntent(Intent shareIntent) {
        if (shareActionProvider != null) {
            if (isOrg) {
                shareActionProvider.setShareIntent(shareIntent);
            }


        }
    }
    private Intent createShareIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                "http://indexify.co/webview.html?id="+userInfoPojo.getOrganizationPojo().getId());
        return shareIntent;
    }
}
