package com.indexify.indexify;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.Manifest.permission.CAMERA;

public class ScanQrCodeFromVisionActivity extends AppCompatActivity {
    EditText txtEnterIndexifyId;
    Button btnSubmitIndexifyId;
    SurfaceView surfaceView;
    TextView skip;
    GLSurfaceView glSurfaceView;
    ImageView imgScanGif;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qrcode_from_vision);
        checkAndRequestPermissions();
        getControls();

    }

    private void getControls() {
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        skip = (TextView) findViewById(R.id.tvSkip);
        imgScanGif = (ImageView) findViewById(R.id.imgScanGif);
        glSurfaceView = (GLSurfaceView) findViewById(R.id.glSurfaceView);
        txtEnterIndexifyId = (EditText) findViewById(R.id.txtEnterIndexifyId);
        btnSubmitIndexifyId = (Button) findViewById(R.id.btnSubmitIndexifyId);

        getData();
    }

    private void getData() {
        btnSubmitIndexifyId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = "";
                id = txtEnterIndexifyId.getText().toString().trim();
                if (id.length() > 0){
                    Intent intent = new Intent();
                    intent.putExtra("indexify_id", id);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }else {

                }
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        Glide.with(ScanQrCodeFromVisionActivity.this).load(R.drawable.qr_scan_area).diskCacheStrategy(DiskCacheStrategy.NONE).into(imgScanGif);
        startScan();

    }

    private void startScan() {
        glSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        glSurfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
        glSurfaceView.setEGLContextClientVersion(2);
       glSurfaceView.setRenderer(new MyRenderer());
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(ScanQrCodeFromVisionActivity.this).build();
        final CameraSource cameraSource = new CameraSource.Builder(ScanQrCodeFromVisionActivity.this, barcodeDetector).setFacing(CameraSource.CAMERA_FACING_BACK)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(1600, 1024).build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {

                try {
                    if (ActivityCompat.checkSelfPermission(ScanQrCodeFromVisionActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling


                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    } else {
                        cameraSource.start(surfaceView.getHolder());
                        cameraSource.start(glSurfaceView.getHolder());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() > 0) {
                    Intent intent = new Intent();
                    intent.putExtra("scanned_info", barcodes.valueAt(0));
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });

    }

    private boolean checkAndRequestPermissions() {
        int permissionCamera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }

        return true;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_ID_MULTIPLE_PERMISSIONS);
        // ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                finish();
                Intent intent = new Intent(ScanQrCodeFromVisionActivity.this,ScanQrCodeFromVisionActivity.class);
                startActivity(intent);
            }
        }
    }
    class MyRenderer implements GLSurfaceView.Renderer
    {
        public void onSurfaceCreated(GL10 unsued, EGLConfig config)
        {
            GLES20.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        }

        public void onDrawFrame(GL10 unused)
        {

        }

        public void onSurfaceChanged(GL10 unused, int width, int height)
        {
            GLES20.glViewport(0, 0, width, height);
        }
    }
}
