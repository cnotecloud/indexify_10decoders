package com.indexify.indexify;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.indexify.indexify.api.LoginApi;
import com.indexify.indexify.api.UserApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.ResponsePojo;
import com.indexify.indexify.pojo.TokenPojo;
import com.indexify.indexify.pojo.UserInfoPojo;
import com.indexify.indexify.pojojson.LoginPojoJson;
import com.white.easysp.EasySP;

import java.lang.reflect.Field;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    EditText txtPassword;
    Button btnLogin;
    TextView tvForgotPassword;
    RelativeLayout rlMain;
    Typeface playBold;
    Typeface playRegular;
    TextView textView;
    TextView tvResendOtp;
    TextInputLayout textInputLayout;
    TextInputLayout textInputlayoutOtp;
    EditText txtOtp;

    String code = "";

    boolean isLoginWithOtp = false;

    String accessToken = "";
    LinearLayout llDetectOtp;
    String userName = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        getControls();

    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                llDetectOtp.setVisibility(View.GONE);
                String message = intent.getStringExtra("message");
                message = message.toLowerCase().trim();
                if ((message.contains("indexify")) && (message.contains("otp"))) {
                    code = parseCode(message);
                    txtOtp.setText(code);
                    txtOtp.setSelection(code.length());
                    if (code.length() > 0) {
                        if (ConstantsMethods.isConnected(LoginActivity.this)) {
                            ConstantsMethods.showProgessDialog(LoginActivity.this, "Authenticating...");
                            checkLogin(accessToken, code);
                        }
                    }
                }


            }
        }
    };

    private void getControls() {
        userName = getIntent().getStringExtra("userName");
        accessToken = getIntent().getStringExtra("accessToken");
        isLoginWithOtp = getIntent().getBooleanExtra("isLoginWithOtp", false);
        boolean isDigitOnly = getIntent().getBooleanExtra("isDigitOnly", false);

        //for font
        playBold = Typeface.createFromAsset(LoginActivity.this.getAssets(),
                "fonts/play_bold.ttf");

        playRegular = Typeface.createFromAsset(LoginActivity.this.getAssets(),
                "fonts/play_regular.ttf");


        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtOtp = (EditText) findViewById(R.id.txtOtp);


        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setTypeface(playRegular);

        rlMain = (RelativeLayout) findViewById(R.id.rlMain);
        textView = (TextView) findViewById(R.id.textView);
        llDetectOtp = (LinearLayout) findViewById(R.id.llDetectOtp);

        tvResendOtp = (TextView) findViewById(R.id.tvResendOtp);
        tvResendOtp.setOnClickListener(LoginActivity.this);
        tvResendOtp.setTypeface(playRegular);

        textView.setTypeface(playBold);


        btnLogin.setTypeface(playBold);
        btnLogin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);


        //set font to edittext Hint
        textInputLayout = (TextInputLayout) findViewById(R.id.textInputLayout);
        textInputLayout.setTypeface(playRegular);
        textInputlayoutOtp = (TextInputLayout) findViewById(R.id.textInputlayoutOtp);
        textInputlayoutOtp.setTypeface(playRegular);


        if (isLoginWithOtp == true) {
            llDetectOtp.setVisibility(View.VISIBLE);
            LocalBroadcastManager.getInstance(LoginActivity.this).registerReceiver(receiver, new IntentFilter("otp"));

            textInputLayout.setVisibility(View.GONE);
            textInputlayoutOtp.setVisibility(View.VISIBLE);
            tvForgotPassword.setVisibility(View.GONE);
            tvResendOtp.setVisibility(View.VISIBLE);

        }

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnLogin) {
            llDetectOtp.setVisibility(View.GONE);
            String password = "";

            if (isLoginWithOtp == true) {
                password = txtOtp.getText().toString().trim();
            } else {
                password = txtPassword.getText().toString().trim();
            }


            if (password.length() == 0) {
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("Empty Data");
                sweetAlertDialog.setContentText("Please Enter Password/ OTP");
                sweetAlertDialog.show();
            } else {
                // check login from server
                if (ConstantsMethods.isConnected(LoginActivity.this)) {
                    ConstantsMethods.showProgessDialog(LoginActivity.this, "Authentication...");
                    checkLogin(accessToken, password);
                }
            }

        } else if (v.getId() == R.id.tvForgotPassword) {

            if (ConstantsMethods.isConnected(LoginActivity.this)) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        } else if (v.getId() == R.id.tvResendOtp) {
            if (ConstantsMethods.isConnected(LoginActivity.this)) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("username", userName);
                LoginApi loginApi = retrofit.create(LoginApi.class);
                Single<TokenPojo> responseAccessToken = loginApi.getStep1TokenWithTagOtp(jsonObject);
                responseAccessToken.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<TokenPojo>() {
                    @Override
                    public void onSuccess(TokenPojo tokenPojo) {
                        if (tokenPojo != null) {
                            if (tokenPojo.getAccessToken() != null) {
                                LocalBroadcastManager.getInstance(LoginActivity.this).registerReceiver(receiver, new IntentFilter("otp"));
                                Toast.makeText(LoginActivity.this, "An OTP has been sent to your Registered Phone or Email", Toast.LENGTH_SHORT).show();
                                accessToken = tokenPojo.getAccessToken();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            HttpException ex = (HttpException) e;
                            String er = ex.response().errorBody().string();
                            ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                            if (responsePojo != null) {
                                if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.INVALID_USERNAME_PASSWORD) {
                                    showError("Unauthorized", "Please check email and password.");
                                } else if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.EMAIL_NOT_VERIFIED) {
                                    showError("User not verified", "Please verify through email and try again.");
                                } else if (responsePojo.getStatus() == 401 && responsePojo.getResponseCode() == ConstantsErrorCodes.AUTHENTICATION_FAILED) {
                                    showError("Authentication Failed", "Please check your email/phone number and try again.");
                                } else if (responsePojo.getStatus() == 403) {
                                    showError("Not access", "Please try again or contact admin.");
                                } else if (responsePojo.getStatus() == 500) {
                                    showError("Internal server error", "Something went wrong please try later.");
                                }
                            }
                        } catch (Exception e1) {
                            if (e instanceof UnknownHostException || e instanceof SocketTimeoutException) {
                                showError("No Internet", "Please Check Your Internet Connection");
                            }
                            showError("Error", "Something went wrong please try later.");
                        } finally {
                            ConstantsMethods.cancleProgessDialog();
                        }
                    }
            });
        }
    }

}


    private void checkLogin(final String accessToken, final String password) {
        Retrofit retrofit;
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Constants.BASE_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        retrofit = builder.build();

        LoginApi api = retrofit.create(LoginApi.class);
//        LoginPojoJson loginPojoJson = new LoginPojoJson(accessToken, password);
//        JsonObject jsonLoginObject = new Gson().toJsonTree(loginPojoJson).getAsJsonObject();
        JsonObject jsonLoginObject = new JsonObject();
        Single<TokenPojo> responseSingle;
        if (isLoginWithOtp == true) {
            jsonLoginObject.addProperty("otp", password);
            responseSingle = api.getLoginWithOtp("Bearer " + accessToken, jsonLoginObject);
        } else {
            jsonLoginObject.addProperty("password", password);
            responseSingle = api.getLoginWithPassword("Bearer " + accessToken, jsonLoginObject);
        }


        responseSingle.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<TokenPojo>() {
                    @Override
                    public void onSuccess(@NonNull TokenPojo token) {
                        EasySP.init(LoginActivity.this).putString(ConstantsEasySP.SP_TOKEN, "Bearer " + token.getToken());
                        EasySP.init(LoginActivity.this).putString(ConstantsEasySP.SP_REFRESH_TOKEN, "Bearer " + token.getRefreshToken());
                        //EasySP.init(LoginActivity.this).putBoolean("isLogin", true);

                        if (ConstantsMethods.isConnectedWithoutMessage(LoginActivity.this)) {
                            getUserInformation();
                        } else {
                            EasySP.init(LoginActivity.this).putBoolean(ConstantsEasySP.SP_IS_LOGIN, false);
                            EasySP.init(LoginActivity.this).remove(ConstantsEasySP.SP_TOKEN);
                            EasySP.init(LoginActivity.this).remove(ConstantsEasySP.SP_REFRESH_TOKEN);

                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText("No Internet");
                            sweetAlertDialog.setContentText("Please check internet connection.");
                            sweetAlertDialog.show();
                            sweetAlertDialog.setCancelable(false);
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    restartApp();
                                }
                            });
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        try {
                            HttpException ex = (HttpException) e;
                            String er = ex.response().errorBody().string();
                            ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                            if (responsePojo != null) {
                                if (responsePojo.getStatus() == 401) {
                                    if (responsePojo.getResponseCode() == ConstantsErrorCodes.INVALID_USERNAME_PASSWORD) {
                                        showError("Unauthorized", "Please check email and password.");
                                    } else if (responsePojo.getResponseCode() == ConstantsErrorCodes.EMAIL_NOT_VERIFIED) {
                                        showError("User not verified", "Please verify through email and try again.");
                                    } else if (responsePojo.getResponseCode() == ConstantsErrorCodes.TOKEN_EXPIRED) {
                                        showErrorwithFinish("Session Expired!", "Please try again");
                                    }else{
                                        showError("Unauthorized", "Please check email and password.");
                                    }
                                } else if (responsePojo.getStatus() == 403) {
                                    showError("Not access", "Please try again or contact admin.");
                                } else if (responsePojo.getStatus() == 500) {
                                    showError("Internal server error", "Something went wrong please try later.");
                                }
                            }
                        } catch (Exception e1) {
                            showError("Error", "Something went wrong please try later.");
                        } finally {
                            EasySP.init(LoginActivity.this).putBoolean("isLogin", false);
                            EasySP.init(LoginActivity.this).remove(ConstantsEasySP.SP_TOKEN);
                            EasySP.init(LoginActivity.this).remove(ConstantsEasySP.SP_REFRESH_TOKEN);
                            ConstantsMethods.cancleProgessDialog();
                        }

                        //ConstantsMethods.showServerError(e, LoginActivity.this);
                    }
                });
    }

    private void getUserInformation() {
        // get organization details
        final UserApi organizationApi = retrofit.create(UserApi.class);
        Single<UserInfoPojo> orgSingleObservable = organizationApi.getOrganizationInformation();
        orgSingleObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<UserInfoPojo>() {
                    @Override
                    public void onSuccess(@NonNull final UserInfoPojo userInfoPojo) {
                        ConstantsMethods.cancleProgessDialog();

                        if (userInfoPojo != null) {
                            String userInfoJson = new Gson().toJson(userInfoPojo);
                            EasySP.init(LoginActivity.this).put(ConstantsEasySP.SP_USER_INFO, userInfoJson);
                            EasySP.init(LoginActivity.this).put(ConstantsEasySP.SP_USER_ID, userInfoPojo.getId());
                            if (userInfoPojo.getOrganizationPojo() != null) {
                                EasySP.init(LoginActivity.this).put(ConstantsEasySP.SP_USER_ORG_ID, userInfoPojo.getOrganizationPojo().getId());
                            }
                            EasySP.init(LoginActivity.this).putBoolean(ConstantsEasySP.SP_IS_LOGIN, true);


                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE).setTitleText("Congratulations.").setContentText("Login Successfully").setConfirmText("   OK   ");
                            sweetAlertDialog.show();
                            sweetAlertDialog.setCancelable(false);
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    //                                   checkAndRequestPermissions();
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            });

                            // show home screen


                           /* } else {

                                //show qr scan code
                               *//* Intent intent = new Intent(LoginActivity.this, DataDownloadActivity.class);
                                startActivity(intent);
                                finish();*//*
                            }*/
                        } else {
                            EasySP.init(LoginActivity.this).putBoolean(ConstantsEasySP.SP_IS_LOGIN, false);
                            EasySP.init(LoginActivity.this).putString(ConstantsEasySP.SP_TOKEN, "");
                            EasySP.init(LoginActivity.this).putString(ConstantsEasySP.SP_USER_INFO, "");
                            EasySP.init(LoginActivity.this).putString(ConstantsEasySP.SP_REFRESH_TOKEN, "");
                            ConstantsMethods.cancleProgessDialog();

                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText("Invalid Credentials");
                            sweetAlertDialog.setContentText("Authentication fail.");
                            sweetAlertDialog.show();
                            sweetAlertDialog.setCancelable(false);
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    finish();
                                }
                            });
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ConstantsMethods.showServerError(e, LoginActivity.this);
                        EasySP.init(LoginActivity.this).putBoolean(ConstantsEasySP.SP_IS_LOGIN, false);
                        EasySP.init(LoginActivity.this).putString(ConstantsEasySP.SP_TOKEN, "");
                        EasySP.init(LoginActivity.this).putString(ConstantsEasySP.SP_REFRESH_TOKEN, "");
                        ConstantsMethods.cancleProgessDialog();

                    }
                });
    }

    private void restartApp() {
       /* Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);*/

        finishAffinity();
        Intent intent = new Intent(LoginActivity.this, IntroActivity.class);
        startActivity(intent);
    }


    private void showError(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });

    }

    private void showErrorwithFinish(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        });

    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }


    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


}
