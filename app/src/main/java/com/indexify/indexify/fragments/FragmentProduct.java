package com.indexify.indexify.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.indexify.indexify.OrganizationDataActivity;
import com.indexify.indexify.PdfListActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.adapter.ProductAdapter;
import com.indexify.indexify.api.ProductApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsBreadScrumb;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.pojo.BreadScrumbPojo;
import com.indexify.indexify.pojo.ProductsGetPojo;
import com.white.easysp.EasySP;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

/**
 * Created by amar on 29-12-2017.
 */

public class FragmentProduct extends Fragment {
    Context context;
    RecyclerView rvProduct;
    LinearLayoutManager mLayoutManager;
    long orgId;
    String nodeName = "";

    int i = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_products, container, false);
        geControls(v);
        return v;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((OrganizationDataActivity) context).removeBreadScrumb(String.valueOf(i));
    }

    private void geControls(View v) {
        rvProduct = (RecyclerView) v.findViewById(R.id.rvProduct);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rvProduct.setHasFixedSize(true);
        rvProduct.setLayoutManager(mLayoutManager);
        nodeName = getArguments().getString("nodeName");
        orgId = getArguments().getLong("orgId");

        if (nodeName != null && nodeName.length() > 0) {
            i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
            setBreadScrumbData(nodeName, String.valueOf(i));
            //setBreadScrumbData(nodeName, ConstantsBreadScrumb.TAG_B_NODE);
            //
        }

        // if (getArguments().getBoolean("found")) {
        getData();
        // }

    }


    private void getData() {
        String type = "";
        type = getArguments().getString("type");
        if (type.equalsIgnoreCase("normal")) {
            String nodeId = getArguments().getString("nodeId");
            // get data from server
            if (ConstantsMethods.isConnected(context)) {
                ConstantsMethods.showProgessDialog(context, "Loading Product's...");

                ProductApi productApi = retrofit.create(ProductApi.class);
                Observable<ProductsGetPojo> observable = productApi.getProductsByOrgIdAndLeafId(orgId, nodeId, ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_PRODUCT_GET_SIZE, Constants.DEFAULT_PAGE);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<ProductsGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(ProductsGetPojo productsGetPojo) {
                                ConstantsMethods.cancleProgessDialog();
                                if (productsGetPojo.getEmbedded().getProducts().size() > 0) {
                                    ProductAdapter adapter = new ProductAdapter(productsGetPojo.getEmbedded().getProducts(), context,orgId);
                                    rvProduct.setAdapter(adapter);
                                } else if (productsGetPojo.getEmbedded().getProducts().size() == 0) {
                                    Set<String> stringSet = EasySP.init(context).getStringSet(ConstantsEasySP.SP_CATALOGUE_LIST);
                                    List<String> list = new ArrayList<>(stringSet);

                                    if (list.size() > 0){
                                        getActivity().getSupportFragmentManager().popBackStack();
                                        Intent intent = new Intent(context, PdfListActivity.class);
                                        intent.putExtra("pdfList", (Serializable) list);
                                        startActivity(intent);
                                    }else{
                                        showMessage("Empty", "No Products Found", SweetAlertDialog.ERROR_TYPE);
                                    }


                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                                showMessage("Unknown Error","Please contact Admin",SweetAlertDialog.ERROR_TYPE);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            }
        } else if (type.equalsIgnoreCase("search")) {
            if (ConstantsMethods.isConnected(context)) {
                ConstantsMethods.showProgessDialog(context, "Searching Product's...");

                ProductApi productApi = retrofit.create(ProductApi.class);
                Observable<ProductsGetPojo> observable = productApi.searchProductsByContainingNameAndOrgId(orgId, nodeName, ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_PRODUCT_GET_SIZE, Constants.DEFAULT_PAGE);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<ProductsGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(ProductsGetPojo productsGetPojo) {
                                ConstantsMethods.cancleProgessDialog();
                                if (productsGetPojo.getEmbedded().getProducts().size() > 0) {
                                    ProductAdapter adapter = new ProductAdapter(productsGetPojo.getEmbedded().getProducts(), context,orgId);
                                    rvProduct.setAdapter(adapter);
                                } else if (productsGetPojo.getEmbedded().getProducts().size() == 0) {
                                    showMessage("Empty", "No Products Found", SweetAlertDialog.ERROR_TYPE);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                                showMessage("Unknown Error","Please contact Admin",SweetAlertDialog.ERROR_TYPE);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            //do when hidden
            Toast.makeText(context, "hide", Toast.LENGTH_SHORT).show();
        } else {
            //do when show
            Toast.makeText(context, "show", Toast.LENGTH_SHORT).show();
        }
    }


    private void setBreadScrumbData(String name, String tag) {
        BreadScrumbPojo breadScrumbPojo = new BreadScrumbPojo(name, tag);
        ((OrganizationDataActivity) context).setBreadScrumb(breadScrumbPojo);
    }

    private void showMessage(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

}
