package com.indexify.indexify.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.indexify.indexify.FavouriteListActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.adapter.FavouriteOrganizationAdapter;
import com.indexify.indexify.adapter.FavouriteProductViewerAdapter;
import com.indexify.indexify.api.FavouriteApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.pojo.FavouriteOrganizationGetPojo;
import com.indexify.indexify.pojo.MyProductFavouriteViewerGetPojo;
import com.indexify.indexify.pojo.MyProductFavouriteViewerPojo;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

/**
 * Created by cnote on 03-05-2018.
 */

public class FragmentMyProductFavourite extends Fragment {
    RecyclerView rvFavouriteMyProductViewer;
    Context context;
    LinearLayoutManager mLayoutManager;
    ProgressBar progressbar;
    List<MyProductFavouriteViewerPojo> alMyProductFavouritePojo = new ArrayList<>();
    FavouriteProductViewerAdapter adapter = null;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 5;
    int page = 0;
    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_favourite_myproduct_list, container, false);
        getControls(v);
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void getControls(View v) {

        rvFavouriteMyProductViewer = (RecyclerView) v.findViewById(R.id.rvFavouriteMyProductViewer);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rvFavouriteMyProductViewer.setHasFixedSize(true);
        rvFavouriteMyProductViewer.setLayoutManager(mLayoutManager);
        progressbar = (ProgressBar) v.findViewById(R.id.progressbar);
        progressbar.setVisibility(View.INVISIBLE);

        resetValues();
        rvFavouriteMyProductViewer.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = rvFavouriteMyProductViewer.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (ConstantsMethods.isConnected(context)) {
                                loadMoreData();
                            }
                        }
                    });
                    // Do something
                    loading = true;
                }
            }
        });

//        if (ConstantsMethods.isConnected(context)) {
//            getData();
//        }
    }

    public void getData() {
        //get my scan from server
        if (ConstantsMethods.isConnected(context)) {
            try {
                ConstantsMethods.showProgessDialogWithCancelable(context, "Loading Favourites By Others List...");

                FavouriteApi favouriteApi = retrofit.create(FavouriteApi.class);
                Observable<MyProductFavouriteViewerGetPojo> observable = favouriteApi.getMyProductFavouriteList(ConstantsProjection.PROJECTION_DETAIL,Constants.DEFAULT_USER_SCAN_LOADING_SIZE, page);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<MyProductFavouriteViewerGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(MyProductFavouriteViewerGetPojo myProductFavouriteViewerGetPojo) {
                                ConstantsMethods.cancleProgessDialog();
                                if (myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite()!= null && myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite().size() > 0) {
                                    alMyProductFavouritePojo = new ArrayList<MyProductFavouriteViewerPojo>();
                                    alMyProductFavouritePojo.addAll(myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite());
                                    adapter = new FavouriteProductViewerAdapter(myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite(), getActivity());
                                    rvFavouriteMyProductViewer.setAdapter(adapter);
                                    //getSupportActionBar().setTitle("(" + alOrganizaitons.size() + ")  Favourites ");
                                } else if (myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite() != null && myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite().size() == 0) {
                                    // show not found message
                                    showError("Not found", "Favourite organization data not found", 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                                try {
                                    HttpException error = (HttpException) e;
                                    if (error != null) {
                                        showError("Not found", "Favourite organization data not found", 1);
                                    }

                                } catch (Exception e1) {
                                    showError("Not found", "Favourite organization data not found", 1);
                                }

                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {

            } finally {

            }
        }
    }
    private void loadMoreData() {
        if (ConstantsMethods.isConnectedWithoutMessage(context)) {
            progressbar.setVisibility(View.VISIBLE);
            try {
                FavouriteApi favouriteApi = retrofit.create(FavouriteApi.class);
                Observable<MyProductFavouriteViewerGetPojo> observable = favouriteApi.getMyProductFavouriteList(ConstantsProjection.PROJECTION_DETAIL,Constants.DEFAULT_USER_SCAN_LOADING_SIZE, ++page);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<MyProductFavouriteViewerGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(MyProductFavouriteViewerGetPojo myProductFavouriteViewerGetPojo) {
                                progressbar.setVisibility(View.INVISIBLE);
                                if (myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite() != null && myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite().size() > 0) {
                                    alMyProductFavouritePojo.addAll(myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite());
                                    adapter.notifyDataSetChanged();
                                   // getSupportActionBar().setTitle("(" + alOrganizations.size() + ")  Favourites ");
                                } else if (myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite() != null && myProductFavouriteViewerGetPojo.getEmbedded().getAlMyFavourite().size() == 0) {
                                    // show not found message
                                    progressbar.setVisibility(View.INVISIBLE);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                progressbar.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {
                progressbar.setVisibility(View.INVISIBLE);
            } finally {

            }

        }
    }
    private void showError(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
//                ((FavouriteListActivity)context).finishOrgDataAndUserScanActivity();
            }
        });

    }
    public void resetValues() {
        alMyProductFavouritePojo.clear();
        page = 0;
        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        loading = true;
        previousTotal = 0;
        visibleThreshold = 5;
       // adapter = new FavouriteOrganizationAdapter(alMyProductFavouritePojo,getActivity());
        rvFavouriteMyProductViewer.setAdapter(null);
    }
//    public void startFavouriteListActivity(Intent intent){
//        Activity activity = getActivity();
//        if(activity != null && isAdded()){
//            activity.startActivityForResult(intent,901);
//        }
//
//    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 901 && resultCode == 900){
//            getControls(v);
//        }
//    }
}
