package com.indexify.indexify.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indexify.indexify.R;


/**
 * Created by amar on 08-01-2018.
 */

public class FragmentIntro3 extends Fragment {
    Context context;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_intro_3, container, false);
        TextView tv = (TextView) v.findViewById(R.id.tvIntro3);
        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "fonts/play_bold.ttf");
        tv.setTypeface(face);
        return v;
    }
}
