package com.indexify.indexify.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.indexify.indexify.ProductViewActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.VisualetActivity;
import com.indexify.indexify.api.FavouriteApi;
import com.indexify.indexify.constants.ConstantsErrorCodes;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.interfaces.Communicator;
import com.indexify.indexify.pojo.ProductPojo;
import com.indexify.indexify.pojo.ResponsePojo;
import com.indexify.indexify.pojojson.FavouriteProductPostjsonPojo;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.apache.commons.lang3.text.WordUtils;

import java.io.Serializable;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;


/**
 * Created by amar on 11-08-2016.
 */
public class MyProductViewFragment extends Fragment implements View.OnClickListener {
    ImageButton cancelButton;
    ImageView imgProduct;
    TextView tvProductName;
    ViewPager pager;
    ViewPager pagerImages;
    PagerAdapter pagerImagesAdapter;
    DotsIndicator dotsIndicator;
    ProductPojo productPojo;
    long orgId = 0;
    RelativeLayout rlSlideClickLayout;
    LinearLayout llSlideClickLayout;
    LinearLayout llData;
    boolean isFavourite = false;
    String favouriteId = "";
    int NUM_PAGES = 0;





    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        pager = (ViewPager) container;
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_my_product_list_slide, container, false);
        productPojo = (ProductPojo) getArguments().getSerializable("productPojo");
        orgId = getArguments().getLong("orgId");

        //decler all components..
        cancelButton = (ImageButton) rootView.findViewById(R.id.cancelButton);

        cancelButton.setOnClickListener(this);
        imgProduct = (ImageView) rootView.findViewById(R.id.imgProduct);
        tvProductName = (TextView) rootView.findViewById(R.id.tvProductName);

        //by sai

        if ((productPojo.getImgUrls() != null) && (productPojo.getImgUrls().size() > 0)) {
            NUM_PAGES = productPojo.getImgUrls().size();
        }

        pagerImages = (ViewPager) rootView.findViewById(R.id.pagerImages);
        pagerImagesAdapter = new ImagesSlidePagerAdapter(inflater);
        pagerImages.setAdapter(pagerImagesAdapter);
        dotsIndicator = (DotsIndicator) rootView.findViewById(R.id.dots_indicator);
        dotsIndicator.setViewPager(pagerImages);
        tvProductName.setText(productPojo.getName());
        rlSlideClickLayout = (RelativeLayout) rootView.findViewById(R.id.rlSlideClickLayout);
        llSlideClickLayout = (LinearLayout) rootView.findViewById(R.id.llSlideClickLayout);
        llSlideClickLayout.setOnClickListener(this);
        llData = (LinearLayout) rootView.findViewById(R.id.llData);
        llData.setOnClickListener(this);
        rlSlideClickLayout.setOnClickListener(this);

        getData(inflater, container);

        return rootView;
    }

    private void getData(LayoutInflater inflater, final ViewGroup container) {

        if ((productPojo.getControls() != null) && (productPojo.getControls().size() > 0)) {
            for (int i = 0; i < productPojo.getControls().size(); i++) {

                ViewGroup controlView = (ViewGroup) inflater.inflate(R.layout.layout_controls, container, false);
                TextView tvTitle = controlView.findViewById(R.id.tvTitle);
                TextView tvValue = controlView.findViewById(R.id.tvValue);
                tvTitle.setText(WordUtils.capitalize(productPojo.getControls().get(i).getName()));
                boolean isUrl = URLUtil.isValidUrl(productPojo.getControls().get(i).getValue().toString().trim());
                if (isUrl) {
                    tvValue.setText(Html.fromHtml("<u>Click Here</u>"));
                    tvValue.setTextColor(ContextCompat.getColor(getContext(),R.color.link));
                    int finalI = i;
                    tvValue.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(productPojo.getControls().get(finalI).getValue().toString()));
                                getContext().startActivity(intent);
                            }catch(Exception error){
                                Toast.makeText(getContext(), "Unable To Open Link", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    tvValue.setText(String.valueOf(productPojo.getControls().get(i).getValue()));
                }
                llData.addView(controlView);
            }
        }
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.cancelButton:
                getActivity().finish();
                break;


        }
    }






    // by sai

    public class ImagesSlidePagerAdapter extends PagerAdapter {
        LayoutInflater mLayoutInflater;

        public ImagesSlidePagerAdapter(LayoutInflater mLayoutInflater) {

            this.mLayoutInflater = mLayoutInflater;

            // mLayoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        }


        @Override
        public int getCount() {
            if ((productPojo.getLocalImgUrls() != null) && (productPojo.getLocalImgUrls().size() > 0)){
                return productPojo.getLocalImgUrls().size();
            }else {
                return NUM_PAGES+1;
            }

        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.image_viewpager, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.product_image);


            if (NUM_PAGES != 0) {
                if ((productPojo.getLocalImgUrls() != null) && (productPojo.getLocalImgUrls().size() > 0)) {

                    if ((productPojo.getLocalImgUrls().get(position) != null) && (productPojo.getLocalImgUrls().get(position).length() > 0)) {
                        Glide.with(getActivity()).load(productPojo.getLocalImgUrls().get(position)).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);
                    } else {
                        Glide.with(getActivity()).load(R.drawable.default_product_image).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);
                    }

                } else {
                    Glide.with(getActivity()).load(R.drawable.default_product_image).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);
                }
            }else{
                Glide.with(getActivity()).load(R.drawable.default_product_image).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);
            }


            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ((productPojo.getLocalImgUrls() != null) && (productPojo.getLocalImgUrls().size() > 0)) {
                            Intent intent = new Intent(getContext(), VisualetActivity.class);
                            //intent.putExtra("productPojo", productPojo);
                            intent.putExtra("imgUrls", (Serializable) productPojo.getLocalImgUrls());
                            intent.putExtra("imagePosition", position);
                            startActivity(intent);

                    } else {
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                        sweetAlertDialog.setTitleText("No Image");
                        sweetAlertDialog.setContentText("No Image Found !");
                        sweetAlertDialog.show();
                    }

                }
            });

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}




