package com.indexify.indexify.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indexify.indexify.R;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.pojo.UserInfoPojo;

import org.apache.commons.lang3.text.WordUtils;


/**
 * Created by amar on 22-06-2017.
 */

public class FragmentOrganizationInfo extends Fragment {
    Context context;
    TextView tvOrgName;
    TextView tvDescription;
    TextView tvWebsite;
    TextView tvContactNumber;
    TextView tvEmailId;
    TextView tvCountry;
    TextView tvRegion;
    TextView tvCity;
    TextView tvAddress;
    TextView tvPostalCode;


    public FragmentOrganizationInfo() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_organization_info, container, false);

        getControls(v);

        return v;
    }

    private void getControls(View v) {
        tvOrgName = (TextView) v.findViewById(R.id.tvOrgName);
        tvDescription = (TextView) v.findViewById(R.id.tvDescription);
        tvWebsite = (TextView) v.findViewById(R.id.tvWebsite);
        tvContactNumber = (TextView) v.findViewById(R.id.tvContactNumber);
        tvEmailId = (TextView) v.findViewById(R.id.tvEmailId);
        tvCountry = (TextView) v.findViewById(R.id.tvCountry);
        tvRegion = (TextView) v.findViewById(R.id.tvRegion);
        tvCity = (TextView) v.findViewById(R.id.tvCity);
        tvAddress = (TextView) v.findViewById(R.id.tvAddress);
        tvPostalCode = (TextView) v.findViewById(R.id.tvPostalCode);
        getData();
    }

    private void getData() {
        String userInfoPojoJson = getArguments().getString("userinfo");

        if ((userInfoPojoJson != null) && (userInfoPojoJson.length() > 0)) {
            UserInfoPojo userInfoPojo = new Gson().fromJson(userInfoPojoJson, UserInfoPojo.class);

            if (userInfoPojo != null) {
                if (userInfoPojo.getOrganizationPojo() != null) {
                    if ((userInfoPojo.getOrganizationPojo().getDisplayName() != null) && (userInfoPojo.getOrganizationPojo().getDisplayName().length() > 0)) {
                        tvOrgName.setText(WordUtils.capitalize(userInfoPojo.getOrganizationPojo().getDisplayName()));
                    } else {
                        tvOrgName.setText(Constants.IF_EMPTY);
                    }
                    if ((userInfoPojo.getOrganizationPojo().getDescription() != null) && (userInfoPojo.getOrganizationPojo().getDescription().length() > 0)) {
                        tvDescription.setText(WordUtils.capitalize(userInfoPojo.getOrganizationPojo().getDescription()));
                    } else {
                        tvDescription.setText(Constants.IF_EMPTY);
                    }
                    if ((userInfoPojo.getOrganizationPojo().getWebsite() != null) && (userInfoPojo.getOrganizationPojo().getWebsite().length() > 0)) {
                        tvWebsite.setText(userInfoPojo.getOrganizationPojo().getWebsite());
                    } else {
                        tvWebsite.setText(Constants.IF_EMPTY);
                    }

                    if ((userInfoPojo.getOrganizationPojo().getEmail() != null) && (userInfoPojo.getOrganizationPojo().getEmail().length() > 0)) {
                        tvEmailId.setText(userInfoPojo.getOrganizationPojo().getEmail());
                    } else {
                        tvEmailId.setText(Constants.IF_EMPTY);
                    }
                    if ((userInfoPojo.getOrganizationPojo().getContact() != null) && (userInfoPojo.getOrganizationPojo().getContact().length() > 0)) {
                        tvContactNumber.setText(userInfoPojo.getOrganizationPojo().getContact());
                    } else {
                        tvContactNumber.setText(Constants.IF_EMPTY);
                    }

                    if (userInfoPojo.getOrganizationPojo().getLocationPojo() != null) {
                        if ((userInfoPojo.getOrganizationPojo().getLocationPojo().getAddress() != null) && (userInfoPojo.getOrganizationPojo().getLocationPojo().getAddress().length() > 0)) {
                            tvAddress.setText(WordUtils.capitalize(userInfoPojo.getOrganizationPojo().getLocationPojo().getAddress()));
                        } else {
                            tvAddress.setText(Constants.IF_EMPTY);
                        }
                        if ((userInfoPojo.getOrganizationPojo().getLocationPojo().getCountry() != null) && (userInfoPojo.getOrganizationPojo().getLocationPojo().getCountry().length() > 0)) {
                            tvCountry.setText(WordUtils.capitalize(userInfoPojo.getOrganizationPojo().getLocationPojo().getCountry()));
                        } else {
                            tvCountry.setText(Constants.IF_EMPTY);
                        }
                        if ((userInfoPojo.getOrganizationPojo().getLocationPojo().getRegion() != null) && (userInfoPojo.getOrganizationPojo().getLocationPojo().getRegion().length() > 0)) {
                            tvRegion.setText(WordUtils.capitalize(userInfoPojo.getOrganizationPojo().getLocationPojo().getRegion()));
                        } else {
                            tvRegion.setText(Constants.IF_EMPTY);
                        }
                        if ((userInfoPojo.getOrganizationPojo().getLocationPojo().getCity() != null) && (userInfoPojo.getOrganizationPojo().getLocationPojo().getCity().length() > 0)) {
                            tvCity.setText(WordUtils.capitalize(userInfoPojo.getOrganizationPojo().getLocationPojo().getCity()));
                        } else {
                            tvCity.setText(Constants.IF_EMPTY);
                        }
                        if ((userInfoPojo.getOrganizationPojo().getLocationPojo().getPostalCode() != null) && (userInfoPojo.getOrganizationPojo().getLocationPojo().getPostalCode().length() > 0)) {
                            tvPostalCode.setText(userInfoPojo.getOrganizationPojo().getLocationPojo().getPostalCode());
                        } else {
                            tvPostalCode.setText(Constants.IF_EMPTY);
                        }
                    } else {
                        tvAddress.setText(Constants.IF_EMPTY);
                        tvCountry.setText(Constants.IF_EMPTY);
                        tvRegion.setText(Constants.IF_EMPTY);
                        tvCity.setText(Constants.IF_EMPTY);
                        tvPostalCode.setText(Constants.IF_EMPTY);
                    }

                } else {
                    tvOrgName.setText(Constants.IF_EMPTY);
                    tvDescription.setText(Constants.IF_EMPTY);
                    tvWebsite.setText(Constants.IF_EMPTY);
                    tvEmailId.setText(Constants.IF_EMPTY);
                    tvContactNumber.setText(Constants.IF_EMPTY);
                    tvAddress.setText(Constants.IF_EMPTY);
                    tvCountry.setText(Constants.IF_EMPTY);
                    tvRegion.setText(Constants.IF_EMPTY);
                    tvCity.setText(Constants.IF_EMPTY);
                    tvPostalCode.setText(Constants.IF_EMPTY);
                }
            }

        }

    }
}
