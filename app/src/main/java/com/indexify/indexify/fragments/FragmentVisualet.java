package com.indexify.indexify.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.indexify.indexify.R;


/**
 * Created by amar on 25-01-2018.
 */

public class FragmentVisualet extends Fragment {
    PhotoView photoView;
    PhotoViewAttacher attacher;
    Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_photoview, container, false);
        geControls(v);
        return v;
    }

    private void geControls(View v) {
        photoView = v.findViewById(R.id.imgView);
        String localImgUrl = getArguments().getString("localImgUrl");
        Glide.with(context).load(localImgUrl).diskCacheStrategy(DiskCacheStrategy.NONE).into(photoView);
        attacher = new PhotoViewAttacher(photoView);

    }
}
