package com.indexify.indexify.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indexify.indexify.R;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.pojo.UserInfoPojo;

import org.apache.commons.lang3.text.WordUtils;


/**
 * Created by amar on 22-06-2017.
 */

public class FragmentUserInfo extends Fragment {
    Context context;
    TextView tvFirstName;
    TextView tvLastName;
    TextView tvContactNumber;
    TextView tvEmailId;
    TextView tvCountry;
    TextView tvRegion;
    TextView tvCity;
    TextView tvAddress;
    TextView tvPostalCode;


    public FragmentUserInfo() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_user_info, container, false);

        getControls(v);

        return v;
    }

    private void getControls(View v) {
        tvFirstName = (TextView) v.findViewById(R.id.tvFirstName);
        tvLastName = (TextView) v.findViewById(R.id.tvLastName);
        tvContactNumber = (TextView) v.findViewById(R.id.tvContactNumber);
        tvEmailId = (TextView) v.findViewById(R.id.tvEmailId);
        tvCountry = (TextView) v.findViewById(R.id.tvCountry);
        tvRegion = (TextView) v.findViewById(R.id.tvRegion);
        tvCity = (TextView) v.findViewById(R.id.tvCity);
        tvAddress = (TextView) v.findViewById(R.id.tvAddress);
        tvPostalCode = (TextView) v.findViewById(R.id.tvPostalCode);


        getData();
    }

    private void getData() {
        String userInfoPojoJson = getArguments().getString("userinfo");

        if ((userInfoPojoJson != null) && (userInfoPojoJson.length() > 0)) {
            UserInfoPojo userInfoPojo = new Gson().fromJson(userInfoPojoJson, UserInfoPojo.class);


            if ((userInfoPojo.getFirstName() != null) && (userInfoPojo.getFirstName().length() > 0)) {
                tvFirstName.setText(WordUtils.capitalize(userInfoPojo.getFirstName()));
            } else {
                tvFirstName.setText(Constants.IF_EMPTY);
            }
            if ((userInfoPojo.getLastName() != null) && (userInfoPojo.getLastName().length() > 0)) {
                tvLastName.setText(WordUtils.capitalize(userInfoPojo.getLastName()));
            } else {
                tvLastName.setText(Constants.IF_EMPTY);
            }
            if ((userInfoPojo.getEmail() != null) && (userInfoPojo.getEmail().length() > 0)) {
                tvEmailId.setText(userInfoPojo.getEmail());
            } else {
                tvEmailId.setText(Constants.IF_EMPTY);
            }
            if ((userInfoPojo.getContact() != null) && (userInfoPojo.getContact().length() > 0)) {
                tvContactNumber.setText(userInfoPojo.getContact());
            } else {
                tvContactNumber.setText(Constants.IF_EMPTY);
            }
            if (userInfoPojo.getLocationPojo() != null) {
                if ((userInfoPojo.getLocationPojo().getAddress() != null) && (userInfoPojo.getLocationPojo().getAddress().length() > 0)) {
                    tvAddress.setText(WordUtils.capitalize(userInfoPojo.getLocationPojo().getAddress()));
                } else {
                    tvAddress.setText(Constants.IF_EMPTY);
                }
                if ((userInfoPojo.getLocationPojo().getCountry() != null) && (userInfoPojo.getLocationPojo().getCountry().length() > 0)) {
                    tvCountry.setText(WordUtils.capitalize(userInfoPojo.getLocationPojo().getCountry()));
                } else {
                    tvCountry.setText(Constants.IF_EMPTY);
                }
                if ((userInfoPojo.getLocationPojo().getRegion() != null) && (userInfoPojo.getLocationPojo().getRegion().length() > 0)) {
                    tvRegion.setText(WordUtils.capitalize(userInfoPojo.getLocationPojo().getRegion()));
                } else {
                    tvRegion.setText(Constants.IF_EMPTY);
                }
                if ((userInfoPojo.getLocationPojo().getCity() != null) && (userInfoPojo.getLocationPojo().getCity().length() > 0)) {
                    tvCity.setText(WordUtils.capitalize(userInfoPojo.getLocationPojo().getCity()));
                } else {
                    tvCity.setText(Constants.IF_EMPTY);
                }
                if ((userInfoPojo.getLocationPojo().getPostalCode() != null) && (userInfoPojo.getLocationPojo().getPostalCode().length() > 0)) {
                    tvPostalCode.setText(userInfoPojo.getLocationPojo().getPostalCode());
                } else {
                    tvPostalCode.setText(Constants.IF_EMPTY);
                }
            } else {
                tvAddress.setText(Constants.IF_EMPTY);
                tvCountry.setText(Constants.IF_EMPTY);
                tvRegion.setText(Constants.IF_EMPTY);
                tvCity.setText(Constants.IF_EMPTY);
                tvPostalCode.setText(Constants.IF_EMPTY);
            }


//
//
//            tvFirstName.setText(WordUtils.capitalize(userInfoPojo.getFirstName()));
//            tvLastName.setText(WordUtils.capitalize(userInfoPojo.getLastName()));
//            tvUsername.setText(userInfoPojo.getUsername());
//
////            if (userInfoPojo.getRoles().contains(Constants.ROLE_ADMIN)) {
////                tvRole.setText("ADMIN");
////            } else {
////                tvRole.setText("USER");
////            }
//            tvContactNumber.setText(userInfoPojo.getContact());
//            tvEmailId.setText(userInfoPojo.getEmail());
//            tvCountry.setText(userInfoPojo.getLocation().getCountry().getName());
//            tvRegion.setText(userInfoPojo.getLocation().getRegion());
//            tvCity.setText(userInfoPojo.getLocation().getCity());
//            tvAddress.setText(userInfoPojo.getLocation().getAddress());
//
//            llChangePassword.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (ConstantsMethods.isConnected(context)) {
//                        Intent intent = new Intent(context, ChangePasswordActivity.class);
//                        intent.putExtra("ID", userInfoPojo.getId());
//                        intent.putStringArrayListExtra("ROLES", (ArrayList<String>) roles);
//                        context.startActivity(intent);
//                    }
//                }
//            });
//
//        }
        }
    }
}
