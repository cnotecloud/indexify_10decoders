package com.indexify.indexify.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.indexify.indexify.MyOrganizationDataActivity;
import com.indexify.indexify.OrganizationDataActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.adapter.MyProductAdapter;
import com.indexify.indexify.adapter.ProductAdapter;
import com.indexify.indexify.api.ProductApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsBreadScrumb;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.database.DatabaseAccess;
import com.indexify.indexify.pojo.BreadScrumbPojo;
import com.indexify.indexify.pojo.ProductPojo;
import com.indexify.indexify.pojo.ProductsGetPojo;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.indexify.indexify.R.id.rvProduct;
import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

/**
 * Created by amar on 29-12-2017.
 */

public class FragmentMyProducts extends Fragment {
    Context context;
    RecyclerView rvMyProduct;
    LinearLayoutManager mLayoutManager;
    List<ProductPojo> alMyProducts;
    long orgId;
    String nodeName = "";

    int i = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_my_products, container, false);
        geControls(v);
        return v;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MyOrganizationDataActivity) context).removeBreadScrumb(String.valueOf(i));
    }

    private void geControls(View v) {
        rvMyProduct = (RecyclerView) v.findViewById(R.id.rvMyProduct);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rvMyProduct.setHasFixedSize(true);
        rvMyProduct.setLayoutManager(mLayoutManager);
        nodeName = getArguments().getString("nodeName");
        orgId = getArguments().getLong("orgId");

        if (nodeName != null && nodeName.length() > 0) {
            i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
            setBreadScrumbData(nodeName, String.valueOf(i));
            //setBreadScrumbData(nodeName, ConstantsBreadScrumb.TAG_B_NODE);
            //
        }

        // if (getArguments().getBoolean("found")) {
        getData();
        // }

    }


    private void getData() {
        String type = "";
        type = getArguments().getString("type");
        if (type.equalsIgnoreCase("normal")) {
            String nodeId = getArguments().getString("nodeId");
            DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
            databaseAccess.open();
            alMyProducts = databaseAccess.getProductsByLeafNode(nodeId);

            if ((alMyProducts != null) && (alMyProducts.size() > 0)) {
                MyProductAdapter myProductAdapter = new MyProductAdapter(alMyProducts, context, orgId, type, nodeId);
                rvMyProduct.setAdapter(myProductAdapter);
            }
        } else if (type.equalsIgnoreCase("search")) {
            String searchText = getArguments().getString("nodeName");
            DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
            databaseAccess.open();
            List<ProductPojo> alSearchProducts = databaseAccess.searchProductsByName(searchText);
            databaseAccess.close();

            if (alSearchProducts.size() > 0) {
                MyProductAdapter adapter = new MyProductAdapter(alSearchProducts, context, orgId, type, searchText);
                rvMyProduct.setAdapter(adapter);
            }
        }


    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            //do when hidden
            Toast.makeText(context, "hide", Toast.LENGTH_SHORT).show();
        } else {
            //do when show
            Toast.makeText(context, "show", Toast.LENGTH_SHORT).show();
        }
    }


    private void setBreadScrumbData(String name, String tag) {
        BreadScrumbPojo breadScrumbPojo = new BreadScrumbPojo(name, tag);
        ((MyOrganizationDataActivity) context).setBreadScrumb(breadScrumbPojo);
    }

    private void showMessage(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

}
