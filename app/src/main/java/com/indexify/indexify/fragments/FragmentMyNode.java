package com.indexify.indexify.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indexify.indexify.MyOrganizationDataActivity;
import com.indexify.indexify.OrganizationDataActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.adapter.MyNodeAdapter;
import com.indexify.indexify.constants.ConstantsBreadScrumb;
import com.indexify.indexify.pojo.BreadScrumbPojo;
import com.indexify.indexify.pojo.NodePojo;
import com.stone.vega.library.VegaLayoutManager;

import java.util.List;

/**
 * Created by amar on 29-12-2017.
 */

public class FragmentMyNode extends Fragment {
    Context context;
    RecyclerView rvMyNode;
   // LinearLayoutManager mLayoutManager;
    int orgId ;
    int i = 0;

    /* public static FragmentNode newInstance(List<NodePojo> alNodes) {
         FragmentNode nodeFragment = new FragmentNode();

         Bundle bundle = new Bundle();
         bundle.putSerializable("node", (Serializable) alNodes);
         nodeFragment.setArguments(bundle);
         return nodeFragment;
     }
 */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_my_node, container, false);
        geControls(v);
        return v;

    }

    private void geControls(View v) {
        rvMyNode = (RecyclerView) v.findViewById(R.id.rvMyNode);
       // mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        rvMyNode.setHasFixedSize(true);
        rvMyNode.setLayoutManager(new VegaLayoutManager());

        String nodeName = getArguments().getString("nodeName");
        orgId = getArguments().getInt("orgId");

        if (nodeName != null && nodeName.length() > 0) {
            i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
            setBreadScrumbData(nodeName, String.valueOf(i));
            //setBreadScrumbData(nodeName, ConstantsBreadScrumb.TAG_B_NODE);
            //
        }else{

        }

        getData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MyOrganizationDataActivity) context).removeBreadScrumb(String.valueOf(i));
    }

    private void getData() {
        List<NodePojo> alMyNodes = (List<NodePojo>) getArguments().getSerializable("nodes");
        MyNodeAdapter adapter = new MyNodeAdapter(alMyNodes, context,orgId);
        rvMyNode.setAdapter(adapter);
    }

    private void setBreadScrumbData(String name, String tag) {
        BreadScrumbPojo breadScrumbPojo = new BreadScrumbPojo(name, tag);
        ((MyOrganizationDataActivity) context).setBreadScrumb(breadScrumbPojo);
    }

}
