package com.indexify.indexify;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;

public class ScanQRcodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler, View.OnClickListener {
    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;
    TextView tvSkip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        getControls();
    }

    private void getControls() {
        scannerView = (ZXingScannerView) findViewById(R.id.scanView);
        tvSkip = (TextView) findViewById(R.id.tvSkip);
        tvSkip.setOnClickListener(this);
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (scannerView == null) {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);
                scannerView.startCamera();
            } else {
                requestPermission();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        try{
            final String myResult = result.getText();
            Intent intent = new Intent(ScanQRcodeActivity.this, MainActivity.class)
                    .putExtra("scanned_info", myResult);
            setResult(Activity.RESULT_OK, intent);
            finish();

        }catch (Exception e){

        }



        /*Log.d("QRCodeScanner", result.getText());
        Log.d("QRCodeScanner", result.getBarcodeFormat().toString());
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Successfull")
                .setContentText(result.getText())
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        Intent intent = new Intent(ScanQRcodeActivity.this, MainActivity.class)
                                .putExtra("scanned_info", result.getText());
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                        sDialog.dismissWithAnimation();
                    }
                })
                .setCancelButton("Cancel", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        scannerView.resumeCameraPreview(ScanQRcodeActivity.this);
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();*/


    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
        // ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvSkip) {
            finish();
        }
    }
}
