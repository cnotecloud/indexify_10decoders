package com.indexify.indexify.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indexify.indexify.FavouriteListActivity;
import com.indexify.indexify.FavouriteProductListActivity;
import com.indexify.indexify.MyViewersListActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.fragments.FragmentFavouriteList;
import com.indexify.indexify.pojo.FavouriteOrganizationGetPojo;

import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by amar on 29-12-2017.
 */

public class FavouriteOrganizationAdapter extends RecyclerView.Adapter<FavouriteOrganizationAdapter.ViewHolder> {
    List<FavouriteOrganizationGetPojo> alFavOrganizations;
    Context context;

    public FavouriteOrganizationAdapter(List<FavouriteOrganizationGetPojo> alFavOrganizations, Context context) {
        this.alFavOrganizations = alFavOrganizations;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_favourite_organization_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FavouriteOrganizationGetPojo favouriteOrganizationGetPojo = alFavOrganizations.get(position);
        if (favouriteOrganizationGetPojo != null) {
            if (favouriteOrganizationGetPojo.getOrganizationName() != null && favouriteOrganizationGetPojo.getOrganizationName().length() > 0) {
                holder.tvOrganization.setText(WordUtils.capitalize(favouriteOrganizationGetPojo.getOrganizationName()));
            }
            holder.llOrganization.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConstantsMethods.isConnected(context)) {

                        Intent intent = new Intent(context, FavouriteProductListActivity.class);
                        intent.putExtra("orgName", favouriteOrganizationGetPojo.getOrganizationName());
                        intent.putExtra("orgId", favouriteOrganizationGetPojo.getOrganizationId());
                        context.startActivity(intent);
//                        FragmentFavouriteList fragmentFavouriteList = new FragmentFavouriteList();
//                        fragmentFavouriteList.startFavouriteListActivity(intent);

                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return alFavOrganizations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout llOrganization;
        TextView tvOrganization;

        public ViewHolder(View itemView) {
            super(itemView);
            llOrganization = (LinearLayout) itemView.findViewById(R.id.llOrganization);
            tvOrganization = (TextView) itemView.findViewById(R.id.tvOrganization);
            llOrganization.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.llOrganization) {

            }

        }
    }

}
