package com.indexify.indexify.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.indexify.indexify.MyProductViewActivity;
import com.indexify.indexify.ProductViewActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.pojo.ProductPojo;

import org.apache.commons.lang3.text.WordUtils;

import java.io.File;
import java.io.Serializable;
import java.util.List;

/**
 * Created by amar on 29-12-2017.
 */

public class MyProductAdapter extends RecyclerView.Adapter<MyProductAdapter.ViewHolder> {
    List<ProductPojo> alProducts;
    Context context;
    long orgId;
    String type = "";
    String text = "";


    public MyProductAdapter(List<ProductPojo> alProducts, Context context, long orgId,String type, String text) {
        this.alProducts = alProducts;
        this.context = context;
        this.orgId = orgId;
        this.type = type;
        this.text = text;
        // ConstantsViewPager.PRODUCT_LIST = alProducts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_my_product_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductPojo productPojo = alProducts.get(position);
        if (productPojo!= null) {

            holder.tvProductName.setText(WordUtils.capitalize(productPojo.getName()));
            if (productPojo.getLocalImgUrls() != null && productPojo.getLocalImgUrls().size() > 0) {
                try {
                    // show image from local path

                    Glide.with(context).load(productPojo.getLocalImgUrls().get(0))
                            .priority(Priority.IMMEDIATE)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(holder.imgProduct);

                    context.deleteFile(getFileName(productPojo.getLocalImgUrls().get(0).trim()));
                }catch (Exception error){
                    Toast.makeText(context, ""+error.toString(), Toast.LENGTH_SHORT).show();
            }
            } else {
                // show from server
                Glide.with(context).load(R.drawable.default_product_image)
                        .priority(Priority.IMMEDIATE)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(holder.imgProduct);
            }
            holder.llProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MyProductViewActivity.class);
                    intent.putExtra("products", (Serializable) alProducts);
                    intent.putExtra("position", position);
                    intent.putExtra("orgId", orgId);
                    intent.putExtra("type", type);
                    intent.putExtra("text", text);
                    context.startActivity(intent);
                }
            });
            holder.tvProductName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MyProductViewActivity.class);
                    intent.putExtra("products", (Serializable) alProducts);
                    intent.putExtra("position", position);
                    intent.putExtra("orgId", orgId);
                    intent.putExtra("type", type);
                    intent.putExtra("text", text);
                    context.startActivity(intent);
                }
            });
        }
    }
    String getFileName(String url) {
        return url.substring(url.lastIndexOf(File.separatorChar) + 1, url.length());
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return alProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout llProduct;
        TextView tvProductName;
        ImageView imgProduct;

        public ViewHolder(View itemView) {
            super(itemView);
            llProduct = (LinearLayout) itemView.findViewById(R.id.llProduct);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductName);
            imgProduct = (ImageView) itemView.findViewById(R.id.imgProduct);
            llProduct.setOnClickListener(this);
            tvProductName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if ((v.getId() == R.id.llProduct) || (v.getId() == R.id.tvProductName)) {

            }
        }
    }
}
