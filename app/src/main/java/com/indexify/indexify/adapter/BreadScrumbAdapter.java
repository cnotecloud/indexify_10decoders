package com.indexify.indexify.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indexify.indexify.OrganizationDataActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.pojo.BreadScrumbPojo;

import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

/**
 * Created by amar on 02-08-2017.
 */

public class BreadScrumbAdapter extends RecyclerView.Adapter<BreadScrumbAdapter.ViewHolder> {
    List<BreadScrumbPojo> alBreadScrumList;
    Context context;

    public BreadScrumbAdapter(List<BreadScrumbPojo> alBreadScrumList, Context context) {
        this.alBreadScrumList = alBreadScrumList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_bread_scrumb_list, parent, false);
        BreadScrumbAdapter.ViewHolder viewHolder = new BreadScrumbAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BreadScrumbPojo breadScrumbPojo = alBreadScrumList.get(position);
        if (breadScrumbPojo != null){
            holder.tvDisplayName.setText(WordUtils.capitalize(breadScrumbPojo.getDisplayText()));
            holder.tvDisplayName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        ((OrganizationDataActivity) context).removeBreadScrumbOnClik(breadScrumbPojo.getTag());
                    } catch (Exception e) {

                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return alBreadScrumList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvDisplayName;

        public ViewHolder(View itemView) {
            super(itemView);

            tvDisplayName = (TextView) itemView.findViewById(R.id.tvDisplayName);
            tvDisplayName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.tvDisplayName) {

            }
        }
    }
}
