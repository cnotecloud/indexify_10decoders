package com.indexify.indexify.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indexify.indexify.FavouriteProductListActivity;
import com.indexify.indexify.ProductInfoActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.api.FavouriteApi;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.FavouriteProductPojo;

import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

/**
 * Created by amar on 29-12-2017.
 */

public class FavouriteOrganizationProductAdapter extends RecyclerView.Adapter<FavouriteOrganizationProductAdapter.ViewHolder> {
    List<FavouriteProductPojo> alFavouriteProducts;
    Context context;

    public FavouriteOrganizationProductAdapter(List<FavouriteProductPojo> alFavouriteProducts, Context context) {
        this.alFavouriteProducts = alFavouriteProducts;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_favourite_organization_product_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FavouriteProductPojo favouriteProductPojo = alFavouriteProducts.get(position);
        if (favouriteProductPojo != null) {
            if (favouriteProductPojo.getProductName() != null && favouriteProductPojo.getProductName().length() > 0) {
                holder.tvProduct.setText(WordUtils.capitalize(favouriteProductPojo.getProductName()));
            }

            if (favouriteProductPojo.getCreated() != null && favouriteProductPojo.getCreated().length() > 0) {
                holder.tvDate.setText(ConstantsMethods.convertStringToDateStringFull(favouriteProductPojo.getCreated()));
            }
            holder.tvProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConstantsMethods.isConnected(context)) {
                        Intent intent = new Intent(context, ProductInfoActivity.class);
                        intent.putExtra("id", favouriteProductPojo.getProductId());
                        intent.putExtra("orgId", favouriteProductPojo.getOrganizationId());
                        intent.putExtra("name", favouriteProductPojo.getProductName());
                        context.startActivity(intent);
                    }
                }
            });
            holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConstantsMethods.isConnected(context)) {
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                        sweetAlertDialog.setTitleText("Are You Sure");
                        sweetAlertDialog.setContentText("Want to Delete " + favouriteProductPojo.getProductName() + " ?");
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setConfirmText("    OK    ");
                        sweetAlertDialog.setCancelText("     CANCEL    ");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                if (ConstantsMethods.isConnected(context)) {
                                    String id = favouriteProductPojo.getId();
                                    deleteFavourite(id, position);
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            }
                        });
                        sweetAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        });
                        sweetAlertDialog.show();
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return alFavouriteProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvProduct;
        TextView tvDate;
        ImageView imgDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            imgDelete = itemView.findViewById(R.id.imgDelete);
            tvProduct = (TextView) itemView.findViewById(R.id.tvProduct);
            tvDate = (TextView) itemView.findViewById(R.id.tvCreated);
            tvProduct.setOnClickListener(this);
            imgDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.tvProduct) {

            } else if (v.getId() == R.id.imgDelete) {

            }
        }
    }

    private void deleteFavourite(String id, int position) {
        if (ConstantsMethods.isConnected(context)) {
            ConstantsMethods.showProgessDialogWithCancelable(context, "please wait");
            try {
                FavouriteApi favouriteApi = retrofit.create(FavouriteApi.class);
                Observable<Response<Void>> observable = favouriteApi.deleteFavouriteProduct(id);
                observable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Response<Void>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(Response<Void> response) {
                                if (response.code() == 204) {
                                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE).setTitleText("Deleted.").setContentText("favourite deleted Successfully").setConfirmText("   OK   ");
                                    sweetAlertDialog.show();
                                    sweetAlertDialog.setCancelable(false);
                                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            ConstantsMethods.cancleProgessDialog();
                                            alFavouriteProducts.remove(position);
                                            notifyDataSetChanged();
                                            ((FavouriteProductListActivity) context).updateToolbar(alFavouriteProducts.size());
                                            sweetAlertDialog.dismissWithAnimation();
                                        }
                                    });
                                    // means deleted succesfully
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                                showError("Unknown Error","Something went wrong!",SweetAlertDialog.ERROR_TYPE);
                            }

                            @Override
                            public void onComplete() {
                                ConstantsMethods.cancleProgessDialog();
                            }
                        });
            } catch (Exception e) {
                ConstantsMethods.cancleProgessDialog();
            }
        }
    }
    private void showError(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                //finish();
            }
        });

    }
}
