package com.indexify.indexify.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indexify.indexify.MyProductFavouriteViewerActivity;
import com.indexify.indexify.MyScanListActivity;
import com.indexify.indexify.MyScannedUserInfoActivity;
import com.indexify.indexify.OrganizationInfoActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.api.FavouriteApi;
import com.indexify.indexify.api.UserScanApi;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.MyProductFavouriteViewerPojo;
import com.indexify.indexify.pojo.ResponsePojo;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

/**
 * Created by amar on 29-12-2017.
 */

public class FavouriteProductViewerAdapter extends RecyclerView.Adapter<FavouriteProductViewerAdapter.ViewHolder> {
    List<MyProductFavouriteViewerPojo> alMyProductFavouritePojo;
    Context context;

    public FavouriteProductViewerAdapter(List<MyProductFavouriteViewerPojo> alMyProductFavouritePojo, Context context) {
        this.alMyProductFavouritePojo = alMyProductFavouritePojo;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_favourite_product_viewer_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        MyProductFavouriteViewerPojo pojo = alMyProductFavouritePojo.get(position);
        if (pojo != null) {
            if ((pojo.getCreated() != null) && (pojo.getCreated().length() > 0)) {
                holder.tvScannedDate.setText(ConstantsMethods.convertStringToDateStringFull(pojo.getCreated()));
            }
            if ((pojo.getProductName() != null) && (pojo.getProductName().length() > 0)) {
                holder.tvProductName.setText(WordUtils.capitalize(pojo.getProductName()));
            }

            if ((pojo.getUserName() != null) && (pojo.getUserName().length() > 0)) {
                holder.tvUserName.setText(WordUtils.capitalize(pojo.getUserName()));
            }
            if ((pojo.getOrganizationName() != null) && (pojo.getOrganizationName().length() > 0)) {
                holder.tvUserName.setText(WordUtils.capitalize(pojo.getOrganizationName()));
            }
            if ((pojo.getUserId() > 0) && (pojo.getOrganizationId() == null)) {
                //cosider it as user and set Tag as user
                holder.imgTag.setBackgroundResource(R.drawable.tag_user);
            }
            if ((pojo.getUserId() > 0) && (pojo.getOrganizationId() != null) && (pojo.getOrganizationId().length() > 0)) {
                //cosider it as Organization and set Tag as Organization
                holder.imgTag.setBackgroundResource(R.drawable.tag_org);
            }

            holder.tvProductName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConstantsMethods.isConnected(context)) {

                        Intent intent = new Intent(context, MyProductFavouriteViewerActivity.class);
                        intent.putExtra("productId", pojo.getProductId());
                        context.startActivity(intent);
//                        FragmentFavouriteList fragmentFavouriteList = new FragmentFavouriteList();
//                        fragmentFavouriteList.startFavouriteListActivity(intent);

                    }
                }
            });

           holder.imgInfo.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   if ((pojo.getUserId() > 0) && (pojo.getOrganizationId() == null)) {
                       //cosider it as user and set Tag as user
                       Intent intent = new Intent(context, MyScannedUserInfoActivity.class);
                       intent.putExtra("userId",pojo.getUserId());
                       intent.putExtra("userName", pojo.getUserName());
                       context.startActivity(intent);
                   }
                   if ((pojo.getUserId() > 0) && (pojo.getOrganizationId() != null) && (pojo.getOrganizationId().length() > 0)) {
                       //cosider it as Organization and set Tag as Organization
                       Intent intent = new Intent(context.getApplicationContext(), OrganizationInfoActivity.class);
                       intent.putExtra("orgName", pojo.getOrganizationName());
                       long orgId = NumberUtils.createLong(pojo.getOrganizationId());
                       intent.putExtra("orgId", orgId);
                       context.startActivity(intent);
                   }

               }
           });
            holder.llUserInfo.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   if ((pojo.getUserId() > 0) && (pojo.getOrganizationId() == null)) {
                       //cosider it as user and set Tag as user
                       Intent intent = new Intent(context, MyScannedUserInfoActivity.class);
                       intent.putExtra("userId",pojo.getUserId());
                       intent.putExtra("userName", pojo.getUserName());
                       context.startActivity(intent);
                   }
                   if ((pojo.getUserId() > 0) && (pojo.getOrganizationId() != null) && (pojo.getOrganizationId().length() > 0)) {
                       //cosider it as Organization and set Tag as Organization
                       Intent intent = new Intent(context.getApplicationContext(), OrganizationInfoActivity.class);
                       intent.putExtra("orgName", pojo.getOrganizationName());
                       long orgId = NumberUtils.createLong(pojo.getOrganizationId());
                       intent.putExtra("orgId", orgId);
                       context.startActivity(intent);
                   }

               }
           });
            holder.tvUserName.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   if ((pojo.getUserId() > 0) && (pojo.getOrganizationId() == null)) {
                       //cosider it as user and set Tag as user
                       Intent intent = new Intent(context, MyScannedUserInfoActivity.class);
                       intent.putExtra("userId",pojo.getUserId());
                       intent.putExtra("userName", pojo.getUserName());
                       context.startActivity(intent);
                   }
                   if ((pojo.getUserId() > 0) && (pojo.getOrganizationId() != null) && (pojo.getOrganizationId().length() > 0)) {
                       //cosider it as Organization and set Tag as Organization
                       Intent intent = new Intent(context.getApplicationContext(), OrganizationInfoActivity.class);
                       intent.putExtra("orgName", pojo.getOrganizationName());
                       long orgId = NumberUtils.createLong(pojo.getOrganizationId());
                       intent.putExtra("orgId", orgId);
                       context.startActivity(intent);
                   }

               }
           });
            holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                    sweetAlertDialog.setTitleText("Are You Sure");
                    sweetAlertDialog.setContentText("Want to Delete " + pojo.getUserName() + " Favourite?");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.setConfirmText("    OK    ");
                    sweetAlertDialog.setCancelText("     CANCEL    ");
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            if (ConstantsMethods.isConnected(context)) {
                                if (pojo.getId() != null) {
                                    ConstantsMethods.showProgessDialog(context, "Please Wait...");
                                    String favouriteId = pojo.getId();
                                    deleteFavouriteFromList(favouriteId);
                                    sweetAlertDialog.dismiss();
                                }
                            }
                        }
                    });
                    sweetAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            sweetAlertDialog.dismiss();
                        }
                    });
                    sweetAlertDialog.show();
                }
            });

        }


    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return alMyProductFavouritePojo.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llUserInfo;
        TextView tvScannedDate;
        TextView tvProductName;
        TextView tvUserName;
        ImageView imgTag;
        ImageView imgInfo;
        ImageView imgDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            llUserInfo = (LinearLayout) itemView.findViewById(R.id.llUserInfo);
            tvScannedDate = (TextView) itemView.findViewById(R.id.tvScannedDate);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductName);
            tvUserName = (TextView) itemView.findViewById(R.id.tvUserName);
            imgTag = (ImageView) itemView.findViewById(R.id.imgTag);
            imgInfo = (ImageView) itemView.findViewById(R.id.imgInfo);
            imgDelete = (ImageView) itemView.findViewById(R.id.imgDelete);


        }


    }
        private void deleteFavouriteFromList(String favouriteId) {

            try {
                FavouriteApi favouriteApi = retrofit.create(FavouriteApi.class);
                Single<Response<Void>> deleteScan = favouriteApi.deleteMyFavouriteViewer(favouriteId);
                deleteScan.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<Response<Void>>() {
                    @Override
                    public void onSuccess(Response<Void> value) {
                        if (value.code() == 204) {
                            ConstantsMethods.cancleProgessDialog();
                            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                            sweetAlertDialog.setTitleText("Deleted!");
                            sweetAlertDialog.setContentText("Deleted Successfully");
                            sweetAlertDialog.setCancelable(false);
                            sweetAlertDialog.setConfirmText("    OK    ");
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    for (int i = 0; i < alMyProductFavouritePojo.size(); i++) {
                                        if (alMyProductFavouritePojo.get(i).getId().equalsIgnoreCase(favouriteId)) {
                                            alMyProductFavouritePojo.remove(i);
//                                            ((MyScanListActivity) context).deleteOrganizationScanById(orgScanId);
                                            notifyDataSetChanged();
                                            sweetAlertDialog.dismiss();
                                            break;

                                        }
                                    }

                                }
                            });
                            sweetAlertDialog.show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            HttpException ex = (HttpException) e;
                            String er = ex.response().errorBody().string();
                            ResponsePojo erroPojo = new Gson().fromJson(er, ResponsePojo.class);

                            if (erroPojo != null) {
                                if (erroPojo.getStatus() == 500) {
                                    showError("Internal server error", "Something went wrong please try later.");
                                } else if (erroPojo.getStatus() == 403) {
                                    showError("Access Denied", "Please Try Again");
                                }
                            }
                        } catch (Exception e1) {
                            showError("Error", "Something went wrong please try later.");
                        } finally {
                            ConstantsMethods.cancleProgessDialog();
                        }
                    }
                });


            } catch (Exception error) {

            }
        }

    private void showError(String title, String message) {

        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });


    }

}
