package com.indexify.indexify.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.indexify.indexify.R;

import java.io.File;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by amar on 02-04-2018.
 */

public class PdfAdapter extends RecyclerView.Adapter<PdfAdapter.ViewHolder> {
    List<String> alPdf;
    Context context;

    public PdfAdapter(List<String> alPdf, Context context) {
        this.alPdf = alPdf;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_pdf_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String pdfString = alPdf.get(position);
        String fileName = getFileNameFromLink(pdfString);
        holder.btnPdf.setText(fileName);
    }

    public String getFileNameFromLink(String url) {
        return url.substring(url.lastIndexOf(File.separatorChar) + 1, url.length());
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return alPdf.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button btnPdf;

        public ViewHolder(View itemView) {
            super(itemView);

            btnPdf = itemView.findViewById(R.id.btnPdf);
            btnPdf.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.btnPdf) {
                try {
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(alPdf.get(getAdapterPosition())));
                    context.startActivity(i);
                } catch (Exception e) {
                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
                    sweetAlertDialog.setTitleText("No App Found !");
                    sweetAlertDialog.setContentText("Please Click on Install Button to View File");
                    sweetAlertDialog.setCancelable(true);
                    sweetAlertDialog.setConfirmText(" OK ");
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.cancel();
                        }
                    });
                   /* sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.cancel();
                        }
                    });*/
                    sweetAlertDialog.show();
                }
            }
        }
    }
}
