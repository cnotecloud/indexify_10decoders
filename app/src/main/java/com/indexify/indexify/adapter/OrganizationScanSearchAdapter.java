package com.indexify.indexify.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indexify.indexify.MyScannedUserInfoActivity;
import com.indexify.indexify.MyViewersListActivity;
import com.indexify.indexify.MyViewersSearchListActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.api.OrganizationScanApi;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.OrganizationScanPojo;
import com.indexify.indexify.pojo.ResponsePojo;

import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

/**
 * Created by amar on 29-12-2017.
 */

public class OrganizationScanSearchAdapter extends RecyclerView.Adapter<OrganizationScanSearchAdapter.ViewHolder> {
    List<OrganizationScanPojo> alOrganizationScan;
    Context context;

    public OrganizationScanSearchAdapter(List<OrganizationScanPojo> alOrganizationScan, Context context) {
        this.alOrganizationScan = alOrganizationScan;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_organization_scan_search_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrganizationScanPojo organizationScanPojo = alOrganizationScan.get(position);
        if (organizationScanPojo != null) {
            if (organizationScanPojo.getName() != null && organizationScanPojo.getName().length() > 0) {
                holder.tvUserName.setText(WordUtils.capitalize(organizationScanPojo.getName()));
            }
            if ((organizationScanPojo.getCreated() != null) && (organizationScanPojo.getCreated().length() > 0)) {
                holder.tvUserScannedDate.setText(ConstantsMethods.convertStringToDateStringFull(organizationScanPojo.getCreated()));
            }
            holder.llOrganizationScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConstantsMethods.isConnected(context)) {
                        Intent intent = new Intent(context, MyScannedUserInfoActivity.class);
                        intent.putExtra("userId", organizationScanPojo.getUserId());
                        intent.putExtra("userName", organizationScanPojo.getName());
                        context.startActivity(intent);
                    }
                }
            });
            holder.llDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                    sweetAlertDialog.setTitleText("Are You Sure");
                    sweetAlertDialog.setContentText("Want to Delete " + organizationScanPojo.getName() + " Viewer?");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.setConfirmText("    OK    ");
                    sweetAlertDialog.setCancelText("     CANCEL    ");
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            if (ConstantsMethods.isConnected(context)) {
                                if (organizationScanPojo.getId() != null) {
                                    ConstantsMethods.showProgessDialog(context,"Please Wait...");
                                    String userScanId = organizationScanPojo.getId();
                                    deleteScanFromList(userScanId);
                                    sweetAlertDialog.dismiss();
                                }
                            }
                        }
                    });
                    sweetAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            sweetAlertDialog.dismiss();
                        }
                    });
                    sweetAlertDialog.show();
                }
            });

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return alOrganizationScan.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout llOrganizationScan;
        LinearLayout llDelete;
        TextView tvUserName;
        TextView tvUserScannedDate;

        public ViewHolder(View itemView) {
            super(itemView);
            llOrganizationScan = (LinearLayout) itemView.findViewById(R.id.llOrganizationScan);
            llDelete = (LinearLayout) itemView.findViewById(R.id.llDelete);
            tvUserName = (TextView) itemView.findViewById(R.id.tvUserName);
            tvUserScannedDate = (TextView) itemView.findViewById(R.id.tvUserScannedDate);
            llOrganizationScan.setOnClickListener(this);
            llDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.llOrganizationScan || v.getId() == R.id.llInfo) {

            }else if (v.getId() == R.id.llDelete){


            }

        }
    }

    private void deleteScanFromList(String userScanId) {
        try {
            OrganizationScanApi organizationScanApi = retrofit.create(OrganizationScanApi.class);
            Single<Response<Void>> deleteUser = organizationScanApi.deleteScannedUser(userScanId);
            deleteUser.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<Response<Void>>() {
                @Override
                public void onSuccess(Response<Void> value) {
                    if (value.code() == 204) {
                        ConstantsMethods.cancleProgessDialog();
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Deleted!");
                        sweetAlertDialog.setContentText("Deleted Successfully");
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setConfirmText("    OK    ");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                for (int i = 0; i < alOrganizationScan.size(); i++) {
                                    if (alOrganizationScan.get(i).getId().equalsIgnoreCase(userScanId)) {
                                        alOrganizationScan.remove(i);
                                        ((MyViewersSearchListActivity) context).deleteUserScanById(userScanId);
                                        notifyDataSetChanged();
                                        sweetAlertDialog.dismiss();
                                        break;

                                    }
                                }

                            }
                        });
                        sweetAlertDialog.show();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    try {
                        HttpException ex = (HttpException) e;
                        String er = ex.response().errorBody().string();
                        ResponsePojo erroPojo = new Gson().fromJson(er, ResponsePojo.class);

                        if (erroPojo != null) {
                            if (erroPojo.getStatus() == 500) {
                                showError("Internal server error", "Something went wrong please try later.");
                            } else if (erroPojo.getStatus() == 403) {
                                showError("Access Denied", "Please Try Again");
                            }
                        }
                    } catch (Exception e1) {
                        showError("Error", "Something went wrong please try later.");
                    } finally {
                        ConstantsMethods.cancleProgessDialog();
                    }
                }
            });


        } catch (Exception error) {

        }
    }
    private void showError(String title, String message) {

        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });


    }
}
