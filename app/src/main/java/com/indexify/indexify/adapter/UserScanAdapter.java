package com.indexify.indexify.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indexify.indexify.MyScanListActivity;
import com.indexify.indexify.MyScannedUserInfoActivity;
import com.indexify.indexify.OrganizationDataActivity;
import com.indexify.indexify.OrganizationInfoActivity;
import com.indexify.indexify.R;
import com.indexify.indexify.api.UserScanApi;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.ResponsePojo;
import com.indexify.indexify.pojo.UserScanPojo;

import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

/**
 * Created by amar on 29-12-2017.
 */

public class UserScanAdapter extends RecyclerView.Adapter<UserScanAdapter.ViewHolder> {
    List<UserScanPojo> alUserScans;
    Context context;


    public UserScanAdapter(List<UserScanPojo> alUserScans, Context context) {
        this.alUserScans = alUserScans;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_user_scan_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserScanPojo userScanPojo = alUserScans.get(position);
        if (userScanPojo != null) {
            if (userScanPojo.getName() != null && userScanPojo.getName().length() > 0) {
                holder.tvOrganization.setText(WordUtils.capitalize(userScanPojo.getName()));
            }
            if ((userScanPojo.getCreated() != null) && (userScanPojo.getCreated().length() > 0)) {
                holder.tvScannedDate.setText(ConstantsMethods.convertStringToDateStringFull(userScanPojo.getCreated()));
            }

            if (userScanPojo.getUserId() > 0) {
                holder.imgtag.setBackgroundResource(R.drawable.tag_user);
            }else if(userScanPojo.getOrganizationId() > 0){
                holder.imgtag.setBackgroundResource(R.drawable.tag_org);
            }



            holder.llUserScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConstantsMethods.isConnected(context)) {
                        if (userScanPojo.getOrganizationId() > 0) {
                            Intent intent = new Intent(context.getApplicationContext(), OrganizationDataActivity.class);
                            intent.putExtra("orgName", userScanPojo.getName());
                            intent.putExtra("orgId", userScanPojo.getOrganizationId());
                            ((MyScanListActivity) context).startForActivityResult(intent);
                            //context.startActivity(intent);
                        }else if (userScanPojo.getUserId() > 0){
                            Intent intent = new Intent(context, MyScannedUserInfoActivity.class);
                            intent.putExtra("userId",userScanPojo.getUserId());
                            intent.putExtra("userName", userScanPojo.getName());
                            context.startActivity(intent);
                        }
                    }
                }
            });

            holder.llInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ConstantsMethods.isConnected(context)) {
                        if (userScanPojo.getOrganizationId() > 0 ) {
                            Intent intent = new Intent(context, OrganizationInfoActivity.class);
                            intent.putExtra("orgId", userScanPojo.getOrganizationId());
                            intent.putExtra("orgName", userScanPojo.getName());
                            context.startActivity(intent);
                        }else if (userScanPojo.getUserId() > 0){
                            Intent intent = new Intent(context, MyScannedUserInfoActivity.class);
                            intent.putExtra("userId", userScanPojo.getUserId());
                            intent.putExtra("userName",userScanPojo.getName());
                            context.startActivity(intent);
                        }
                    }
                }
            });
            holder.llDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                    sweetAlertDialog.setTitleText("Are You Sure");
                    sweetAlertDialog.setContentText("Want to Delete " + userScanPojo.getName() + " scan?");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.setConfirmText("    OK    ");
                    sweetAlertDialog.setCancelText("     CANCEL    ");
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            if (ConstantsMethods.isConnected(context)) {
                                if (userScanPojo.getId() != null) {
                                    ConstantsMethods.showProgessDialog(context, "Please Wait...");
                                    String orgScanId = userScanPojo.getId();
                                    deleteScanFromList(orgScanId);
                                    sweetAlertDialog.dismiss();
                                }
                            }
                        }
                    });
                    sweetAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            sweetAlertDialog.dismiss();
                        }
                    });
                    sweetAlertDialog.show();
                }
            });
        }




    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return alUserScans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout llUserScan;
        LinearLayout llInfo;
        LinearLayout llDelete;
        TextView tvOrganization;
        TextView tvScannedDate;
        ImageView imgtag;

        public ViewHolder(View itemView) {
            super(itemView);
            llUserScan = (LinearLayout) itemView.findViewById(R.id.llUserScan);
            llInfo = (LinearLayout) itemView.findViewById(R.id.llInfo);
            llDelete = (LinearLayout) itemView.findViewById(R.id.llDelete);
            tvOrganization = (TextView) itemView.findViewById(R.id.tvOrganization);
            tvScannedDate = (TextView) itemView.findViewById(R.id.tvScannedDate);
            imgtag = itemView.findViewById(R.id.imgTag);


//            llUserScan.setOnClickListener(this);
//            llInfo.setOnClickListener(this);
//            llDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.llUserScan) {

            } else if (v.getId() == R.id.llInfo) {

            } else if (v.getId() == R.id.llDelete) {


            }

        }


    }

    private void deleteScanFromList(String orgScanId) {

        try {
            UserScanApi userScanApi = retrofit.create(UserScanApi.class);
            Single<Response<Void>> deleteScan = userScanApi.deleteScannedOrganization(orgScanId);
            deleteScan.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableSingleObserver<Response<Void>>() {
                @Override
                public void onSuccess(Response<Void> value) {
                    if (value.code() == 204) {
                        ConstantsMethods.cancleProgessDialog();
                        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                        sweetAlertDialog.setTitleText("Deleted!");
                        sweetAlertDialog.setContentText("Deleted Successfully");
                        sweetAlertDialog.setCancelable(false);
                        sweetAlertDialog.setConfirmText("    OK    ");
                        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                for (int i = 0; i < alUserScans.size(); i++) {
                                    if (alUserScans.get(i).getId().equalsIgnoreCase(orgScanId)) {
                                        alUserScans.remove(i);
                                        ((MyScanListActivity) context).deleteOrganizationScanById(orgScanId);
                                        notifyDataSetChanged();
                                        sweetAlertDialog.dismiss();
                                        break;

                                    }
                                }

                            }
                        });
                        sweetAlertDialog.show();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    try {
                        HttpException ex = (HttpException) e;
                        String er = ex.response().errorBody().string();
                        ResponsePojo erroPojo = new Gson().fromJson(er, ResponsePojo.class);

                        if (erroPojo != null) {
                            if (erroPojo.getStatus() == 500) {
                                showError("Internal server error", "Something went wrong please try later.");
                            } else if (erroPojo.getStatus() == 403) {
                                showError("Access Denied", "Please Try Again");
                            }
                        }
                    } catch (Exception e1) {
                        showError("Error", "Something went wrong please try later.");
                    } finally {
                        ConstantsMethods.cancleProgessDialog();
                    }
                }
            });


        } catch (Exception error) {

        }
    }


    private void showError(String title, String message) {

        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });


    }
}
