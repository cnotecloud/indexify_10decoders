package com.indexify.indexify.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.indexify.indexify.R;
import com.indexify.indexify.constants.ConstantsBreadScrumb;
import com.indexify.indexify.database.DatabaseAccess;
import com.indexify.indexify.fragments.FragmentMyNode;
import com.indexify.indexify.fragments.FragmentMyProducts;
import com.indexify.indexify.fragments.FragmentNode;
import com.indexify.indexify.fragments.FragmentProduct;
import com.indexify.indexify.pojo.NodePojo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by amar on 29-12-2017.
 */

public class MyNodeAdapter extends RecyclerView.Adapter<MyNodeAdapter.ViewHolder> {
    List<NodePojo> alNodes;
    Context context;
    int orgId;

    public MyNodeAdapter(List<NodePojo> alNodes, Context context, int orgId) {
        this.alNodes = alNodes;
        this.context = context;
        this.orgId = orgId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_my_node_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NodePojo nodePojo = alNodes.get(position);
        if (nodePojo != null) {
            holder.tvNode.setText(nodePojo.getDisplayName());
            holder.llNode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    NodePojo nodePojo = alNodes.get(getAdapterPosition());
                    if (nodePojo.isLeaf()) {
                        int i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
                        FragmentMyProducts fragmentMyProducts = new FragmentMyProducts();
                        Bundle bundle = new Bundle();
                        bundle.putString("nodeName", nodePojo.getDisplayName());
                        bundle.putString("nodeId", nodePojo.getId());
                        bundle.putLong("orgId", orgId);
                        bundle.putString("type", "normal");
                        fragmentMyProducts.setArguments(bundle);
                        FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        // transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        transaction.add(R.id.myFrameLayout, fragmentMyProducts, String.valueOf(i));
                        transaction.addToBackStack(String.valueOf(fragmentMyProducts));
                        transaction.commit();

                    } else {
                        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
                        databaseAccess.open();
                        List<NodePojo> alNodes = databaseAccess.getMyNodes(nodePojo.getId());
                        databaseAccess.close();

                        if (alNodes.size() > 0) {
                            int i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
                            FragmentMyNode fragmentMyNode = new FragmentMyNode();
                            Bundle bundle = new Bundle();
                            bundle.putString("nodeName", nodePojo.getDisplayName());
                            bundle.putString("nodeId", String.valueOf(i));
                            bundle.putInt("orgId", orgId);
                            bundle.putSerializable("nodes", (Serializable) alNodes);
                            fragmentMyNode.setArguments(bundle);
                            FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
                            //  transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                            //transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                            transaction.add(R.id.myFrameLayout, fragmentMyNode, String.valueOf(i));
                            transaction.addToBackStack(String.valueOf(fragmentMyNode));
                            transaction.commit();

                        } else if (alNodes.size() == 0) {
                            Toast.makeText(context, "not found", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
            holder.tvNode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (nodePojo.isLeaf()) {
                        int i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
                        FragmentMyProducts fragmentMyProducts = new FragmentMyProducts();
                        Bundle bundle = new Bundle();
                        bundle.putString("nodeName", nodePojo.getDisplayName());
                        bundle.putString("nodeId", nodePojo.getId());
                        bundle.putLong("orgId", orgId);
                        bundle.putString("type", "normal");
                        fragmentMyProducts.setArguments(bundle);
                        FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        // transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                        transaction.add(R.id.myFrameLayout, fragmentMyProducts, String.valueOf(i));
                        transaction.addToBackStack(String.valueOf(fragmentMyProducts));
                        transaction.commit();

                    } else {
                        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(context);
                        databaseAccess.open();
                        List<NodePojo> alNodes = databaseAccess.getMyNodes(nodePojo.getId());
                        databaseAccess.close();

                        if (alNodes.size() > 0) {
                            int i = ++ConstantsBreadScrumb.BREADSCRUMB_INIT;
                            FragmentMyNode fragmentMyNode = new FragmentMyNode();
                            Bundle bundle = new Bundle();
                            bundle.putString("nodeName", nodePojo.getDisplayName());
                            bundle.putString("nodeId", String.valueOf(i));
                            bundle.putInt("orgId", orgId);
                            bundle.putSerializable("nodes", (Serializable) alNodes);
                            fragmentMyNode.setArguments(bundle);
                            FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
                            //  transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                            //transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                            transaction.add(R.id.myFrameLayout, fragmentMyNode, String.valueOf(i));
                            transaction.addToBackStack(String.valueOf(fragmentMyNode));
                            transaction.commit();

                        } else if (alNodes.size() == 0) {
                            Toast.makeText(context, "not found", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return alNodes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout llNode;
        TextView tvNode;

        public ViewHolder(View itemView) {
            super(itemView);
            llNode = (LinearLayout) itemView.findViewById(R.id.llNode);
            tvNode = (TextView) itemView.findViewById(R.id.tvNode);
            llNode.setOnClickListener(this);
            tvNode.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if ((v.getId() == R.id.llNode) || (v.getId() == R.id.tvNode)) {

            }
        }
    }
}
