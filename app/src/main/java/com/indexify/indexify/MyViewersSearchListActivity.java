package com.indexify.indexify;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.indexify.indexify.adapter.OrganizationScanSearchAdapter;
import com.indexify.indexify.api.OrganizationScanApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.constants.ConstantsProjection;
import com.indexify.indexify.interfaces.DeleteUserScanById;
import com.indexify.indexify.pojo.OrganizationScanGetPojo;
import com.indexify.indexify.pojo.OrganizationScanPojo;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class MyViewersSearchListActivity extends AppCompatActivity implements DeleteUserScanById{

    RecyclerView rvMyViewersList;
    LinearLayoutManager mLayoutManager;
    ProgressBar progressbar;
    SearchView search;
    List<OrganizationScanPojo> alViewersList = new ArrayList<>();
    OrganizationScanSearchAdapter adapter = null;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private boolean loading = true;
    private int previousTotal = 0;
    private int visibleThreshold = 5;
    int page = 0;
    String queryText = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_viewers_search_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getControls();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getControls() {
        resetValues();
        queryText = getIntent().getStringExtra("searchText");
        rvMyViewersList = (RecyclerView) findViewById(R.id.rvMyViewersList);
        mLayoutManager = new LinearLayoutManager(MyViewersSearchListActivity.this, LinearLayoutManager.VERTICAL, false);
        rvMyViewersList.setHasFixedSize(true);
        rvMyViewersList.setLayoutManager(mLayoutManager);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.INVISIBLE);

        rvMyViewersList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = rvMyViewersList.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (ConstantsMethods.isConnected(MyViewersSearchListActivity.this)) {
                                searchMoreViewers();
                            }
                        }
                    });
                    // Do something
                    loading = true;
                }
            }
        });

        if (ConstantsMethods.isConnected(MyViewersSearchListActivity.this)) {
            getData();
        }
    }

    private void searchMoreViewers() {
        if (ConstantsMethods.isConnectedWithoutMessage(MyViewersSearchListActivity.this)) {
            progressbar.setVisibility(View.VISIBLE);
            try {
                OrganizationScanApi organizationScanApi = retrofit.create(OrganizationScanApi.class);
                Observable<OrganizationScanGetPojo> observable = organizationScanApi.searchOrgScanListContainingName(queryText, ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_USER_SCAN_LOADING_SIZE, ++page);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<OrganizationScanGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(OrganizationScanGetPojo organizationScanGetPojo) {
                                progressbar.setVisibility(View.INVISIBLE);
                                if (organizationScanGetPojo != null && organizationScanGetPojo.getEmbedded().getOrganizationScans().size() > 0) {
                                    alViewersList.addAll(organizationScanGetPojo.getEmbedded().getOrganizationScans());
                                    adapter.notifyDataSetChanged();
                                } else {
                                    // show not found message
                                    //showError("Not found", "User scan data not found", 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                progressbar.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {
                progressbar.setVisibility(View.INVISIBLE);
            } finally {

            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_viewers_search_list_activity, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search = (SearchView) menu.findItem(R.id.action_search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() < 3) {
                    Toast.makeText(MyViewersSearchListActivity.this, "Enter minimum 3 chars", Toast.LENGTH_SHORT).show();
                } else {
                    // check internet connectin
                    if (ConstantsMethods.isConnected(MyViewersSearchListActivity.this)) {
                        resetValues();
                        queryText = query;
                        searchViewersContaining(query);
                    }
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                /*if (newText.length() == 0) {
                    rvUserList.setAdapter(adapter);
                }*/
                return true;
            }
        });
        return true;
    }

    private void searchViewersContaining(String query) {
        if (ConstantsMethods.isConnected(MyViewersSearchListActivity.this)) {
            try {
                ConstantsMethods.showProgessDialogWithCancelable(MyViewersSearchListActivity.this, "Searching...");

                OrganizationScanApi organizationScanApi = retrofit.create(OrganizationScanApi.class);
                Observable<OrganizationScanGetPojo> observable = organizationScanApi.searchOrgScanListContainingName(query.toLowerCase(), ConstantsProjection.PROJECTION_DETAIL, Constants.DEFAULT_USER_SCAN_LOADING_SIZE, 0);
                observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<OrganizationScanGetPojo>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(OrganizationScanGetPojo organizationScanGetPojo) {
                                ConstantsMethods.cancleProgessDialog();

                                if (organizationScanGetPojo != null && organizationScanGetPojo.getEmbedded().getOrganizationScans().size() > 0) {

                                    alViewersList.addAll(organizationScanGetPojo.getEmbedded().getOrganizationScans());
                                    adapter = new OrganizationScanSearchAdapter(alViewersList, MyViewersSearchListActivity.this);
                                    rvMyViewersList.setAdapter(adapter);

                                } else {
                                    // show not found message
                                    showErrorWithoutFinish("Not found", "Viewers data not found", 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                ConstantsMethods.cancleProgessDialog();
                                showErrorWithoutFinish("Not found", "Viewers data not found", 1);
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            } catch (Exception e) {

            } finally {

            }
        }
    }


    public void getData() {
        //get my scan from server
        List<OrganizationScanPojo> alViewers = (List<OrganizationScanPojo>) getIntent().getSerializableExtra("viewers");

        if (alViewers != null && alViewers.size() > 0) {

            alViewersList.addAll(alViewers);
            adapter = new OrganizationScanSearchAdapter(alViewersList, MyViewersSearchListActivity.this);
            rvMyViewersList.setAdapter(adapter);

        } else {
            // show not found message
            //showError("Not found", "User scan data not found", 1);
        }
    }

    private void showError(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MyViewersSearchListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        });

    }


    private void showErrorWithoutFinish(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MyViewersSearchListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                //  finish();
            }
        });

    }

    public void resetValues() {
        alViewersList.clear();
        adapter = null;
        page = 0;
        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        loading = true;
        previousTotal = 0;
        visibleThreshold = 5;
    }

    @Override
    public void deleteUserScanById(String userScanId) {
        for (int i = 0; i < alViewersList.size(); i++) {
            if (alViewersList.get(i).getId().equalsIgnoreCase(userScanId)) {
                alViewersList.remove(i);
                adapter = new OrganizationScanSearchAdapter(alViewersList, MyViewersSearchListActivity.this);
                adapter.notifyDataSetChanged();
                rvMyViewersList.setAdapter(adapter);
            }
        }
    }
}
