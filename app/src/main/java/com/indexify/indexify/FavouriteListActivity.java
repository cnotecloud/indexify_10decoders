package com.indexify.indexify;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.indexify.indexify.constants.ConstantsEasySP;
import com.indexify.indexify.fragments.FragmentFavouriteList;
import com.indexify.indexify.fragments.FragmentMyProductFavourite;
import com.indexify.indexify.interfaces.FinishOrgDataAndUserScanActivity;
import com.indexify.indexify.pojo.UserInfoPojo;
import com.white.easysp.EasySP;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class FavouriteListActivity extends AppCompatActivity implements FinishOrgDataAndUserScanActivity{
    TabLayout tabLayout;
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getControls();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getControls() {
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        getData();



    }

    private void getData() {
        sendDataToFragment();
    }


    private void showError(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(FavouriteListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        });

    }

    private void showErrorWithoutFinish(String title, String message, int type) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(FavouriteListActivity.this, type);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                //    finish();
            }
        });

    }

    private void sendDataToFragment() {
        String userInfojson = EasySP.init(FavouriteListActivity.this).getString(ConstantsEasySP.SP_USER_INFO);
        UserInfoPojo userInfoPojo = new Gson().fromJson(userInfojson, UserInfoPojo.class);
    ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        //int limit = (viewPagerAdapter.getCount() > 1 ? viewPagerAdapter.getCount() - 1 : 1);
        FragmentFavouriteList fragmentFavouriteList = new FragmentFavouriteList();
        viewPagerAdapter.addFragment(fragmentFavouriteList, "By Me");
        FragmentMyProductFavourite myProductFavourite = new FragmentMyProductFavourite();
        if (userInfoPojo != null){
            if (userInfoPojo.getOrganizationPojo() != null){
                viewPagerAdapter.addFragment(myProductFavourite, "By Others");
            }

        }


        //viewPager.setOffscreenPageLimit(limit);
        viewPager.setAdapter(viewPagerAdapter);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
//                    fragmentFavouriteList.resetValues();
//                    fragmentFavouriteList.getData();

                } else if (position == 1) {
                    if (userInfoPojo.getOrganizationPojo()!= null) {
                        myProductFavourite.resetValues();
                        myProductFavourite.getData();
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });





    }

    @Override
    public void finishOrgDataAndUserScanActivity() {
        finish();
    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }




}
