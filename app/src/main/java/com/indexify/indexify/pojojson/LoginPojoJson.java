package com.indexify.indexify.pojojson;

import java.io.Serializable;

/**
 * Created by amar on 19-01-2018.
 */

public class LoginPojoJson implements Serializable {
    String username;
    String password;

    public LoginPojoJson(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
