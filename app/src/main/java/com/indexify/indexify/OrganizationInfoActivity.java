package com.indexify.indexify;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indexify.indexify.api.OrganizationApi;
import com.indexify.indexify.constants.Constants;
import com.indexify.indexify.constants.ConstantsMethods;
import com.indexify.indexify.pojo.OrganizationInfoPojo;
import com.indexify.indexify.pojo.ResponsePojo;

import org.apache.commons.lang3.text.WordUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static com.indexify.indexify.interceptor.ApplicationController.retrofit;

public class OrganizationInfoActivity extends AppCompatActivity {
    TextView tvOrgName;
    TextView tvDescription;
    TextView tvWebsite;
    TextView tvContactNumber;
    TextView tvEmailId;
    TextView tvCountry;
    TextView tvRegion;
    TextView tvCity;
    TextView tvAddress;
    TextView tvPostalCode;
    long orgId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(WordUtils.capitalize(getIntent().getStringExtra("orgName")));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        orgId = getIntent().getLongExtra("orgId", 0);

        getControls();
    }

    private void getControls() {
        tvOrgName = (TextView) findViewById(R.id.tvOrgName);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvWebsite = (TextView) findViewById(R.id.tvWebsite);
        tvContactNumber = (TextView) findViewById(R.id.tvContactNumber);
        tvEmailId = (TextView) findViewById(R.id.tvEmailId);
        tvCountry = (TextView) findViewById(R.id.tvCountry);
        tvRegion = (TextView) findViewById(R.id.tvRegion);
        tvCity = (TextView) findViewById(R.id.tvCity);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvPostalCode = (TextView) findViewById(R.id.tvPostalCode);
        getData();
    }

    private void getData() {
        if (ConstantsMethods.isConnected(OrganizationInfoActivity.this)) {
            ConstantsMethods.showProgessDialog(OrganizationInfoActivity.this, "Loading...");
            getOrgInfoFromServer();
        }

    }

    private void getOrgInfoFromServer() {
        OrganizationApi detailsApi = retrofit.create(OrganizationApi.class);
        Single<OrganizationInfoPojo> orgResponse = detailsApi.getOrgDetails(orgId);
        orgResponse.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<OrganizationInfoPojo>() {
                    @Override
                    public void onSuccess(OrganizationInfoPojo organizationInfoPojo) {
                        ConstantsMethods.cancleProgessDialog();
                        setOrgDetails(organizationInfoPojo);
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            HttpException ex = (HttpException) e;
                            String er = ex.response().errorBody().string();
                            ResponsePojo responsePojo = new Gson().fromJson(er, ResponsePojo.class);

                            if (responsePojo != null) {
                                if (responsePojo.getStatus() == 404) {
                                    showErrorwithFinish("Not Found", "No Organization Found");
                                } else if (responsePojo.getStatus() == 403) {
                                    showErrorwithFinish("No access", "Please try again or contact admin.");
                                } else if (responsePojo.getStatus() == 500) {
                                    showErrorwithFinish("Internal server error", "Something went wrong please try later.");
                                } else if (responsePojo.getStatus() == 400) {
                                    showErrorwithFinish("Bad Request", "Something went wrong please try later.");
                                }
                            }
                        } catch (Exception e1) {
                            showErrorwithFinish("Error", "Something went wrong please try later.");

                        } finally {
                            ConstantsMethods.cancleProgessDialog();
                        }
                    }
                });

    }

    private void setOrgDetails(OrganizationInfoPojo organizationInfoPojo) {
        if (organizationInfoPojo != null) {

            if ((organizationInfoPojo.getDisplayName() != null) && (organizationInfoPojo.getDisplayName().length() > 0)) {
                tvOrgName.setText(WordUtils.capitalize(organizationInfoPojo.getDisplayName()));
            } else {
                tvOrgName.setText(Constants.IF_EMPTY);
            }
            if ((organizationInfoPojo.getDescription() != null) && (organizationInfoPojo.getDescription().length() > 0)) {
                tvDescription.setText(organizationInfoPojo.getDescription());
            } else {
                tvDescription.setText(Constants.IF_EMPTY);
            }
            if ((organizationInfoPojo.getWebsite() != null) && (organizationInfoPojo.getWebsite().length() > 0)) {
                tvWebsite.setText(organizationInfoPojo.getWebsite());
            } else {
                tvWebsite.setText(Constants.IF_EMPTY);
            }

            if ((organizationInfoPojo.getAlEmails() != null) && (organizationInfoPojo.getAlEmails().size() > 0)) {
                String organizationEmails = "";
                for (int i = 0; i < organizationInfoPojo.getAlEmails().size(); i++) {
                    if ((organizationInfoPojo.getAlEmails().get(i).getValue() != null) && (organizationInfoPojo.getAlEmails().get(i).getValue().toString().length() > 0)) {
                        organizationEmails = organizationEmails + organizationInfoPojo.getAlEmails().get(i).getValue().toString() + "\n\n";
                    }
                }
                tvEmailId.setText(organizationEmails.trim());
            } else {
                tvEmailId.setText(Constants.IF_EMPTY);
            }
            if ((organizationInfoPojo.getAlContacts() != null) && (organizationInfoPojo.getAlContacts().size() > 0)) {
                String organizationContacts = "";
                for (int j = 0; j < organizationInfoPojo.getAlContacts().size(); j++) {
                    if ((organizationInfoPojo.getAlContacts().get(j).getValue() != null) && (organizationInfoPojo.getAlContacts().get(j).getValue().toString().length() > 0)) {
                        organizationContacts = organizationContacts + organizationInfoPojo.getAlContacts().get(j).getValue().toString() + "\n\n";


                    }
                }
                tvContactNumber.setText(organizationContacts.trim());
            } else {
                tvContactNumber.setText(Constants.IF_EMPTY);
            }

            if (organizationInfoPojo.getLocationPojo() != null) {
                if ((organizationInfoPojo.getLocationPojo().getAddress() != null) && (organizationInfoPojo.getLocationPojo().getAddress().length() > 0)) {
                    tvAddress.setText(WordUtils.capitalize(organizationInfoPojo.getLocationPojo().getAddress()));
                } else {
                    tvAddress.setText(Constants.IF_EMPTY);
                }
                if ((organizationInfoPojo.getLocationPojo().getCountry() != null) && (organizationInfoPojo.getLocationPojo().getCountry().length() > 0)) {
                    tvCountry.setText(WordUtils.capitalize(organizationInfoPojo.getLocationPojo().getCountry()));
                } else {
                    tvCountry.setText(Constants.IF_EMPTY);
                }
                if ((organizationInfoPojo.getLocationPojo().getRegion() != null) && (organizationInfoPojo.getLocationPojo().getRegion().length() > 0)) {
                    tvRegion.setText(WordUtils.capitalize(organizationInfoPojo.getLocationPojo().getRegion()));
                } else {
                    tvRegion.setText(Constants.IF_EMPTY);
                }
                if ((organizationInfoPojo.getLocationPojo().getCity() != null) && (organizationInfoPojo.getLocationPojo().getCity().length() > 0)) {
                    tvCity.setText(WordUtils.capitalize(organizationInfoPojo.getLocationPojo().getCity()));
                } else {
                    tvCity.setText(Constants.IF_EMPTY);
                }
                if ((organizationInfoPojo.getLocationPojo().getPostalCode() != null) && (organizationInfoPojo.getLocationPojo().getPostalCode().length() > 0)) {
                    tvPostalCode.setText(organizationInfoPojo.getLocationPojo().getPostalCode());
                } else {
                    tvPostalCode.setText(Constants.IF_EMPTY);
                }
            } else {
                tvAddress.setText(Constants.IF_EMPTY);
                tvCountry.setText(Constants.IF_EMPTY);
                tvRegion.setText(Constants.IF_EMPTY);
                tvCity.setText(Constants.IF_EMPTY);
                tvPostalCode.setText(Constants.IF_EMPTY);
            }

        } else {
            tvOrgName.setText(Constants.IF_EMPTY);
            tvDescription.setText(Constants.IF_EMPTY);
            tvWebsite.setText(Constants.IF_EMPTY);
            tvEmailId.setText(Constants.IF_EMPTY);
            tvContactNumber.setText(Constants.IF_EMPTY);
            tvAddress.setText(Constants.IF_EMPTY);
            tvCountry.setText(Constants.IF_EMPTY);
            tvRegion.setText(Constants.IF_EMPTY);
            tvCity.setText(Constants.IF_EMPTY);
            tvPostalCode.setText(Constants.IF_EMPTY);
        }

    }

    private void showError(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(OrganizationInfoActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        });

    }

    private void showErrorwithFinish(String title, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(OrganizationInfoActivity.this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText(title);
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.show();
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //on selecting on item in menu
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
