package com.indexify.indexify;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.eftimoff.viewpagertransformers.CubeOutTransformer;
import com.indexify.indexify.fragments.FragmentVisualet;

import java.util.ArrayList;
import java.util.List;

public class VisualetActivity extends AppCompatActivity implements View.OnClickListener {

    List<String> alImgUrls = new ArrayList<>();
    List<String> alLocalImgUrls = new ArrayList<>();
    ImageButton cancelButton;
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private int NUM_PAGES = 0;
    public int position1 = 0;


    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
//    private ViewPager mPager;
    HackyViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setStatusBarColor(ContextCompat.getColor(ProductViewActivity.this,R.color.my_statusbar_color));
        setContentView(R.layout.activity_visualet_view);

        // get data from database
        position1 = getIntent().getIntExtra("imagePosition", 0);
        cancelButton = (ImageButton) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);
        alLocalImgUrls = (List<String>) getIntent().getSerializableExtra("imgUrls");
        NUM_PAGES = alLocalImgUrls.size();
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (HackyViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        // mPager.setPageTransformer(true, new ZoomOutPageTransformer());

        ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                position1 = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        mPager.addOnPageChangeListener(onPageChangeListener);
        mPager.setAdapter(mPagerAdapter);
        //  mPager.setPageTransformer(false, new Transformer());
        mPager.setPageTransformer(true, new CubeOutTransformer());
        mPager.setCurrentItem(position1);


        // auto slide

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (position1 == NUM_PAGES) {
                    position1 = 0;
                }
                mPager.setCurrentItem(position1++, true);
            }
        };


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.cancelButton) {
            finish();
        }
    }


    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */

    public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        // static ScreenSlidePageFragment s = new ScreenSlidePageFragment();

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            FragmentVisualet s = new FragmentVisualet();
            Bundle b = new Bundle();
            b.putInt("test", position);
            b.putSerializable("localImgUrl", alLocalImgUrls.get(position));
            s.setArguments(b);
            //return new ScreenSlidePageFragment();

            return s;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }


    }

}
