package com.indexify.indexify.pojo;

import java.io.Serializable;

/**
 * Created by amar on 31-01-2018.
 */

public class ResponsePojo implements Serializable {

    int status;
    String message;
    int responseCode;
    String timestamp;
    ResponseExtraPojo extras;


    public ResponseExtraPojo getExtras() {
        return extras;
    }

    public void setExtras(ResponseExtraPojo extras) {
        this.extras = extras;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
