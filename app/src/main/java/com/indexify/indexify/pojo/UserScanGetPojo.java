package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by amar on 05-02-2018.
 */

public class UserScanGetPojo implements Serializable {

    @SerializedName("_embedded")
    private UserScanGetPojo.Embedded embedded;
    private PagePojo page;

    public Embedded getEmbedded() {
        return embedded;
    }

    public void setEmbedded(Embedded embedded) {
        this.embedded = embedded;
    }

    public PagePojo getPage() {
        return page;
    }

    public void setPage(PagePojo page) {
        this.page = page;
    }

    public class Embedded implements Serializable {

        @SerializedName("userScans")
        List<UserScanPojo> userScans;

        public List<UserScanPojo> getUserScans() {
            return userScans;
        }

        public void setUserScans(List<UserScanPojo> userScans) {
            this.userScans = userScans;
        }
    }
}
