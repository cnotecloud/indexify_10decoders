package com.indexify.indexify.pojo;

import java.io.Serializable;

/**
 * Created by cnote on 06-03-2018.
 */

public class QRPojo implements Serializable {

    int id;
    String name;
    String displayName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
