package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by cnote on 07-02-2018.
 */

public class ValueTagPojo implements Serializable{
    @SerializedName("value")
    public Object value = "";
    @SerializedName("tag")
    public Object tag = "";

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }
}
