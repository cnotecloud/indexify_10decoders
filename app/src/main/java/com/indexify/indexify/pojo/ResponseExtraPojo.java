package com.indexify.indexify.pojo;

import java.io.Serializable;

/**
 * Created by amar on 13-02-2018.
 */

public class ResponseExtraPojo implements Serializable {
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
