package com.indexify.indexify.pojo;

import java.io.Serializable;

/**
 * Created by amar on 02-08-2017.
 */

public class BreadScrumbPojo implements Serializable {

    String displayText;
    String tag;

    public BreadScrumbPojo(String displayText, String tag) {
        this.displayText = displayText;
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }
}
