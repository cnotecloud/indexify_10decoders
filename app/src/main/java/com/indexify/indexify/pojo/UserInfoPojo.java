package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by amar on 19-01-2018.
 */

public class UserInfoPojo implements Serializable {

    long id;
    String email;
    String username;
    String firstName;
    String lastName;
    String contact;



    @SerializedName("location")
    LocationPojo locationPojo;

    @SerializedName("organization")
    OrganizationPojo organizationPojo;
    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
    public LocationPojo getLocationPojo() {
        return locationPojo;
    }

    public void setLocationPojo(LocationPojo locationPojo) {
        this.locationPojo = locationPojo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public OrganizationPojo getOrganizationPojo() {
        return organizationPojo;
    }

    public void setOrganizationPojo(OrganizationPojo organizationPojo) {
        this.organizationPojo = organizationPojo;
    }


}
