package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by amar on 20-01-2018.
 */

public class ControlsPojo implements Serializable {

    @SerializedName("name")
    private String name ="";
    @SerializedName("value")
    private Object value ="";


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
