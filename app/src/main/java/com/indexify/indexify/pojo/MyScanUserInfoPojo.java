package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cnote on 07-02-2018.
 */

public class MyScanUserInfoPojo {
    long id;
    String firstName;
    String lastName;

    @SerializedName("email")
    ValueTagPojo email;

    @SerializedName("contact")
    ValueTagPojo contact;

    @SerializedName("location")
    LocationPojo locationPojo;

    public ValueTagPojo getEmail() {
        return email;
    }

    public void setEmail(ValueTagPojo email) {
        this.email = email;
    }

    public ValueTagPojo getContact() {
        return contact;
    }

    public void setContact(ValueTagPojo contact) {
        this.contact = contact;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocationPojo getLocationPojo() {
        return locationPojo;
    }

    public void setLocationPojo(LocationPojo locationPojo) {
        this.locationPojo = locationPojo;
    }
}
