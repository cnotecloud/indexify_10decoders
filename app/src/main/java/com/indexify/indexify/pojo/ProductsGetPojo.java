package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by amar on 20-01-2018.
 */

public class ProductsGetPojo implements Serializable {
    @SerializedName("_embedded")
    private ProductsGetPojo.Embedded embedded;
    private PagePojo page;

    public Embedded getEmbedded() {
        return embedded;
    }

    public void setEmbedded(Embedded embedded) {
        this.embedded = embedded;
    }

    public PagePojo getPage() {
        return page;
    }

    public void setPage(PagePojo page) {
        this.page = page;
    }

    public class Embedded implements Serializable {

        @SerializedName("products")
        List<ProductPojo> products;

        public List<ProductPojo> getProducts() {
            return products;
        }

        public void setProducts(List<ProductPojo> products) {
            this.products = products;
        }
    }
}
