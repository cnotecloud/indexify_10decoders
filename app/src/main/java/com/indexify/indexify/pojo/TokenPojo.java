package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TokenPojo implements Serializable {

    @SerializedName("token")
    private String token;
    @SerializedName("refreshToken")
    private String refreshToken;

    String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
