package com.indexify.indexify.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by amar on 29-12-2017.
 */

public class ProductPojo implements Serializable {
    String id;
    String name;
    String leafId;
    List<String> imgUrls;
    String imgUrlString;
    List<String> localImgUrls;
    String localImgUrlString;
    List<ControlsPojo> controls;


    public String getId() {
        return id;
    }

    public String getImgUrlString() {
        return imgUrlString;
    }

    public void setImgUrlString(String imgUrlString) {
        this.imgUrlString = imgUrlString;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLeafId() {
        return leafId;
    }

    public void setLeafId(String leafId) {
        this.leafId = leafId;
    }

    public List<String> getImgUrls() {
        return imgUrls;
    }

    public void setImgUrls(List<String> imgUrls) {
        this.imgUrls = imgUrls;
    }

    public List<ControlsPojo> getControls() {
        return controls;
    }

    public void setControls(List<ControlsPojo> controls) {
        this.controls = controls;
    }

    public List<String> getLocalImgUrls() {
        return localImgUrls;
    }

    public void setLocalImgUrls(List<String> localImgUrls) {
        this.localImgUrls = localImgUrls;
    }

    public String getLocalImgUrlString() {
        return localImgUrlString;
    }

    public void setLocalImgUrlString(String localImgUrlString) {
        this.localImgUrlString = localImgUrlString;
    }
}
