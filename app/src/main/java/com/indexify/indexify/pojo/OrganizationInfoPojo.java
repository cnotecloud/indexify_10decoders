package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cnote on 07-02-2018.
 */

public class OrganizationInfoPojo {
    long id;
    String displayName;
    String website;
    String imgUrl;
    String description;
    boolean publicProfile;
    String catalogue;
    List<String> catalogues;

    @SerializedName("location")
    LocationPojo locationPojo;

    @SerializedName("emails")
    List<ValueTagPojo> alEmails;

    @SerializedName("contacts")
    List<ValueTagPojo> alContacts;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublicProfile() {
        return publicProfile;
    }

    public void setPublicProfile(boolean publicProfile) {
        this.publicProfile = publicProfile;
    }

    public LocationPojo getLocationPojo() {
        return locationPojo;
    }

    public void setLocationPojo(LocationPojo locationPojo) {
        this.locationPojo = locationPojo;
    }

    public List<ValueTagPojo> getAlEmails() {
        return alEmails;
    }

    public void setAlEmails(List<ValueTagPojo> alEmails) {
        this.alEmails = alEmails;
    }

    public List<ValueTagPojo> getAlContacts() {
        return alContacts;
    }

    public void setAlContacts(List<ValueTagPojo> alContacts) {
        this.alContacts = alContacts;
    }

    public String getCatalogue() {
        return catalogue;
    }

    public void setCatalogue(String catalogue) {
        this.catalogue = catalogue;
    }

    public List<String> getCatalogues() {
        return catalogues;
    }

    public void setCatalogues(List<String> catalogues) {
        this.catalogues = catalogues;
    }
}
