package com.indexify.indexify.pojo;

import java.io.Serializable;

/**
 * Created by amar on 12-02-2018.
 */

public class FavouriteOrganizationGetPojo implements Serializable {
    String organizationName;
    Long organizationId;

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }
}
