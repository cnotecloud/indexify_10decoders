package com.indexify.indexify.pojo;

import java.io.Serializable;

/**
 * Created by amar on 05-02-2018.
 */

public class ScanPojo implements Serializable {

    int id;
    int scannedId;
    String scannedName;
    boolean isOrg;
    int userId;
    int userOrgId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScannedId() {
        return scannedId;
    }

    public void setScannedId(int scannedId) {
        this.scannedId = scannedId;
    }

    public String getScannedName() {
        return scannedName;
    }

    public void setScannedName(String scannedName) {
        this.scannedName = scannedName;
    }

    public boolean isOrg() {
        return isOrg;
    }

    public void setOrg(boolean org) {
        isOrg = org;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserOrgId() {
        return userOrgId;
    }

    public void setUserOrgId(int userOrgId) {
        this.userOrgId = userOrgId;
    }
}
