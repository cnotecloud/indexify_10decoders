package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by amar on 19-01-2018.
 */

public class MyProductFavouriteViewerGetPojo implements Serializable {

    @SerializedName("_embedded")
    private Embedded embedded;


    public Embedded getEmbedded() {
        return embedded;
    }

    public void setEmbedded(Embedded embedded) {
        this.embedded = embedded;
    }

    public class Embedded implements Serializable {

        @SerializedName("reverseFavouriteProducts")
        List<MyProductFavouriteViewerPojo> alMyFavourite;

        public List<MyProductFavouriteViewerPojo> getAlMyFavourite() {
            return alMyFavourite;
        }

        public void setAlMyFavourite(List<MyProductFavouriteViewerPojo> alMyFavourite) {
            this.alMyFavourite = alMyFavourite;
        }
    }
}
