package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by amar on 19-01-2018.
 */

public class OrganizationPojo implements Serializable {

    long id;
    String displayName;
    String website;
    String email;
    String contact;
    String description;
    boolean publicProfile;
    @SerializedName("location")
    LocationPojo locationPojo;

    public boolean isPublicProfile() {
        return publicProfile;
    }

    public void setPublicProfile(boolean publicProfile) {
        this.publicProfile = publicProfile;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocationPojo getLocationPojo() {
        return locationPojo;
    }

    public void setLocationPojo(LocationPojo locationPojo) {
        this.locationPojo = locationPojo;
    }
}
