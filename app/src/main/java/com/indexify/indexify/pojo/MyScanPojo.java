package com.indexify.indexify.pojo;

import java.io.Serializable;

/**
 * Created by amar on 01-02-2018.
 */

public class MyScanPojo implements Serializable {
    int id;
    int orgId;
    String orgInfo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public String getOrgInfo() {
        return orgInfo;
    }

    public void setOrgInfo(String orgInfo) {
        this.orgInfo = orgInfo;
    }
}
