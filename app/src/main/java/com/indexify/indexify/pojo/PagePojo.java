package com.indexify.indexify.pojo;

import java.io.Serializable;

/**
 * Created by amar on 20-01-2018.
 */

public class PagePojo implements Serializable {
    int size;
    int totalElements;
    int totalPages;
    int number;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
