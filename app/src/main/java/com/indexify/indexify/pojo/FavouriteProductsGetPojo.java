package com.indexify.indexify.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by amar on 13-02-2018.
 */

public class FavouriteProductsGetPojo {

    @SerializedName("_embedded")
    private FavouriteProductsGetPojo.Embedded embedded;
    private PagePojo page;

    public Embedded getEmbedded() {
        return embedded;
    }

    public void setEmbedded(Embedded embedded) {
        this.embedded = embedded;
    }

    public PagePojo getPage() {
        return page;
    }

    public void setPage(PagePojo page) {
        this.page = page;
    }

    public class Embedded implements Serializable {
        List<FavouriteProductPojo> favouriteProducts;

        public List<FavouriteProductPojo> getFavouriteProducts() {
            return favouriteProducts;
        }

        public void setFavouriteProducts(List<FavouriteProductPojo> favouriteProducts) {
            this.favouriteProducts = favouriteProducts;
        }
    }
}
